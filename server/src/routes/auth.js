import express from 'express';
import User from '../models/user';
import bcrypt from 'bcrypt-nodejs';
import jwt from 'jsonwebtoken';
import config from '../config'

let router = express.Router();

router.post('/', (req, res) => {
    const { identifier, password } = req.body;
    console.log(req.body);
    User.query({
        where: { email: identifier }
    }).fetch().then(user =>{
        if(user){
            if(bcrypt.compareSync(password, user.get('password_digest'))){
                const token =jwt.sign({
                    id: user.get('id'),
                    name: user.get('name'),
                    email: user.get('email'),
                    isAdmin: user.get('isAdmin'),
                    isSuperAdmin: user.get('isSuperAdmin')
                }, config.jwtSecret);
                res.json({ token });
            }else{
                res.status(401).json({ errors: { form: 'No se ha logrado iniciar sesión con la información suministrada' } });
            }
        }else{
            res.status(401).json({ errors: { form: 'No se ha logrado iniciar sesión con la información suministrada' } });
        }
    })
});

export  default router;