import express from 'express';

import Invoice from '../models/invoice';
import authenticate from '../middlewares/authenticate';

let router = express.Router();

router.post('/get', (req, res) => {
    Invoice.query({
        where: { id: req.body.id }
    }).fetch().then(invoice => {
        res.json({ invoice });
    });
});

router.post('/create', authenticate, (req, res) => {
    const {
        title,
        content,
        category,
        date,
        outstanding,
        active
    } = req.body;
    let media = JSON.stringify(req.body.pictures);
    Invoice.forge({
        title,
        content,
        media,
        category,
        date,
        outstanding,
        active
    }, { hasTimestamps: true })
        .save()
        .then(invoice => res.json({ success: true }))
        .catch(err => res.status(500).json({ error: err }));
});

router.post('/update', (req, res) => {
    let {
        title,
        content,
        media,
        category,
        date,
        outstanding,
        active
    } = req.body;
    media = JSON.stringify(media);
    new Invoice({ id: req.body.id }).save({
        title,
        content,
        media,
        category,
        date,
        outstanding,
        active
    }, {hasTimestamps: true})
        .then(invoice => res.json({success: true}))
        .catch(err => res.status(500).json({error: err}));
});

router.post('/all', authenticate, (req, res) => {
    let params = req.body;
    Invoice.query({
        where: { category: params.page }
    })
        .count()
        .then(products => {
            let count = parseInt(products);
            new Invoice()
                .query({
                    where: { category: params.page }
                })
                .query(invoices => {
                    invoices.limit(params.pageSize)
                        .offset( (params.pageSize * (params.currentPage - 1)) )
                        .orderBy('created_at', 'DESC');
                })
                .fetchAll()
                .then( filteredInvoices => {
                    res.json({
                        "error": false,
                        "message": "",
                        "currentPage": params.currentPage,
                        "pageSize": params.pageSize,
                        "productsCount": count,
                        "totalPages": Math.ceil(count/params.pageSize),
                        "filteredInvoices": filteredInvoices
                    });
                })

        })
});

router.post('/all/limit', (req, res) => {
    Invoice.query(qb =>{
        qb.limit(req.body.limit)
    }).fetchAll()
        .then(invoices => res.json({ invoices }))
        .catch(err => res.status(500).json({ error: err }));
});

router.post('/all/limit', authenticate, (req, res) => {
    Invoice.query(qb =>{
        qb.limit(req.body.limit)
    }).fetchAll()
        .then(invoices => res.json({ invoices }))
        .catch(err => res.status(500).json({ error: err }));
});

router.post('/delete', (req, res) => {
    new Invoice({ id: req.body.id })
        .destroy()
        .then(() => res.json({ success: true }))
        .catch(err => res.status(500).json({ error: err }));
});

export default router;