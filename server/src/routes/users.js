import express from 'express';
import commonValidations from '../shared/validations/signup';
import authenticate from '../middlewares/authenticate';
import bcrypt from 'bcrypt-nodejs';
import isEmpty from 'lodash/isEmpty';

//Model
import User from '../models/user';

let router = express.Router();

function validateInput(data, otherValidations){
    let { errors } = otherValidations(data);

    return User.query({
        where: { email: data.email }
    }).fetch().then(user => {
        if(user){
            if(user.get('email') === data.email){
                errors.email = 'El correo ingresado ya está registrado.';
            }
        }

        return {
            errors,
            isValid: isEmpty(errors)
        }
    });
}

function validateEditInputs(data, otherValidations){
    let { errors } = otherValidations(data);

    return User.query({
        where: { id: data.id }
    }).fetch().then(user => {
        if(!user) {
            errors.email = 'El correo ingresado no está registrado.';
        }
        return {
            errors,
            isValid: isEmpty(errors)
        }
    });
}

router.get('/:identifier', (req, res) => {
    User.query({
        select: [ 'id', 'email' ],
        where: { email: req.params.identifier }
    }).fetch().then(user => {
        res.json({ user });
    })
});

router.get('/get-id/:identifier', authenticate, (req, res) => {
    User.query({
        where: { id: req.params.identifier }
    }).fetch().then(user => {
        res.json({ user });
    })
});

router.post('/get', authenticate, (req, res) => {
    User.where('email', 'LIKE', `%${req.body.searchString}%`)
        .fetchAll().then(users => {
            res.json({ users });
    })
});

router.post('/update', authenticate, (req, res) => {
    validateEditInputs(req.body, commonValidations).then(({ errors, isValid }) => {
        if (isValid) {
            const {
                name,
                email,
                password,
                gender,
                birthDate,
                telephone,
                city,
                isAdmin,
                isSuperAdmin
            } = req.body;
            const password_digest = bcrypt.hashSync(password);

            new User({ id: req.body.id }).save({
                name,
                email,
                password_digest,
                gender,
                birthDate,
                telephone,
                city,
                isAdmin,
                isSuperAdmin
            }, {hasTimestamps: true})
                .then(user => res.json({success: true}))
                .catch(err => res.status(500).json({error: err}));
        } else {
            res.status(400).json(errors);
        }
    });
});


router.post('/', (req, res) => {
    validateInput(req.body, commonValidations).then(({ errors, isValid }) => {
        if(isValid) {
            const {
                name,
                email,
                password,
                gender,
                birthDate,
                telephone,
                city,
                isAdmin,
                isSuperAdmin
            } = req.body;
            const password_digest = bcrypt.hashSync(password);

            User.forge({
                name,
                email,
                password_digest,
                gender,
                birthDate,
                telephone,
                city,
                isAdmin,
                isSuperAdmin
            }, { hasTimestamps: true })
                .save()
                .then(user => res.json({ success: true }))
                .catch(err => res.status(500).json({ error: err }));
        }else{
            res.status(400).json(errors);
        }
    });
});

router.post('/all', authenticate, (req, res) => {
    let params = req.body;
    new User()
        .count()
        .then(users => {
            let count = parseInt(users);
            new User()
                .query(users => {
                    users.limit(params.pageSize)
                        .offset( (params.pageSize * (params.currentPage - 1)) )
                        .orderBy('created_at', 'DESC');
                })
                .fetchAll()
                .then( filteredUsers => {
                    res.json({
                        "error": false,
                        "message": "",
                        "currentPage": params.currentPage,
                        "pageSize": params.pageSize,
                        "usersCount": count,
                        "totalPages": Math.ceil(count/params.pageSize),
                        "filteredUsers": filteredUsers
                    });
                })

    })
});

/*router.post('/create-admin', (req, res) => {
    const password_digest = bcrypt.hashSync('Gaton123');

    User.forge({
        username: 'casamatria@casamatria.org',
        name: 'Casa Matria',
        gender: 1,
        documentType: 'Cedula',
        document: '0000000000',
        birthDate: '2016-11-20',
        telephone: '000-0000',
        address: 'Casa Matria',
        district: 1,
        neighborhood: 1,
        organization: 'Casa Matria',
        institution: 'Casa Matria',
        visitReason: 'Todas',
        isAdmin: true,
        userAdmin: true,
        eventAdmin: true,
        password_digest
    }, { hasTimestamps: true })
        .save()
        .then(user => res.json({ success: true }))
        .catch(err => res.status(500).json({ error: err }));
});*/

router.post('/delete', authenticate, (req, res) => {
    new User({ id: req.body.id })
        .destroy()
        .then(user => res.json({ success: true }))
        .catch(err => res.status(500).json({ error: err }));
});



export default router;
