import express from 'express';
import authenticate from '../middlewares/authenticate';
import isEmpty from 'lodash/isEmpty';

//Model
import Product from '../models/product';

let router = express.Router();

router.post('/get', (req, res) => {
    Product.query({
        where: { id: req.body.id }
    }).fetch().then(product => {
        res.json({ product });
    });
});

router.post('/update', (req, res) => {
    const {
        name,
        description,
        media,
        price,
        page,
        active
    } = req.body;

    new Product({ id: req.body.id }).save({
        name,
        description,
        media,
        price,
        page,
        active
    }, {hasTimestamps: true})
        .then(product => res.json({success: true}))
        .catch(err => res.status(500).json({error: err}));
});


router.post('/', (req, res) => {
    const {
        name,
        description,
        price,
        page,
        active
    } = req.body;
    let media = JSON.stringify(req.body.media);
    let invalidPrice = isNaN(price);
    if(invalidPrice){
        res.status(500).json({ errors: {price: 'Precio Invalido'} });
        return false;
    }
    Product.forge({
        name,
        description,
        media,
        price,
        page,
        active
    }, { hasTimestamps: true })
        .save()
        .then(product => res.json({ success: true }))
        .catch(err => res.status(500).json({ error: err }));
});

router.post('/all', (req, res) => {
    let params = req.body;
        Product.query({
            where: { page: params.page }
        })
        .count()
        .then(products => {
            let count = parseInt(products);
            new Product()
                .query({
                    where: { page: params.page }
                })
                .query(products => {
                    products.limit(params.pageSize)
                        .offset( (params.pageSize * (params.currentPage - 1)) )
                        .orderBy('created_at', 'DESC');
                })
                .fetchAll()
                .then( filteredUsers => {
                    res.json({
                        "error": false,
                        "message": "",
                        "currentPage": params.currentPage,
                        "pageSize": params.pageSize,
                        "productsCount": count,
                        "totalPages": Math.ceil(count/params.pageSize),
                        "filteredProducts": filteredUsers
                    });
                })

        })
});

router.post('/delete', (req, res) => {
    new Product({ id: req.body.id })
        .destroy()
        .then(product => res.json({ success: true }))
        .catch(err => res.status(500).json({ error: err }));
});



export default router;
