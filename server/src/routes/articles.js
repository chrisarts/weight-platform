import express from 'express';

import Article from '../models/article';
import authenticate from '../middlewares/authenticate';

let router = express.Router();

router.post('/get', (req, res) => {
    const { id } = req.body;
    if(id !== -1){
        Article.query({
            where: { id: req.body.id }
        }).fetch().then(article => {
            res.json({ article });
        });
    }else{
        res.json({
            error: true,
            message: 'Debes especificar un ID'
        });
    }

});

router.post('/create', authenticate, (req, res) => {
    const {
        title,
        content,
        category,
        date,
        outstanding,
        active
    } = req.body;
    let media = JSON.stringify(req.body.pictures);
    Article.forge({
        title,
        content,
        media,
        category,
        date,
        outstanding,
        active
    }, { hasTimestamps: true })
        .save()
        .then(article => res.json({ success: true }))
        .catch(err => res.status(500).json({ error: err }));
});

router.post('/update', (req, res) => {
    let {
        title,
        content,
        media,
        category,
        date,
        outstanding,
        active
    } = req.body;
    media = JSON.stringify(media);
    new Article({ id: req.body.id }).save({
        title,
        content,
        media,
        category,
        date,
        outstanding,
        active
    }, {hasTimestamps: true})
        .then(article => res.json({success: true}))
        .catch(err => res.status(500).json({error: err}));
});

router.post('/all', authenticate, (req, res) => {
    let params = req.body;
    Article.query({
        where: { category: params.page }
    })
        .count()
        .then(products => {
            let count = parseInt(products);
            new Article()
                .query({
                    where: { category: params.page }
                })
                .query(articles => {
                    articles.limit(params.pageSize)
                        .offset( (params.pageSize * (params.currentPage - 1)) )
                        .orderBy('created_at', 'DESC');
                })
                .fetchAll()
                .then( filteredArticles => {
                    res.json({
                        "error": false,
                        "message": "",
                        "currentPage": params.currentPage,
                        "pageSize": params.pageSize,
                        "productsCount": count,
                        "totalPages": Math.ceil(count/params.pageSize),
                        "filteredArticles": filteredArticles
                    });
                })

        })
});

router.post('/all/limit', (req, res) => {
    Article.query(qb =>{
        qb.limit(req.body.limit)
    }).fetchAll()
        .then(articles => res.json({ articles }))
        .catch(err => res.status(500).json({ error: err }));
});

router.post('/all/limit', authenticate, (req, res) => {
    Article.query(qb =>{
        qb.limit(req.body.limit)
    }).fetchAll()
        .then(articles => res.json({ articles }))
        .catch(err => res.status(500).json({ error: err }));
});

router.post('/delete', (req, res) => {
    new Article({ id: req.body.id })
        .destroy()
        .then(() => res.json({ success: true }))
        .catch(err => res.status(500).json({ error: err }));
});

export default router;