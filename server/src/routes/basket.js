import express from 'express';
import authenticate from '../middlewares/authenticate';
import isEmpty from 'lodash/isEmpty';

//Model
import Basket from '../models/basket';

let router = express.Router();

router.get('/:identifier', (req, res) => {
    Basket.query({
        where: { id: req.params.identifier }
    }).fetch().then(basket => {
        res.json({ basket });
    });
});

router.get('/product/:identifier', (req, res) => {
    Basket.query({
        where: { productId: req.params.identifier }
    }).fetchAll().then(basket => {
        res.json({ basket });
    });
});

router.get('/user/:identifier', (req, res) => {
    Basket.query({
        where: { userId: req.params.identifier }
    }).fetchAll().then(basket => {
        res.json({ basket });
    });
});

router.post('/', (req, res) => {
    const {
        productId,
        userId,
        quantity,
        page
    } = req.body;
    Basket.forge({
        productId,
        userId,
        quantity,
        page
    }, { hasTimestamps: true })
        .save()
        .then(basket => res.json({ success: true }))
        .catch(err => res.status(500).json({ error: err }));
});

router.post('/update/product', (req, res) => {
    const {
        quantity
    } = req.body;

    new Basket({ id: req.body.id }).save({
        quantity
    }, {hasTimestamps: true})
        .then(basket => res.json({success: true}))
        .catch(err => res.status(500).json({error: err}));
});

router.post('/all', (req, res) => {
    let params = req.body;
    new Basket()
        .count()
        .then(baskets => {
            let count = parseInt(baskets);
            new Basket()
                .query(baskets => {
                    baskets.limit(params.pageSize)
                        .offset( (params.pageSize * (params.currentPage - 1)) )
                        .orderBy('created_at', 'DESC');
                })
                .fetchAll()
                .then( filteredUsers => {
                    res.json({
                        "error": false,
                        "message": "",
                        "currentPage": params.currentPage,
                        "pageSize": params.pageSize,
                        "basketsCount": count,
                        "totalPages": Math.ceil(count/params.pageSize),
                        "filteredBaskets": filteredUsers
                    });
                })

        })
});

router.post('/delete', (req, res) => {
    new Basket({ id: req.body.id })
        .destroy()
        .then(basket => res.json({ success: true }))
        .catch(err => res.status(500).json({ error: err }));
});



export default router;
