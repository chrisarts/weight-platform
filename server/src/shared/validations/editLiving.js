import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export default function validateInput(data){
    let errors = {};

    if(Validator.isEmpty(data.name)){
        errors.name = 'Este campo es obligatiorio';
    }
    if(Validator.isEmpty(String(data.quantity))){
        errors.quantity = 'Este campo es obligatiorio';
    }

    return {
        errors,
        isValid: isEmpty(errors)
    }
}