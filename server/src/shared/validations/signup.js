import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export default function validateInput(data){
    let errors = {};

    if(Validator.isEmpty(data.name)){
        errors.name = 'Este campo es obligatiorio';
    }
    if(Validator.isEmpty(data.email)){
        errors.email = 'Este campo es obligatiorio';
    }
    if(Validator.isEmpty(data.password)){
        errors.password = 'Este campo es obligatiorio';
    }
    if(Validator.isEmpty(data.confirmPassword)){
        errors.confirmPassword = 'Este campo es obligatiorio';
    }
    if(!Validator.equals(data.password, data.confirmPassword)){
        errors.password = 'Las contraseñas no coinciden';
        errors.confirmPassword = 'Las contraseñas no coinciden';
    }

    return {
        errors,
        isValid: isEmpty(errors)
    }
}
