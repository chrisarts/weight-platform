import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export default function validateInput(data){
    let errors = {};

    if(Validator.isEmpty(data.identifier)){
        errors.identifier = 'Este campo es obligatorio'
    }

    if(Validator.isEmpty(data.password)){
        errors.password = 'Este campo es obligatorio'
    }

    return {
        errors,
        isValid: isEmpty(errors)
    };
}