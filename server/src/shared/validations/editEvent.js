import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export default function validateInput(data){
    let errors = {};

    if(Validator.isEmpty(data.title)){
        errors.title = 'Este campo es obligatorio'
    }
    if(Validator.isEmpty(data.eventType)){
        errors.eventType = 'Este campo es obligatorio'
    }
    if(Validator.isEmpty(data.subject)){
        errors.subject = 'Este campo es obligatorio'
    }
    if(Validator.isEmpty(data.inCharge)){
        errors.inCharge = 'Este campo es obligatorio'
    }
    if(Validator.isEmpty( String(data.tallerist) )){
        errors.tallerist = 'Este campo es obligatorio'
    }
    if(Validator.isEmpty(data.startDate)){
        errors.startDate = 'Este campo es obligatorio'
    }
    if(Validator.isEmpty(data.startHour)){
        errors.startHour = 'Este campo es obligatorio'
    }
    if(Validator.isEmpty(data.schedules)){
        errors.schedules = 'Este campo es obligatorio'
    }
    if(Validator.isEmpty(data.tallerType)){
        errors.tallerType = 'Este campo es obligatorio'
    }

    return {
        errors,
        isValid: isEmpty(errors)
    };
}