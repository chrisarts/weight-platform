exports.up = function(knex) {
    return knex.schema.createTable('invoiceDetails', function(table){
        table.increments();
        table.integer('invoiceId').notNullable();
        table.integer('productId').notNullable();
        table.string('currentPrice').notNullable();
        table.timestamps();
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('invoiceDetails');
};