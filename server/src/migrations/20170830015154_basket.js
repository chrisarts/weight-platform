exports.up = function(knex) {
    return knex.schema.createTable('basket', function(table){
        table.increments();
        table.integer('productId').notNullable();
        table.integer('userId').notNullable();
        table.integer('quantity').notNullable();
        table.string('page').notNullable();
        table.timestamps();
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('basket');
};
