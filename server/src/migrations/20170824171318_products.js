exports.up = function(knex, Promise) {
    return knex.schema.createTable('products', function(table){
        table.increments();
        table.string('name').notNullable();
        table.text('description', 'longtext').notNullable();
        table.text('media', 'longtext').notNullable();
        table.string('price').notNullable();
        table.string('page').notNullable();
        table.boolean('active').notNullable();
        table.timestamps();
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('products');
};
