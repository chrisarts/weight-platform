exports.up = function(knex) {
    return knex.schema.createTable('invoices', function(table){
        table.increments();
        table.integer('userId').notNullable();
        table.string('receiverName').notNullable();
        table.string('receiverAddress').notNullable();
        table.string('page').notNullable();
        table.timestamps();
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('invoices');
};
