
exports.up = function(knex) {
    return knex.schema.createTable('users', function(table){
        table.increments();
        table.string('name').notNullable();
        table.string('email').notNullable();
        table.string('password_digest').notNullable();
        table.string('gender');
        table.date('birthDate');
        table.string('telephone');
        table.string('city');
        //Reglas de administración
        // #1 = Ver - #2 = Ver/Editar - #3 = Ver/Editar/Borrar
        table.boolean('isAdmin').notNullable();
        table.boolean('isSuperAdmin').notNullable();
        table.timestamps();
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('users');
};
