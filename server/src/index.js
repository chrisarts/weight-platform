import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import ejs from 'ejs';
import fs from 'fs';

//React SSR

//Routes
import users from './routes/users';
import auth from './routes/auth';
import products from './routes/products';
import articles from './routes/articles';
import basket from './routes/basket';

let app = express();

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(bodyParser.json());


app.use('/api/users', users);
app.use('/api/login', auth);
app.use('/api/articles', articles);
app.use('/api/products', products);
app.use('/api/basket', basket);


app.use(express.static(path.join(__dirname, '../../client/public')));


//Configure Front-end communication
const metadata = {
    title: 'Home'
};
//since we are in a request handler function
//we're using readFile instead of readFileSync


app.use('*', function (request, response){
    let temp = "title";
    ejs.render(__dirname, '../../client/public/index.html', { temp } );
});


//Configure server
app.listen(8080, () => console.log('Running on localhost:8080'));