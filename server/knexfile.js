'use strict';

// Update with your config settings.

module.exports = {

  production: {
    client: 'postgresql',
    connection: {
      database: 'weight-platform',
      user: 'chris',
      password: 'Gaton123!!'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },
  development: {
    client: 'postgresql',
    connection: {
      database: 'weight-platform',
      user: 'postgres',
      password: 'Gaton123'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  }
};