import React from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

// Fitmas Page Components
import App from './template/components/App';
import Home from './template/components/Home';
import LoginFront from './template/components/login/LoginFront';
import SignUpPage from './template/components/signup/SignUpPage';
import Product from './template/components/Product';
import Products from './template/components/Products';
import AboutFitmas from './template/components/aboutComponents/AboutFitmas';
import AboutLipoblue from './template/components/aboutComponents/AboutLipoblue';
import Blog from './template/components/Blog';
import Article from './template/components/Article';
import Contact from './template/components/Contact';
import Basket from './template/components/basket/Basket';
import Payment from './template/components/payment/Payment';

import requireAuth from './utils/requireAuth';
import Admin from './admin/Admin';
import AdminHome from './admin/Home';
import LoginPage from './admin/login/LoginPage';
import CreateArticle from './admin/articles/CreateArticle';
import EditArticle from './admin/articles/EditArticle';
import CreateProduct from './admin/products/CreateProduct';
import ProductsList from './admin/products/ProductsList';
import EditProduct from './admin/products/EditProduct';

//Routes for this app (if any route must be protected wrap it with the function requireAuth(ReactComponent)
export default (
    <Router history={browserHistory}>
        <Route path="/" components={App}>
            <IndexRoute component={Home} />
            <Route path="/iniciar-sesion" component={LoginFront}/>
            <Route path="/registrarse" component={SignUpPage}/>
            <Route path="/productos" component={Products}/>
            <Route path="/producto/:name/:identifier" component={Product}/>
            <Route path="/blog" component={Blog}/>
            <Route path="/blog/:articleName/:articleId" component={Article}/>
            <Route path="/contacto" component={Contact}/>

            <Route path="/sobre-fitmas" component={AboutFitmas}/>
            <Route path="/sobre-lipoblue" component={AboutLipoblue}/>

            <Route path="/carrito" component={Basket}/>
            <Route path="/pago" component={Payment}/>
        </Route>

        <Route path="/admin" component={Admin}>
            <Route path="/admin/login" component={LoginPage}/>
            <Route path="/admin/home" component={requireAuth(AdminHome)}/>
            <Route path="/admin/articles/create" component={requireAuth(CreateArticle)}/>
            <Route path="/admin/products" component={ProductsList}/>
            <Route path="/admin/products/create" component={CreateProduct}/>
            <Route path="/admin/products/:identifier" component={EditProduct}/>
            <Route path="/admin/articles/:identifier" component={EditArticle}/>
        </Route>
    </Router>

);
