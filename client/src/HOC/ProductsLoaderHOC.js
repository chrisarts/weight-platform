import React from 'react';
import CircularProgress from 'material-ui/CircularProgress';

const ProductsLoaderHOC = (WrappedComponent) => {
    return class ProductsLoaderHOC extends React.Component{
        constructor(props){
            super(props);
            console.log(this.props);
        }
        render(){
            return(
                this.props.products.length === 0 ? (
                    <div
                        className={`loading-section show`}
                    >
                        <CircularProgress
                            style={{
                                marginTop: "10%"
                            }}
                            size={60}
                            thickness={7}
                        />
                        <h3>Cargando Productos...</h3>
                    </div>
                ) : (
                    <WrappedComponent products={this.props.products}/>
                )
            );
        };
    }
};

export default ProductsLoaderHOC;