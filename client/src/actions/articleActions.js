import {
    ARTICLES_REQUEST,
    ARTICLES_SUCCESS,
    ARTICLES_FAILURE
} from '../constants';
import api from '../api/articles';
import axios from 'axios';

const fetchingArticle = () => ({
    type: ARTICLES_REQUEST
})

const articleSuccess = (article) => ({
    type: ARTICLES_SUCCESS,
    payload: article
})

export function fetchArticle(id){
    return(dispatch) => {
        dispatch(fetchingArticle());
        return api.getArticle(id)
                    .then((article) => dispatch(articleSuccess));
    };
}

export function createArticle(article){
    return dispatch => {
        return axios.post('/api/articles/create', article)
    }
}

export function updateArticle(article){
    return dispatch => {
        return axios.post('/api/articles/update', article)
    }
}

export function getArticleList(article){
    return dispatch => {
        return axios.post('/api/articles/all', article)
    }
}

export function getArticleListLimit(limit) {
    return dispatch => {
        return axios.post('/api/articles/all/limit', limit)
    }
}

export function deleteArticle(article) {
    return dispatch => {
        return axios.post('/api/articles/all/limit', article)
    }
}