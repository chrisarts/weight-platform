import axios from 'axios';

export function getProductList(pagination){
    return () => {
        return axios.post('/api/products/all', pagination)
    }
}

export function getProduct(product){
    return () => {
        return axios.post(`/api/products/get`, product)
    }
}

export function createProduct(product){
    return () => {
        return axios.post('/api/products', product);
    }
}

export function updateProduct(product){
    return () => {
        return axios.post('/api/products/update', product);
    }
}

export function deleteProduct(id){
    return () => {
        return axios.post('/api/products/delete', id);
    }
}