import {SET_CURRENT_PAGE} from './types';

// Set the page in localStorage using his reducer
export function setCurrentPage(page){
    return {
        type: SET_CURRENT_PAGE,
        page
    }
}