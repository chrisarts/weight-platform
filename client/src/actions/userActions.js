import axios from 'axios';

export function addToCar(params){
    return () => {
        return axios.post('/api/basket', params);
    }
}

export function getBasket(user){
    return () => {
        return axios.get(`/api/basket/user/${user}`);
    }
}

export function findItem(id){
    return() => {
        return axios.get(`/api/basket/${id}`);
    }
}

export function findProduct(id){
    return() => {
        return axios.get(`/api/basket/product/${id}`);
    }
}

export function deleteBasketProduct(id){
    return () => {
        return axios.post(`/api/basket/delete`, id);
    }
}

export function updateBasketProduct(params){
    return () => {
        return axios.post('/api/basket/update/product', params);
    }
}