import axios from 'axios';

export function getInvoice(article){
    return dispatch => {
        return axios.post('/api/invoices/get', article)
    }
}

export function createInvoice(article){
    return dispatch => {
        return axios.post('/api/invoices/create', article)
    }
}

export function updateInvoice(article){
    return dispatch => {
        return axios.post('/api/invoices/update', article)
    }
}

export function getInvoiceList(article){
    return dispatch => {
        return axios.post('/api/invoices/all', article)
    }
}

export function getInvoiceListLimit(limit) {
    return dispatch => {
        return axios.post('/api/invoices/all/limit', limit)
    }
}

export function deleteInvoice(article) {
    return dispatch => {
        return axios.post('/api/invoices/all/limit', article)
    }
}