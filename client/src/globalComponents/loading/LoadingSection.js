import React from 'react';
import PropTypes from 'prop-types';
import CircularProgress from 'material-ui/CircularProgress';

class LoadingSection extends React.Component{

    render(){
        return(
            <div
                className={`loading-section ${this.props.payload ? 'show' : 'hidden'}`}
                style={{
                    width: this.props.width,
                    height: this.props.height,
                    left: 0
                }}
            >
                <CircularProgress
                    style={{
                        marginTop: "10%"
                    }}
                    size={60}
                    thickness={7}
                />
                {this.props.text ? (
                    <div>
                        Cargando Productos...
                    </div>
                ) : (
                    <div>&nbsp;</div>
                )}
            </div>
        );
    }
}

LoadingSection.PropTypes = {
    payload: PropTypes.bool.isRequired,
    width: PropTypes.string,
    height: PropTypes.string
};

export default LoadingSection;