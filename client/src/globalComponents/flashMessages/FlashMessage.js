import React from 'react';
import Snackbar from 'material-ui/Snackbar';

class FlashMessage extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            open: true
        };
    }

    handleRequestClose() {
        this.setState({
            open: false,
        });
    };

    render(){
        const { text } = this.props.message;
        return(
            <div>
                <Snackbar
                    open={this.state.open}
                    message={text}
                    action="Cerrar"
                    autoHideDuration={3000}
                    onActionTouchTap={this.handleRequestClose.bind(this)}
                />
            </div>
        );
    }
}

FlashMessage.propTypes = {
    message: React.PropTypes.object.isRequired
};

export default FlashMessage;