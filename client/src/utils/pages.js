const pages = {
    fitmas: {
        trueName: 'Fit+',
        name: 'fitmas',
        letterColor1: '#8a2a17',
        footerBackground: '#53263a',
        buttonColor: '#89286d'
    },
    lipoblue: {
        trueName: 'Lipoblue',
        name: 'lipoblue',
        letterColor1: '#490147',
        footerBackground: '#0a6780',
        buttonColor: '#ff006e'
    }
};

let currentPage = {};

switch(document.domain.split('.')[0]){
    case 'fitmas':
        currentPage  = pages.fitmas;
        break;
    case 'lipobluestore':
        currentPage = pages.lipoblue;
        break;
    default:
        currentPage = pages.fitmas;
        break;
}

export default currentPage;

