import React from 'react';
import LoginForm from './LoginForm';

class LoginFront extends React.Component{

    render(){
        return(
            <div className="login-front-container">
                <LoginForm/>
            </div>
        );
    }
}

export default LoginFront;