import React from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import {Helmet} from 'react-helmet';

class Contact extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            name: '',
            city: '',
            telephone: '',
            isLoading: false,
            errors: {}
        }
    }

    onChange(event){
        this.setState({ [event.target.name]: event.target.value });
    };

    render(){
        let responsive = window.innerWidth <= 830;
        const {isLoading, errors} = this.state;
        return(
            <div
                className="page-contact"
                style={responsive ? {
                   paddingTop: 30
                }:{}}
            >
                <Helmet>
                    <title>Contacto</title>
                </Helmet>
                <div
                    className="left"
                    style={responsive ? {
                       width: '100%',
                        paddingLeft: 0,
                        padding: '0 15px',
                        textAlign: 'center'
                    }:{}}
                >
                    <h1>CONTACTO</h1>
                    <p>
                        Tienda Mujer Slim es perfecta para todas
                        las mujeres interesadas en belleza,
                        bienestar y salud, a través de nuestros
                        productos buscamos afianzar la confianza
                        en sí misma.
                    </p>
                    <p>Contra Entrega, Bogotá - Cali</p>
                    <p>PBX (1) 381-9663 / 314-773-823</p>
                </div>
                <div
                    className="right"
                    style={responsive ? {
                        width: '100%',
                        paddingLeft: 0,
                        padding: '0 15px',
                        textAlign: 'center'
                    }:{}}
                >
                    <form action="">
                        <h2>
                            ¿Quieres ser distribuidor
                            en tu país y ciudad?
                            Envíanos tus datos
                        </h2>
                        <TextField
                            name="name"
                            floatingLabelText="Nombre"
                            value={this.state.name}
                            errorText={errors.name}
                            onChange={this.onChange.bind(this)}
                            style={{
                                width: '100%'
                            }}
                            inputStyle={{
                                backgroundColor: '#53263a'
                            }}
                            floatingLabelStyle={{
                                color: 'white'
                            }}
                            underlineFocusStyle={{
                                color: 'white'
                            }}
                            underlineStyle={{
                                color: 'white'
                            }}
                        />
                        <TextField
                            name="city"
                            floatingLabelText="Ciudad"
                            value={this.state.city}
                            errorText={errors.city}
                            onChange={this.onChange.bind(this)}
                            style={{
                                width: '100%'
                            }}
                            inputStyle={{
                                backgroundColor: '#53263a'
                            }}
                            floatingLabelStyle={{
                                color: 'white'
                            }}
                        />
                        <TextField
                            name="telephone"
                            floatingLabelText="Teléfono"
                            value={this.state.telephone}
                            errorText={errors.telephone}
                            onChange={this.onChange.bind(this)}
                            style={{
                                width: '100%'
                            }}
                            inputStyle={{
                                backgroundColor: '#53263a'
                            }}
                            floatingLabelStyle={{
                                color: 'white'
                            }}
                        />
                        <RaisedButton
                            label="Enviar"
                            type="submit"
                            disabled={isLoading}
                        />
                    </form>
                </div>
            </div>
        )
    }
}

export default Contact;