import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {TableRow, TableRowColumn} from 'material-ui/Table';

import {getProduct} from '../../../actions/productActions';
import LoadingSection from "../../../globalComponents/loading/LoadingSection";

class ProductSubtotal extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            id: 0,
            name: '',
            price: '',
            subTotal: ''
        };
        this.styles = {
            deleteActions: {
                float: 'right',
                buttons: {
                    marginRight: 20
                }
            }
        };
    };

    componentDidMount(){
        let This = this;
        console.log(this.props.basketProduct);
        This.props.getProduct({id: This.props.basketProduct.productId}).then(result => {
            let price = parseInt(result.data.product.price, 0);
            let priceFormat = price.toLocaleString(
                undefined, // use a string like 'en-US' to override browser locale
                { minimumFractionDigits: 0 }
            );
            let subTotal = price * parseInt(This.props.basketProduct.quantity, 0);
            let subTotalFormat = subTotal.toLocaleString(
                undefined, // use a string like 'en-US' to override browser locale
                { minimumFractionDigits: 0 }
            );
            This.setState({
                id: result.data.product.id,
                name: result.data.product.name,
                price: priceFormat,
                subTotal: subTotalFormat,
            });
            let currentTotal = This.props.getTotal();
            let newTotal = parseInt(currentTotal, 0) + parseInt(subTotal, 0);
            This.props.updateTotal(newTotal);
        })
    }


    render(){
        return(
            <TableRow key={this.state.id}>
                <LoadingSection
                    payload={this.state.isLoading}
                    width="100%"
                    height="100px"
                />
                <TableRowColumn>{this.state.name}</TableRowColumn>
                <TableRowColumn>{this.props.basketProduct.quantity}</TableRowColumn>
                <TableRowColumn>$ {this.state.price}</TableRowColumn>
                <TableRowColumn>$ {this.state.subTotal}</TableRowColumn>
            </TableRow>
        )
    }
}

ProductSubtotal.PropTypes = {
    getProduct: PropTypes.func.isRequired,
    basketProduct: PropTypes.object.isRequired,
    updateTotal: PropTypes.func.isRequired,
    getTotal: PropTypes.func.isRequired
};

export default connect(null, {getProduct})(ProductSubtotal);