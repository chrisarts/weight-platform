import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableFooter, TableRowColumn} from 'material-ui/Table';
import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';
import jwtDecode from 'jwt-decode';
import md5 from 'md5';

import LoadingSection from '../../../globalComponents/loading/LoadingSection';
import ProductSubtotal from './ProductSubtotal';
import {getBasket} from '../../../actions/userActions';

function validateInputEvent(data){
    let errors = {};

    if(Validator.isEmpty(data.name)){
        errors.name = 'Este campo es obligatorio'
    }
    if(Validator.isEmpty(data.address)){
        errors.address = 'Este campo es obligatorio'
    }

    if(Validator.isEmpty(data.price)){
        errors.price = 'Este campo es obligatorio'
    }

    return {
        errors,
        isValid: isEmpty(errors)
    };
}

class Payment extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            user: {},
            receiverName: '',
            receiverAddress: '',
            products: [],
            signature: '',
            total: '0',
            merchantId: '',
            errors: {},
            isLoading: false
        };
    }

    componentDidMount(){
        let This = this;
        let decodedToken = jwtDecode(localStorage.jwtToken);
        This.props.getBasket(decodedToken.id).then(result => {
            This.setState({
                user: decodedToken,
                products: result.data.basket
            })
        });
    }

    isValid(){
        const { errors, isValid } = validateInputEvent(this.state);

        if(!isValid){
            this.setState({ errors: errors })
        }

        return isValid;
    };

    onChange(event){
        this.setState({ [event.target.name]: event.target.value });
    };

    updateTotal(newTotal){
        let signature = md5(`4Vj8eK4rloUd272L48hsrarnUA~508029~TestPayU~20000~COP`);
        this.setState({
            total: newTotal,
            signature
        });
    }
    getCurrentTotal(){
        return this.state.total;
    }

    render(){
        const { errors } = this.state;
        return(
            <div className="page-payment">
                <h1>Información de envío</h1>
                <form
                    method="post"
                    action="https://sandbox.gateway.payulatam.com/ppp-web-gateway"
                    target="_blank"
                >
                    <input name="merchantId" type="hidden" value="508029" />
                    <input name="accountId" type="hidden" value="512321" />
                    <input name="signature" type="hidden" defaultValue={this.state.signature} />
                    <input name="referenceCode" type="hidden" defaultValue="TestPayU" />
                    <input name="amount" type="hidden" value="20000" />
                    <input name="buyerFullName" type="hidden" value="Chris el comprador" />
                    <input name="buyerEmail" type="hidden" value="test@test.com" />
                    <input name="description" type="hidden" value={`Test PAYU`} />
                    <input name="responseUrl" type="hidden" defaultValue={`http://${document.domain}/response`} />
                    <input name="confirmationUrl" type="hidden" defaultValue={`http://${document.domain}/confirmation`} />
                    <input name="tax" type="hidden" value="3193" />
                    <input name="taxReturnBase" type="hidden" value="16806" />
                    <input name="currency" type="hidden" value="COP" />
                    <input name="test" type="hidden" value="1" />
                    <TextField
                        name="name"
                        floatingLabelText="Nombre de quién recibe *"
                        value={this.state.name}
                        errorText={errors.name}
                        onChange={this.onChange.bind(this)}
                        style={{
                            width: '100%'
                        }}
                    />
                    <TextField
                        name="address"
                        floatingLabelText="Dirección de quién recibe *"
                        value={this.state.address}
                        errorText={errors.address}
                        onChange={this.onChange.bind(this)}
                        style={{
                            width: '100%'
                        }}
                    />
                    <section>
                        <h2>Lista de Productos</h2>
                        <LoadingSection
                            payload={this.state.isLoading}
                            width="100%"
                            height="400px"
                        />
                        <Table>
                            <TableHeader
                                displaySelectAll={false}
                                adjustForCheckbox={false}
                            >
                                <TableRow>
                                    <TableHeaderColumn>Nombre</TableHeaderColumn>
                                    <TableHeaderColumn>Cantidad</TableHeaderColumn>
                                    <TableHeaderColumn>Precio Unitario</TableHeaderColumn>
                                    <TableHeaderColumn>Subtotal</TableHeaderColumn>
                                </TableRow>
                            </TableHeader>
                            <TableBody
                                displayRowCheckbox={false}
                            >
                                {
                                    this.state.products.map(product =>
                                        <ProductSubtotal
                                            key={product.id}
                                            updateTotal={this.updateTotal.bind(this)}
                                            getTotal={this.getCurrentTotal.bind(this)}
                                            basketProduct={product}
                                        />
                                    )
                                }
                            </TableBody>
                            <TableFooter>
                                <TableRow>
                                    <TableRowColumn>&nbsp;</TableRowColumn>
                                    <TableRowColumn>&nbsp;</TableRowColumn>
                                    <TableRowColumn>
                                        <h4
                                            style={{
                                                textAlign: 'right'
                                            }}
                                        >
                                            Total:
                                        </h4>
                                    </TableRowColumn>
                                    <TableRowColumn>
                                        <h4>$
                                            {this.state.total.toLocaleString(
                                                undefined, // use a string like 'en-US' to override browser locale
                                                { minimumFractionDigits: 0 }
                                            )}
                                        </h4>
                                    </TableRowColumn>
                                </TableRow>
                            </TableFooter>
                        </Table>
                        <FlatButton
                            label="Proceder al pago"
                            type="submit"
                            backgroundColor="#89286d"
                            className="payment-button"
                            style={{
                                marginTop: 20,
                                fontFamily: 'cuyabra',
                                color: 'white',
                                fontWeight: 'bold',
                                padding: '0px 25px',
                                float: 'right'
                            }}
                        />
                    </section>
                </form>
            </div>
        )
    }
}

Payment.PropTypes = {
    getBasket: PropTypes.func.isRequired
};

function mapStateToProps(state){
    return {
        page: state.page.page
    }
}

export default connect(mapStateToProps, {getBasket})(Payment);