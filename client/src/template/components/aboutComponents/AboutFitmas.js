import React from 'react';
import {connect} from 'react-redux';

class About extends React.Component{

    render(){
        let page = this.props.page;
        let responsive = window.innerWidth < 700;
        return(
            <div>
                <figure>
                    <img
                        style={{
                            width: '100%'
                        }}
                        src={`/images/${page.name}/aboutBanner.jpg`} alt={`Sobre ${page.trueName}`}/>
                </figure>
                <div className={`about-section ${page.name}`}>
                    <div className="image" style={responsive ? {
                        width: '90%',
                        margin: '0 auto'
                    }:{

                    }}>
                        <figure>
                            <img
                                src={`/images/${page.name}/aboutInfo1.jpg`} alt={`${page.trueName} Producto`}
                            />
                        </figure>
                    </div>
                    <div className="text" style={responsive ? {
                        width: '95%',
                        margin: '0 auto'
                    }:{

                    }}>
                        <h3 style={responsive ? {
                           textAlign: 'center'
                        }:{}}>
                            TODOS QUEREMOS UN CUERPO “IDEAL”
                            UNA SALUD “IDEAL” UNA VIDA “IDEAL”</h3>
                        <p>
                            La quema de grasa es vital para tener un cuerpo con alto
                            porcentaje múscular y un bajo nivel graso, y numerosos
                            estudios han demostrado que los componentes de ciertos
                            alimentos ayudan a estimular la quema de grasas en el
                            cuerpo.
                        </p>
                        <strong>
                            ESTO TERMINA EN UN CUERPO CON UNA COMPOSICIÓN ALTA
                            EN GRASA Y BAJA EN MÚSCULO, SIENDO TODO LO CONTRARIO
                            DE LO IDEAL PARA TENER UNA VIDA SANA Y ENÉRGICA
                        </strong>
                    </div>
                </div>
                <div className={`about-section1 ${page.name}`}>
                    <div className="left"
                         style={responsive ? {width: '100%'}:{}}
                    >
                        <h3>COMPONENTES PRINCIPALES DE FIT+</h3>
                        <ul>
                            <li>Extracto de semilla de mango Africano</li>
                            <li>Extracto de cascara de Naranja Agria</li>
                            <li>Extracto de raíz de Coleus Forksholll</li>
                            <li>Dihidrocapsiato (componente clave del pigmento rojo)</li>
                        </ul>
                    </div>
                    <div className="right" style={responsive ? {width: '100%'}:{}}>
                        <div>
                            UN PRODUCTO DISEÑADO PARA AYUDAR A LOS ADULTOS
                            DE TODAS LAS EDADES A ALCANZAR UN CUERPO
                            DELGADO Y SALUDABLE
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state){
    return {
        page: state.page.page
    }
}

export default connect(mapStateToProps, {})(About);