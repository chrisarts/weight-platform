import React from 'react';
import {connect} from 'react-redux';

class About extends React.Component{

    render(){
        let page = this.props.page;
        return(
            <div>
                <figure>
                    <img
                        style={{
                            width: '100%'
                        }}
                        src={`/images/${page.name}/aboutBanner.jpg`} alt={`Sobre ${page.trueName}`}/>
                </figure>
                <div className={`about-section`}>
                    <div className="image">
                        <figure>
                            <img src={`/images/${page.name}/aboutInfo1.jpg`} alt={`${page.trueName} Producto`}/>
                        </figure>
                    </div>
                    <div className="text">
                        <h3>
                            TODOS QUEREMOS UN CUERPO “IDEAL”
                            UNA SALUD “IDEAL” UNA VIDA “IDEAL”
                        </h3>
                        <p>
                            La quema de grasa es vital para tener un cuerpo con alto
                            porcentaje múscular y un bajo nivel graso, y numerosos
                            estudios han demostrado que los componentes de ciertos
                            alimentos ayudan a estimular la quema de grasas en el
                            cuerpo.
                        </p>
                        <strong>
                            ESTO TERMINA EN UN CUERPO CON UNA COMPOSICIÓN ALTA
                            EN GRASA Y BAJA EN MÚSCULO, SIENDO TODO LO CONTRARIO
                            DE LO IDEAL PARA TENER UNA VIDA SANA Y ENÉRGICA
                        </strong>
                    </div>
                </div>
                <div className={`about-section2`}>
                    <div className="left">
                        <h3>
                            Con base en estas investigaciones hemos
                            desarrollado un producto que potencializa la
                            quema acelerada de Grasa en nuestro organismo
                        </h3>
                        <figure>
                            <img src="/images/lipoblue/logo.png " alt=""/>
                        </figure>
                    </div>
                </div>
                <div className={`about-section3`}>
                    <div className="left">
                        <h3>BENEFICIOS DE LIPOBLUE</h3>
                        <ul>
                            <li>Estimula la quema de grasas en tu cuerpo</li>
                            <li>Reduce la ansiedad de comer</li>
                            <li>
                                Provee energía para acelerar tus rutinas de
                                ejercicio y alcanzar un rendimiento óptimo
                            </li>
                        </ul>
                    </div>
                    <div className="right">
                        <div className="quote">
                            ¡Con LIPOBLUE podrás quemar de 4 a 6 kg
                            de grasa en solo 30 días!
                        </div>
                        <div className="big-text">
                            UN PRODUCTO DISEÑADO PARA AYUDAR A LOS ADULTOS
                            DE TODAS LAS EDADES A MEJORAR SU SALUD, CALIDAD
                            DE VIDA Y AUTOESTIMA
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state){
    return {
        page: state.page.page
    }
}

export default connect(mapStateToProps, {})(About);