import React from 'react';
import {connect} from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import AppToolbar from './commons/AppToolbar';
import Footer from './commons/Footer';
import SubFooter from './commons/SubFooter';
import FlashMessagesList from '../../globalComponents/flashMessages/FlashMessagesList';

import '../css/styles.css';

// Needed for onTouchTap
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

//This will be our Main Component
class App extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            value: 3,
        };
    }

    render(){
        let page = this.props.page;
        return(
            <MuiThemeProvider>
                <div>
                    <AppToolbar/>
                    <FlashMessagesList/>
                    <div className={`global-container ${page.name}`}>
                        {this.props.children}
                        <Footer/>
                        <SubFooter background={page.footerBackground}/>
                    </div>
                </div>
            </MuiThemeProvider>
        );
    }
}

function mapStateToProps(state){
    return {
        page: state.page.page
    }
}

export default connect(mapStateToProps, {})(App);