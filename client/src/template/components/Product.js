import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Slider from 'react-slick';
import {Grid, Col, Row} from 'react-bootstrap';
import {
    ActionShoppingBasket
} from 'material-ui/svg-icons';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import ReactHtmlParser from 'react-html-parser';
import jwtDecode from 'jwt-decode';
import {Helmet} from 'react-helmet';

import ProductsGridFull from './commons/ProductsGridFull';
import LoadingSection from '../../globalComponents/loading/LoadingSection';
import {getProduct} from '../../actions/productActions';
import {addToCar, findProduct} from '../../actions/userActions';
import {addFlashMessage} from '../../actions/flashMessages';

class Product extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            name: '',
            description: '',
            media: {},
            page: '',
            price: '',
            quantity: 1,
            isLoading: true
        };
        this.productSlider = {
            dots: true,
            fade: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            style: {
                width: 'auto',
		        height: '100%',
                maxHeight: '500px',
                margin: '0 auto'
            }
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isLoading: true
        });
        this.chargeProduct(nextProps.params.identifier);
    }

    onChangeQuantity(event){
        let This = this;
        if(event.target.value > 0){
            This.setState({
                quantity: event.target.value
            });
        }else{
            window.alert('Valor inválido.');
        }
    }

    componentDidMount(){
        this.chargeProduct(this.props.params.identifier);
    }

    chargeProduct(id){
        let This = this;
        This.props.getProduct({ id }).then(result => {
            let newDescription = ReactHtmlParser(result.data.product.description);
            let media = JSON.parse(result.data.product.media);
            let count = 0;
            let image = '';
            for (let key in media) {
                if (media.hasOwnProperty(key) && count === 0) {
                    image = media[key];
                    ++count;
                }
            }
            let price = parseInt(result.data.product.price, 0);
            let priceFormat = price.toLocaleString(
                undefined, // use a string like 'en-US' to override browser locale
                { minimumFractionDigits: 0 }
            );
            This.setState({
                name: result.data.product.name,
                price: priceFormat,
                page: result.data.product.page,
                description: newDescription,
                media: image,
                isLoading: false
            });
        });
    }

    addToCar(){
        let This = this;
        if(This.props.auth.isAuthenticated){
            This.props.findProduct(This.props.params.identifier).then(result => {
                if(result.data.basket.length === 0){
                    let decodedToken = jwtDecode(localStorage.jwtToken);
                    let params = {
                        productId: This.props.params.identifier,
                        userId: decodedToken.id,
                        quantity: This.state.quantity,
                        page: 'fitmas'
                    };
                    This.props.addToCar(params).then(() => {
                        This.props.addFlashMessage({
                            type: 'success',
                            text: 'Producto añadido al carrito'
                        });
                    });
                }else{
                    window.alert('El producto ya está en el carrito');
                }
            });
        }else{
            window.alert('Debes loguearte para agregar productos al carrito');
        }
    }

    render(){

        return(
            <div style={{
                paddingTop: 60,
                width: '100%',
                margin: '0 auto',
                marginBottom: 70
            }}>
                <Helmet>
                    <title>{this.state.name}</title>
                </Helmet>
                <div className="product-preview">
                    <LoadingSection
                        payload={this.state.isLoading}
                        height="576px"
                        width="100%"
                    />
                    <Grid>
                        <Row className="product-image-features">
                            <Col md={4} className="product-media">
                                <Slider {...this.productSlider}>
                                    <div>
                                        <img
                                            src={this.state.media} alt=""
                                            style={this.productSlider.style}
                                        />
                                    </div>
                                </Slider>
                            </Col>
                            <Col md={8} className="product-features">
                                <div className="product-head">
                                    <h1>{this.state.name}</h1>
                                    <div className="basket">
                                        <div className="product-quantity">
                                            <TextField
                                                name="quantity"
                                                value={this.state.quantity}
                                                type="number"
                                                style={{
                                                    width: 60,
                                                    background: 'rgba(255,255,255,0.2)'
                                                }}
                                                inputStyle={{
                                                    textAlign: 'center'
                                                }}
                                                underlineFocusStyle={{
                                                    borderColor: '#fb2900'
                                                }}
                                                onChange={this.onChangeQuantity.bind(this)}
                                            />
                                        </div>
                                        <strong>$ {this.state.price}</strong>
                                        <FlatButton
                                            label="Añadir al Carrito"
                                            backgroundColor="#ff2a00"
                                            icon={<ActionShoppingBasket/>}
                                            style={{
                                                color: 'white'
                                            }}
                                            onClick={this.addToCar.bind(this)}
                                        />
                                    </div>
                                </div>
                                <div className="product-description">
                                    {this.state.description}
                                </div>
                            </Col>
                        </Row>
                    </Grid>
                    <hr style={{ width: '100%' }}/>
                    <ProductsGridFull pageSize={3}/>
                </div>
            </div>
        )
    }
}

Product.PropTypes = {
    getProduct: PropTypes.func.isRequired,
    addToCar: PropTypes.func.isRequired,
    auth: React.PropTypes.object.isRequired,
    findProduct: React.PropTypes.func.isRequired,
    addFlashMessage: React.PropTypes.func.isRequired
};

function mapStateToProps(state){
    return {
        auth: state.auth
    }
}

export default connect(mapStateToProps ,{getProduct,addToCar,findProduct,addFlashMessage})(Product);
