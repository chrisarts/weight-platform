import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { Link } from 'react-router';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import {TableRow, TableRowColumn} from 'material-ui/Table';
import {
    ContentClear,
    ContentCreate
} from 'material-ui/svg-icons';

import {getProduct} from '../../../actions/productActions';
import {deleteBasketProduct,updateBasketProduct} from '../../../actions/userActions';
import {addFlashMessage} from '../../../actions/flashMessages';

class ProductItem extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            id: 0,
            name: '',
            price: '',
            media: '{}',
            quantity: this.props.basketProduct.quantity,
            deleteOpen: false,
            productToDelete: 0,
            editOpen: false,
            productToEdit: 0
        };
        this.styles = {
            deleteActions: {
                float: 'right',
                buttons: {
                    marginRight: 20
                }
            }
        };
    };

    componentDidMount(){
        let This = this;
        This.props.getProduct({id: This.props.basketProduct.productId}).then(result => {
            let price = parseInt(result.data.product.price, 0);
            let subTotal = price * parseInt(this.props.basketProduct.quantity, 0);
            let priceFormat = subTotal.toLocaleString(
                undefined, // use a string like 'en-US' to override browser locale
                { minimumFractionDigits: 0 }
            );
            This.setState({
                id: result.data.product.id,
                name: result.data.product.name,
                price: priceFormat,
                media: result.data.product.media
            })
        })
    }

    onChange(event){
        this.setState({ [event.target.name]: event.target.value });
    };

    deleteActionsNegative(){
        this.setState({
            deleteOpen: false
        });
    }

    deleteActionsPositive(){
        let This = this;
        This.props.deleteBasketProduct({id:This.props.basketProduct.id}).then(() => {
            This.props.addFlashMessage({
                type: 'success',
                text: 'Producto Eliminado'
            });
            This.setState({
                deleteOpen: false
            });
            window.location.reload();
        });
    }

    handleDeleteOpen(){
        this.setState({
            deleteOpen: true
        });
    }

    handleEditOpen(){
        this.setState({
            editOpen: true
        });
    }

    editActionsNegative(){
        this.setState({
            editOpen: false
        });
    }

    editActionsPositive(){
        let This = this;
        This.props.updateBasketProduct({
            id: This.props.basketProduct.id,
            quantity: This.state.quantity
        }).then(() => {
            window.location.reload();
        });
    }


    render(){
        let media = JSON.parse(this.state.media);
        let count = 0;
        let image = '';
        for (let key in media) {
            if (media.hasOwnProperty(key) && count === 0) {
                image = media[key];
                ++count;
            }
        }
        return(
            <TableRow key={this.state.id}>
                <TableRowColumn>
                    <figure>
                        <img
                            height={100}
                            src={image} alt=""/>
                    </figure>
                </TableRowColumn>
                <TableRowColumn>{this.state.name}</TableRowColumn>
                <TableRowColumn>{this.props.basketProduct.quantity}</TableRowColumn>
                <TableRowColumn>$ {this.state.price}</TableRowColumn>
                <TableRowColumn>
                    <Link  onClick={this.handleEditOpen.bind(this)}>
                        <ContentCreate/>
                    </Link>
                    <Dialog
                        title="Editar cantidad de productos en el pedido"
                        open={this.state.editOpen}
                    >
                        <div>
                            <TextField
                                name="quantity"
                                floatingLabelText="Cantidad de Productos *"
                                value={this.state.quantity}
                                type="number"
                                onChange={this.onChange.bind(this)}
                                style={{
                                    width: '100%'
                                }}
                            />
                        </div>
                        <div
                            className="delete-actions"
                            style={this.styles.deleteActions}
                        >
                            <FlatButton
                                label="No Actualizar"
                                onClick={this.editActionsNegative.bind(this)}
                                backgroundColor="#a4c639"
                                hoverColor="#8AA62F"
                                style={this.styles.deleteActions.buttons}
                            />
                            <FlatButton
                                label="Actualizar"
                                onClick={this.editActionsPositive.bind(this)}
                                backgroundColor="#9b2211"
                                hoverColor="#8b1e0f"
                                style={this.styles.deleteActions.buttons}
                            />
                        </div>
                    </Dialog>
                </TableRowColumn>
                <TableRowColumn>
                    <Link onClick={this.handleDeleteOpen.bind(this)}>
                        <ContentClear/>
                    </Link>
                    <Dialog
                        title="¿Estás seguro de querer eliminar éste Producto?"
                        open={this.state.deleteOpen}
                    >
                        <div
                            className="delete-actions"
                            style={this.styles.deleteActions}
                        >
                            <FlatButton
                                label="No Eliminar"
                                onClick={this.deleteActionsNegative.bind(this)}
                                backgroundColor="#a4c639"
                                hoverColor="#8AA62F"
                                style={this.styles.deleteActions.buttons}
                            />
                            <FlatButton
                                label="Eliminar"
                                onClick={this.deleteActionsPositive.bind(this, this.state.id)}
                                backgroundColor="#9b2211"
                                hoverColor="#8b1e0f"
                                style={this.styles.deleteActions.buttons}
                            />
                        </div>
                    </Dialog>
                </TableRowColumn>
            </TableRow>
        )
    }
}

ProductItem.PropTypes = {
    getProduct: PropTypes.object.isRequired,
    basketProduct: PropTypes.object.isRequired,
    deleteBasketProduct: PropTypes.func.isRequired,
    addFlashMessage: PropTypes.func.isRequired,
    updateBasketProduct: PropTypes.func.isRequired
};

export default connect(null, {
    getProduct,
    deleteBasketProduct,
    addFlashMessage,
    updateBasketProduct
})(ProductItem);