import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import FlatButton from 'material-ui/FlatButton';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow} from 'material-ui/Table';
import jwtDecode from 'jwt-decode';

import LoadingSection from '../../../globalComponents/loading/LoadingSection';
import ProductItem from './ProductItem';
import {getBasket} from '../../../actions/userActions';

class Basket extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            products: [],
            isAuth: false,
            user: {},
            deleteOpen: false,
            isLoading: true
        };
    }

    componentDidMount(){
        let This=this;
        if(This.props.auth.isAuthenticated){
            let decodedToken = jwtDecode(localStorage.jwtToken);
            This.setState({
                isAuth: true,
                user: decodedToken
            });
            This.props.getBasket(decodedToken.id).then(result => {
                console.log('Basket: ', result);
                This.setState({
                    products: result.data.basket,
                    isLoading: false
                })
            });
        }
    }

    render(){
        return(
            <div className="page-basket">
                <h1>Productos en Carrito</h1>
                <LoadingSection
                    payload={this.state.isLoading}
                    width="100%"
                    height="300px"
                />
                <div
                    style={{
                        width: '100%',
                        textAlign: 'center'
                    }}
                >
                    <FlatButton
                        label="Proceder al pago"
                        backgroundColor="#89286d"
                        style={{
                            fontFamily: 'cuyabra',
                            color: 'white',
                            fontWeight: 'bold',
                            padding: '0px 25px'
                        }}
                        href="/pago"
                    />
                </div>
                <Table>
                    <TableHeader
                        displaySelectAll={false}
                        adjustForCheckbox={false}
                    >
                        <TableRow>
                            <TableHeaderColumn>Imágen</TableHeaderColumn>
                            <TableHeaderColumn>Nombre</TableHeaderColumn>
                            <TableHeaderColumn>Cantidad</TableHeaderColumn>
                            <TableHeaderColumn>Precio</TableHeaderColumn>
                            <TableHeaderColumn>Editar</TableHeaderColumn>
                            <TableHeaderColumn>Eliminar</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody
                        displayRowCheckbox={false}
                    >
                        {
                            this.state.products.map(product =>
                                <ProductItem
                                    key={product.id}
                                    basketProduct={product}
                                />
                            )
                        }
                    </TableBody>
                </Table>
            </div>
        );
    }
}

Basket.PropTypes = {
    getBasket: PropTypes.func.isRequired
};

function mapStateToProps(state){
    return {
        auth: state.auth
    }
}

export default connect(mapStateToProps ,{getBasket})(Basket);