import React from 'react';
import {connect} from 'react-redux';
import {Grid, Col, Row} from 'react-bootstrap';
import {Helmet} from 'react-helmet';

import ProductsGridFull from './commons/ProductsGridFull';

class Products extends React.Component{

    render(){
        let page = this.props.page;
        return(
            <div className="products-page">
                <Grid>
                    <Helmet>
                        <title>Tienda</title>
                    </Helmet>
                    <Row className="products-sales">
                        <Col md={6} className="sale">
                            <img src={`/images/${page.name}/productsPromo1.jpg`} alt={`${page.name} promoción`}/>
                        </Col>
                        <Col md={6} className="sale">
                            <img src={`/images/${page.name}/productsPromo2.jpg`} alt={`${page.name} promoción`}/>
                        </Col>
                    </Row>
                    <Row className="products-packs">
                        <Col md={4} className="pack">
                            <img src={`/images/${page.name}/productsMediaPromo1.jpg`} alt={`${page.name} Lista de Promociones`}/>
                        </Col>
                        <Col md={4} className="pack">
                            <img src={`/images/${page.name}/productsMediaPromo2.jpg`} alt={`${page.name} Tabla de Precios`}/>
                        </Col>
                        <Col md={4} className="pack">
                            <img src={`/images/${page.name}/productsMediaPromo3.jpg`} alt={`${page.name} PDF`}/>
                        </Col>
                    </Row>
                    <ProductsGridFull pageSize={3}/>
                </Grid>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        page: state.page.page
    }
}

export default connect(mapStateToProps, {})(Products);
