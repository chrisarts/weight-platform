import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Grid, Col, Row} from 'react-bootstrap';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import moment from 'moment';
import {Helmet} from 'react-helmet';

import {getArticleList} from '../../actions/articleActions';
import LoadingSection from '../../globalComponents/loading/LoadingSection';

class Blog extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            articles: [],
            page: this.props.page.name,
            pagination: {
                pageSize: 20,
                currentPage: 1,
                page: this.props.page.name
            },
            isLoading: true
        };
        moment.locale('es');
    }

    componentDidMount(){
        let This = this;
        This.props.getArticleList(This.state.pagination).then(result => {
            This.setState({
                articles: result.data.filteredArticles,
                isLoading: false
            })
        });
    }

    goArticle(articleUrl){
        this.context.router.push(articleUrl);
    }

    render(){
        return(
            <Grid className={`blog-grid  ${this.state.isLoading ? "section-loading" : "section-loaded"}`}>
                <Helmet>
                    <title>Blog</title>
                </Helmet>
                <Row>
                    <h1>Articulos</h1>
                    <LoadingSection
                        payload={this.state.isLoading}
                        width="100%"
                        height="300px"
                    />
                    {this.state.articles.map(article => {
                        let media = JSON.parse(article.media);
                        let count = 0;
                        let image = '';
                        for (let key in media) {
                            if (media.hasOwnProperty(key) && count === 0) {
                                image = media[key];
                                ++count;
                            }
                        }
                        let newDescription = article.content.replace(/<\/?[^>]+(>|$)/g, "");
                        newDescription = newDescription.substring(0, 200);
                        let newDate = moment(article.created_at).format("dddd, MMMM D YYYY, h:mm a");
                        newDate = newDate.replace('pm', "PM");
                        newDate = newDate.replace('am', "AM");
                        let urlName = article.title.replace(' ','-');
                        let articleUrl = `/blog/${urlName}/${article.id}`;
                        return(
                            <Col md={4} key={article.id}>
                                <Card>
                                    <CardHeader
                                        title={article.title}
                                        subtitle={newDate}
                                        style={{
                                            textTransform: 'capitalize'
                                        }}
                                    />
                                    <CardMedia>
                                        <img src={image} alt={article.title} />
                                    </CardMedia>
                                    <CardTitle title={article.title} subtitle={article.category} />
                                    <CardText>
                                        {newDescription}
                                    </CardText>
                                    <CardActions>
                                        <FlatButton
                                            label="Ver Articulo"
                                            fullWidth={true}
                                            backgroundColor={this.props.page.buttonColor}
                                            labelStyle={{
                                                color: 'white'
                                            }}
                                            onClick={this.goArticle.bind(this, articleUrl)}
                                        />
                                    </CardActions>
                                </Card>
                            </Col>
                        )
                    })}
                </Row>
            </Grid>
        );
    }
}

Blog.PropTypes = {
    getArticleList: PropTypes.func.isRequired
};
Blog.contextTypes = {
    router: React.PropTypes.object.isRequired
};

function mapStateToProps(state){
    return {
        page: state.page.page
    }
}

export default connect(mapStateToProps, {getArticleList})(Blog);
