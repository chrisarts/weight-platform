import React from 'react';
import SignUpForm from './SignUpForm';


class SignUpPage extends React.Component{

    render(){
        return(
            <div className="login-front-container">
                <SignUpForm/>
            </div>
        );
    }
}

export default SignUpPage;