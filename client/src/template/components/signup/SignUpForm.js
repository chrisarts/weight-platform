import React from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { connect } from 'react-redux';
import { signup } from '../../../actions/authActions';

import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';

function validateInput(data){
    let errors = {};

    if(Validator.isEmpty(data.name)){
        errors.name = 'Este campo es obligatorio'
    }

    if(Validator.isEmpty(data.email)){
        errors.email = 'Este campo es obligatorio'
    }

    if(Validator.isEmpty(data.password)){
        errors.password = 'Este campo es obligatorio';
    }
    if(Validator.isEmpty(data.confirmPassword)){
        errors.confirmPassword = 'Este campo es obligatorio';
    }

    if(data.password !== data.confirmPassword){
        errors.password = 'Las contraseñas no coinciden';
        errors.confirmPassword = 'Las contraseñas no coinciden';
    }

    return {
        errors,
        isValid: isEmpty(errors)
    };
}

class SignUpForm extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            name: '',
            email: '',
            password: '',
            gender: '',
            birthDate: '2000-01-01',
            telephone: '',
            city: '',
            isAdmin: false,
            isSuperAdmin: false,
            confirmPassword: '',
            errors: {},
            isLoading: false
        };
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(event){
        this.setState({ [event.target.name]: event.target.value })
    }

    isValid(){
        const { errors, isValid } = validateInput(this.state);

        if(!isValid){
            this.setState({ errors });
        }

        return isValid;
    }

    onSubmit(event){
        event.preventDefault();
        if(this.isValid()){
            this.setState({
                errors: {},
                isLoading: true
            });
            this.props.signup(this.state).then(
                (res) => window.location.href='/',
                (err) => this.setState({ errors: err.response.data.errors, isLoading: false })
            );
        }
    }

    render(){
        const { errors, identifier } = this.state;
        return(
            <form onSubmit={this.onSubmit}>
                <hgroup>
                    <h1>Registrate</h1>
                    { errors.form && <h4>{errors.form}</h4> }
                </hgroup>
                <TextField
                    name="name"
                    floatingLabelText="Nombre del usuario"
                    value={identifier}
                    errorText={errors.name}
                    onChange={this.onChange}
                />
                <TextField
                    name="email"
                    floatingLabelText="Correo electrónico"
                    value={identifier}
                    errorText={errors.email}
                    onChange={this.onChange}
                />
                <TextField
                    name="password"
                    floatingLabelText="Contraseña"
                    value={this.state.password}
                    errorText={errors.password}
                    onChange={this.onChange}
                    type="password"
                />
                <TextField
                    name="confirmPassword"
                    floatingLabelText="Confirma Contraseña"
                    value={this.state.confirmPassword}
                    errorText={errors.password}
                    onChange={this.onChange}
                    type="password"
                />
                <RaisedButton
                    label="Registrarme"
                    type="submit"
                />
            </form>
        );
    }
}

SignUpForm.PropTypes = {
    signup: React.PropTypes.func.isRequired
};

SignUpForm.contextTypes = {
    router: React.PropTypes.object.isRequired
};

export default connect(null, { signup })(SignUpForm);