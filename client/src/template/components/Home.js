import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Helmet} from 'react-helmet';

import Slider from './commons/Slider';
import ProductsGrid from './commons/ProductsGrid';

//import Data
import {getArticleListLimit} from '../../actions/articleActions';

class Home extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            slides: [],
            articles: []
        };
        this.sliderStyles = {
            width: '100%'
        };
    }

    componentDidMount(){
        let This = this;
        This.props.getArticleListLimit(3).then(result => {
            //console.log('Articles: ', result);
        });
    }

    render(){
        return(
            <div className="page-home">
                <Helmet>
                    <title>Contacto</title>
                </Helmet>
                <Slider
                    slicks={this.state.slides}
                    settings={{
                        dots: true,
                        infinite: true,
                        speed: 500,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        style: this.sliderStyles
                    }}
                />
                <ProductsGrid pageSize={3}/>
            </div>
        );
    }
}

Home.PropTypes = {
    getArticleListLimit: PropTypes.func.isRequired
};

export default connect(null, {getArticleListLimit})(Home);
