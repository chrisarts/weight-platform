import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import {Col} from 'react-bootstrap';
import Slider from 'react-slick';

import LoadingSection from '../../../globalComponents/loading/LoadingSection';
import {getProductList} from '../../../actions/productActions';


class ProductsGrid extends React.Component{

    constructor(props){
        super(props);
        let slidesToRender = 3;
        if(window.innerWidth <= 1024 && window.innerWidth > 720){
            slidesToRender = 2;
        }else if(window.innerWidth <= 720){
            slidesToRender = 1;
        }
        this.state = {
            products: [],
            pagination: {
                pageSize: this.props.pageSize,
                page: this.props.page.name
            },
            settings: {
                dots: true,
                infinite: true,
                speed: 500,
                slidesToShow: slidesToRender,
                slidesToScroll: slidesToRender
            },
            slicks: [],
            slickKey: 0,
            isLoading: true
        }
    }

    componentDidMount(){
        let This = this;
        This.props.getProductList(This.state.pagination).then(result => {
            This.setState({
                products: result.data.filteredProducts,
                isLoading: false
            });
            if(result.data.filteredProducts.length > 0){
                This.slickMaker();
            }
        });

    }

    slickMaker(){
        let This = this;
        let responsive = window.innerWidth < 1024;
        This.state.products.map(product => {
            let newSlick = This.state.slicks;
            let urlName = product.name.replace(' ','-');
            let media = JSON.parse(product.media);
            let image = '';
            let count = 0;
            for (let key in media) {
                if (media.hasOwnProperty(key) && count === 0) {
                    image = media[key];
                    ++count;
                }
            }
            let productWidth = 4;
            if(This.state.settings.slidesToShow === 1){
                productWidth = 12;
            }else if(This.state.settings.slidesToShow === 2) {
                productWidth = 6;
            }else{
                productWidth = 4;
            }
            newSlick.push(
                <Col md={productWidth} key={product.id}>
                    <figure style={responsive ? {
                        width: '90%'
                    } : {
                        position: 'relative'
                    }}>
                        <img
                            src={image}
                            alt=""
                            style={responsive ? {
                                height: 250,
                                width: 'auto',
                                maxWidth: window.innerWidth-90,
                                margin: '0 auto'
                            } : {
                                height: 350,
                                width: 'auto',
                                margin: '0 auto'
                            }}
                        />
                    </figure>
                    <h4 style={{textAlign:'center',fontWeight:'bold'}}>{product.name}</h4>
                    <h5
                        className="price"
                        style={{width:'100%',textAlign:'center',color:'#8F9194',fontWeight:'bold',fontFamily:'bebas-neue',fontSize:25}}
                    >
                        ${product.price}
                        </h5>
                    <div
                        className="product-item-buttons"
                    >
                        <Link
                            to={`/producto/${urlName}/${product.id}`}
                            className={`product-link ${this.props.page.name}`}
                        >
                            Ver Producto
                        </Link>
                    </div>
                </Col>
            );
            This.setState({
                slicks: newSlick,
                slickKey: This.state.slickKey++
            });
        })
    }

    render(){
        let page = this.props.page;
        let slicks = [];
        if(this.state.products.length > 0){
            slicks = this.state.slicks;
        }else{
            slicks.push(<div key={999999}>&nbsp;</div>);
        }
        if(this.state.isLoading){
            return(
                <LoadingSection
                    payload={this.state.isLoading}
                    height="576px"
                    width="100%"
                    text="Cargando Productos..."
                />
            )
        }
        if(this.state.slicks.length > 0){
            return(
                <Slider {...this.state.settings} className="products-carousel">
                    {slicks}
                </Slider>
            );
        }else{
            return(
                <div>
                    <h2
                        style={{
                            textAlign: 'center',
                            padding: 50
                        }}
                    >
                        No existen articulos aún.
                    </h2>
                </div>
            );
        }
    }
}

ProductsGrid.PropTypes = {
    getProductList: PropTypes.func.isRequired,
    pageSize: PropTypes.number.isRequired
};

function mapStateToProps(state){
    return {
        auth: state.auth,
        page: state.page.page
    }
}

export default connect(mapStateToProps, {getProductList})(ProductsGrid);