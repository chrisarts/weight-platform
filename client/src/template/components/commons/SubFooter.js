import React from 'react';
import {connect} from 'react-redux';

class SubFooter extends React.Component{

    constructor(props){
        super(props);
        this.styles = {
            padding: '24px 21%',
            backgroundColor: this.props.page.footerBackground,
            fontSize: 12
        };
    }

    render(){
        let page = this.props.page;
        return(
            <div style={this.styles} className={`subfooter ${page.name}`}>
                Las declaraciones aquí manifestadas no han sido evaluadas por el ministerio de salud u
                organismo de control de drogas. Con estos productos no se intenta diagnosticar, tratar, curar
                ó prevenir ninguna enfermedad. Lea todas las etiquetas contenidas en el producto antes de usarlo.
                El logo de Facebook pertenece a Facebook Inc. Apple y el logo de Apple son marcas registradas de Apple Inc.
                registrados en U.S. y otros paises. App Store es un servicio registrado por Apple Inc., registrado en U.S.
                y otros paises. Google Play y su logo son marcas registradas de Google Inc. © 2017.
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        page: state.page.page
    }
}

export default connect(mapStateToProps, {})(SubFooter);