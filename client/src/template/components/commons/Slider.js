import React from 'react';
import {connect} from 'react-redux';
import Carousel from 'react-bootstrap/lib/Carousel';
import PropTypes from 'prop-types';

class Slider extends React.Component{

    constructor(props){
        super(props);
        let This = this;
        This.settings = {};
        if(This.props.settings){
            This.settings = This.props.settings;
        }else{
            This.settings = {
                dots: true,
                infinite: true,
                speed: 500,
                slidesToShow: 1,
                slidesToScroll: 1
            };
        }
        This.state = {
            slicks: [],
            slickKey: 0
        };
        This.styles = {
            carousel: {
                overflow: 'hidden'
            },
            image: {
                width: '100%'
            }
        }
    }

    componentDidMount(){
        let This = this;
        This.props.slicks.map(slick => This.slickMaker(slick));
    }

    slickMaker(slick){
        let This = this;
        let newSlicks = This.state.slicks;
        let page = This.props.page;
        newSlicks.push(
                <Carousel.Item 
                    animateIn={true} 
                    animateOut={true}
                    key={This.state.slickKey}
                >
                    <img src={slick.imageSrc} alt={`${page.name} Banner`}/>
                </Carousel.Item>);
        This.setState({
            slickKey: This.state.slickKey++,
            slicks: newSlicks
        });
    }

    render(){
        let page = this.props.page;
        return(
            <Carousel
                style={this.styles.carousel}
                controls={false}
                indicators={false}
            >
                <Carousel.Item
                    animateIn={true}
                    animateOut={true}
                >
                    <img
                        style={this.styles.image}
                        src={`/images/${page.name}/homeBanner.jpg`} alt={`${page.name} Banner`}/>
                </Carousel.Item>
            </Carousel>
        );
    }
}

Slider.PropTypes = {
    settings: PropTypes.object,
    slicks: PropTypes.array.isRequired
};

function mapStateToProps(state){
    return {
        page: state.page.page
    }
}

export default connect(mapStateToProps, {})(Slider);