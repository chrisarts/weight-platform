import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import {Grid, Col} from 'react-bootstrap';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';

class Footer extends React.Component{

    constructor(props){
        super(props);
        this.styles = {
            global: {
                background: this.props.page.footerBackground,
                fontSize: 14,
                paddingTop: 25
            },
            grid: {
                margin: '0 auto',
                width: '90%'
            },
            socialIcons: {
                marginLeft: 8
            }
        };
    }

    render(){
        let page = this.props.page;
        return(
            <div className="footer" style={this.styles.global}>
                <Grid style={this.styles.grid}>
                    <Col md={3} className="footer-block">
                        <h4>Mapa del Sitio</h4>
                        <ul className={`footer-list ${page.name}`}>
                            <li>
                                <Link
                                    to="/"
                                >
                                    Home
                                </Link>
                            </li>
                            <li>
                                <Link
                                    to={`/sobre-${page.name}`}
                                >
                                    Sobre Fit+
                                </Link>
                            </li>
                            <li>
                                <Link
                                    to="/productos"
                                >
                                    Tienda
                                </Link>
                            </li>
                            <li>
                                <Link
                                    to="/blog"
                                >
                                    Blog
                                </Link>
                            </li>
                            <li>
                                <Link
                                    to="/contacto"
                                >
                                    Contacto
                                </Link>
                            </li>
                        </ul>
                    </Col>
                    <Col md={3} className="footer-block">
                        <h4>Información vital</h4>
                        <ul className={`footer-list ${page.name}`}>
                            <li>
                                <a href="/devoluciones">Politicas de devolución</a>
                            </li>
                        </ul>
                    </Col>
                    <Col md={3} className="footer-block">
                        <h4>Ubicación</h4>
                        <ul className={`footer-list ${page.name}`}>
                            <li>
                                <a href="#">Cali, Colombia</a>
                            </li>
                            <li>
                                <a href="#">Bogotá, Colombia</a>
                            </li>
                        </ul>
                        <div>
                            <TextField
                                hintText="Buscar en este sitio"
                                floatingLabelText="¿Buscas algo?"
                            />
                            <IconButton iconClassName="muidocs-icon-custom-github" />
                        </div>
                    </Col>
                    <Col md={3} className="footer-block">
                        <h4>Siguenos</h4>
                        <div className="social-icons">
                            <a
                                href="#"
                                style={this.styles.socialIcons}
                            >
                                <FontIcon
                                    className="fa fa-facebook"
                                    color="#777777"
                                />
                            </a>
                            <a
                                href="#"
                                style={this.styles.socialIcons}
                            >
                                <FontIcon
                                    className="fa fa-google-plus"
                                    color="#777777"
                                />
                            </a>
                            <a
                                href="#"
                                style={this.styles.socialIcons}
                            >
                                <FontIcon
                                    className="fa fa-twitter"
                                    color="#777777"
                                />
                            </a>
                            <a
                                href="#"
                                style={this.styles.socialIcons}
                            >
                                <FontIcon
                                    className="fa fa-instagram"
                                    color="#777777"
                                />
                            </a>
                            <a
                                href="#"
                                style={this.styles.socialIcons}
                            >
                                <FontIcon
                                    className="fa fa-pinterest-p"
                                    color="#777777"
                                />
                            </a>

                        </div>
                    </Col>
                </Grid>
            </div>
        );
    }
}

function mapStateToProps(state){
    return {
        auth: state.auth,
        page: state.page.page
    }
}

export default connect(mapStateToProps,{})(Footer);
