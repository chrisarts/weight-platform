import React from 'react';

class MoreInfo extends React.Component{

    constructor(props){
        super(props);
    }

    render(){
        return(
            <div className="home-app">
                <div className="text">
                    <h3>Descarga nuestra aplicación</h3>
                    <p>
                        Nuestra aplicación contiene herramientras extra que te ayudarán a cumplir tus metas de reducción de peso.
                        Este anuncio puede contener información sobre ventas ó redes sociales.
                    </p>
                    <div className="app-stores">
                        <img src="https://www.hydroxycut.com/wp-content/uploads/download-apple-btn.png" alt=""/>
                        <img src="https://www.hydroxycut.com/wp-content/uploads/download-google-btn.png" alt=""/>
                    </div>
                </div>
                <div className="image">
                    <figure>
                        <img src={require('../images/health-app.jpg')} alt="Health App"/>
                    </figure>
                </div>
            </div>
        );
    }
}

export default MoreInfo;