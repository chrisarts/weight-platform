import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';

class ArticlesCarousel extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            settings: {
                dots: true,
                infinite: true,
                speed: 500,
                slidesToShow: 3,
                slidesToScroll: 3
            },
            slicks: [],
            slickKey: 0
        };
    }

    componentDidMount(){
        if(this.props.articles.length > 0){
            this.slickMaker();
        }
    }

    slickMaker(){
        let This = this;
        This.props.articles.map(article => {
            let newSlick = This.state.slicks;
            newSlick.push(
                <div key={This.state.slickKey} className="article-carousel-item">
                    <figure>
                        <img src={article.image} alt={article.title}/>
                    </figure>
                    <h5>{article.title}</h5>
                    <p>{article.text}</p>
                </div>
            );
            This.setState({
                slicks: newSlick,
                slickKey: This.state.slickKey++
            });
        })
    }

    render(){
        let slicks = [];
        if(this.state.slicks.length > 0){
            slicks = this.state.slicks;
        }else{
            slicks.push(<div key={999999}>&nbsp;</div>);
        }
        if(this.props.articles.length > 0){
            return(
                <Slider {...this.state.settings} className="articles-carousel">
                    {slicks}
                </Slider>
            );
        }else{
            return(
                <div>
                    <h2
                        style={{
                            textAlign: 'center',
                            padding: 50
                        }}
                    >
                        No existen articulos aún.
                    </h2>
                </div>
            );
        }
    }
}

ArticlesCarousel.PropTypes = {
    articles: PropTypes.array.isRequired
};

export default ArticlesCarousel;