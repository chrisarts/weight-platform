import React from 'react';
import {connect} from 'react-redux';
import {Link, browserHistory} from 'react-router';
import MenuItem from 'material-ui/MenuItem';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import {Toolbar, ToolbarGroup} from 'material-ui/Toolbar';
import {
    ActionShoppingBasket,
    ActionAccountCircle,
    NavigationMenu,
    ActionHome,
    ActionPermPhoneMsg,
    CommunicationRssFeed,
    MapsStoreMallDirectory,
    ActionInfo,
    ActionShoppingCart,
    PlacesAirportShuttle
} from 'material-ui/svg-icons';
import jwtDecode from 'jwt-decode';

import {logout} from '../../../actions/authActions';
import {getBasket} from '../../../actions/userActions';

class AppToolbar extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            toolbarStyle: {
                backgroundColor: 'rgba(0,0,0,0.5)',
                position: 'fixed',
                top: 0,
                width: '100%',
                height: 56,
                zIndex: 10
            },
            currentRoute: '/',
            isAuth: false,
            basketCount: 0,
            user: {},
            menuPadding: false,
            drawerOpen: false
        };
        this.styles = {
            menuItem: {
                display: 'block',
                width: '100%',
                padding: '10px 0',
                color: '#666666',
                textDecoration: 'none',
                paddingLeft: '10%'
            },
            responsiveMenuItem: {
                position: 'relative',
                top: 6,
                marginRight: 10
            }
        };
    }

    componentDidMount(){
        let This = this;
        window.addEventListener('scroll', this.handleScroll.bind(this));
        if(This.props.auth.isAuthenticated){
            let decodedToken = jwtDecode(localStorage.jwtToken);
            This.setState({
                isAuth: true,
                user: decodedToken
            });
            This.props.getBasket(decodedToken.id).then(result => {
                This.setState({
                    basketCount: result.data.basket.length,
                    currentRoute: This.context.router.location.pathname
                })
            });
        }
        browserHistory.listen( location =>  {
            document.body.scrollTop= 0;
            window.pageYOffset = 0;
            if(This.props.auth.isAuthenticated){
                let decodedToken = jwtDecode(localStorage.jwtToken);
                This.setState({
                    isAuth: true,
                    user: decodedToken
                });
            }
        });
    }

    componentWillUnmount(){
        window.removeEventListener('scroll', this.handleScroll.bind(this));
    }

    handleScroll(event){
        let newToolbarStyle = this.state.toolbarStyle;
        if(event.target.body.scrollTop > 150){
            newToolbarStyle.backgroundColor = '#1a1a1a';
            newToolbarStyle.height = 70;
            this.setState({
                toolbarStyle: newToolbarStyle,
                menuPadding: true
            });
        }else{
            newToolbarStyle.backgroundColor = 'rgba(0,0,0,0.5)';
            newToolbarStyle.height = 56;
            this.setState({
                toolbarStyle: newToolbarStyle,
                menuPadding: false
            });
        }
    }

    routeChange(e){
        e.preventDefault();
        let linkTo = e.target.parentNode.getAttribute('href');
        this.setState({
            currentRoute: linkTo
        });
        this.context.router.push(linkTo);
    }

    drawerToggle = () => {
        this.setState({
            drawerOpen: !this.state.drawerOpen
        });
        console.log('click!');
    };

    render(){
        let page  = this.props.page;
        let windowWith = window.innerWidth;
        if(windowWith <= 1224){
            return(
                <div>
                    <Drawer
                        open={this.state.drawerOpen}
                    >
                        <AppBar
                            className={`app-toolbar-${page.name}`}
                            style={this.state.toolbarStyle}
                            title={
                                <img
                                    style={{height: '81%', padding: '13px 0px'}}
                                    src={`/images/${page.name}/logo.png`} alt={page.trueName}/>
                            }
                            iconElementLeft={
                                <IconButton
                                    tooltip="Abrir menú"
                                    onClick={() => this.drawerToggle()}
                                >
                                    <NavigationMenu/>
                                </IconButton>
                            }
                        />
                        <div
                            style={{
                                marginTop: 70
                            }}
                        >
                            <Link
                                to="/"
                                className="menu-item-animated-background"
                                style={this.styles.menuItem}
                            >
                                <ActionHome
                                    style={this.styles.responsiveMenuItem}
                                    color="#666666"
                                /> <span>Home</span>
                            </Link>
                            <Link
                                to={`/sobre-${page.name}`}
                                className="menu-item-animated-background"
                                style={this.styles.menuItem}
                            >
                                <ActionInfo
                                    style={this.styles.responsiveMenuItem}
                                    color="#666666"
                                /> <span>Sobre {page.trueName}</span>
                            </Link>
                            <Link
                                to="/productos"
                                className="menu-item-animated-background"
                                style={this.styles.menuItem}
                            >
                                <MapsStoreMallDirectory
                                    style={this.styles.responsiveMenuItem}
                                    color="#666666"
                                /> <span>Tienda</span>
                            </Link>
                            <Link
                                to="/blog"
                                className="menu-item-animated-background"
                                style={this.styles.menuItem}
                            >
                                <CommunicationRssFeed
                                    style={this.styles.responsiveMenuItem}
                                    color="#666666"
                                /> <span>Blog</span>
                            </Link>
                            <Link
                                to="/contacto"
                                className="menu-item-animated-background"
                                style={this.styles.menuItem}
                            >
                                <ActionPermPhoneMsg
                                    style={this.styles.responsiveMenuItem}
                                    color="#666666"
                                /> <span>Contáctanos</span>
                            </Link>
                            {
                                this.state.isAuth ? (
                                    <div>
                                        <Link
                                            to="/carrito"
                                            className="menu-item-animated-background"
                                            style={this.styles.menuItem}
                                        >
                                            <ActionShoppingCart
                                                style={this.styles.responsiveMenuItem}
                                                color="#666666"
                                            /> <span>Carrito (0)</span>
                                        </Link>
                                        <Link
                                            to="/pedidos"
                                            className="menu-item-animated-background"
                                            style={this.styles.menuItem}
                                        >
                                            <PlacesAirportShuttle
                                                style={this.styles.responsiveMenuItem}
                                                color="#666666"
                                            /> <span>Mis Pedidos</span>
                                        </Link>
                                    </div>
                                ) : (
                                    <div>
                                        <MenuItem>Iniciar Sesión</MenuItem>
                                        <MenuItem>Registrarme!</MenuItem>
                                    </div>
                                )
                            }
                        </div>
                    </Drawer>
                    <AppBar
                        className={`app-toolbar-${page.name}`}
                        style={this.state.toolbarStyle}
                        title={
                            <img
                                style={{height: '81%', padding: '13px 0px'}}
                                src={`/images/${page.name}/logo.png`} alt={page.trueName}/>
                        }
                        iconElementLeft={
                            <IconButton
                                tooltip="Abrir menú"
                                onClick={() => this.drawerToggle()}
                            >
                                <NavigationMenu/>
                            </IconButton>
                        }
                    />
                </div>
            )
        }else{
            return(
                <div>
                    <Toolbar
                        className={`app-toolbar-${page.name}`}
                        style={this.state.toolbarStyle}
                    >
                        <ToolbarGroup firstChild={true}>
                            <figure className="page-logo">
                                <img
                                    style={{height: '100%'}}
                                    src={`/images/${page.name}/logo.png`} alt={page.trueName}/>
                            </figure>
                        </ToolbarGroup>
                        <ToolbarGroup>
                            <div
                                className={`main-menu ${page.name}`}
                            >
                                <Link
                                    to="/"
                                    className={(this.state.menuPadding ? "menu-link-padding" : "menu-link") + " " + page.name}
                                    activeClassName="active"
                                    activeStyle={(this.state.currentRoute) === '/' ?
                                        {color: "white"}
                                        :
                                        {color: page.letterColor1}
                                    }
                                    onClick={this.routeChange.bind(this)}
                                >
                                    <span>Home</span>
                                </Link>
                                <Link
                                    to={`/sobre-${page.name}`}
                                    className={(this.state.menuPadding ? "menu-link-padding" : "menu-link") + " " + page.name}
                                    activeClassName="active"
                                    onClick={this.routeChange.bind(this)}
                                >
                                    <span>Sobre {page.trueName}</span>
                                </Link>
                                <Link
                                    to="/productos"
                                    className={(this.state.menuPadding ? "menu-link-padding" : "menu-link") + " " + page.name}
                                    activeClassName="active"
                                    onClick={this.routeChange.bind(this)}
                                >
                                    <span>Tienda</span>
                                </Link>
                                <Link
                                    to="/blog"
                                    className={(this.state.menuPadding ? "menu-link-padding" : "menu-link") + " " + page.name}
                                    activeClassName="active"
                                    onClick={this.routeChange.bind(this)}
                                >
                                    <span>Blog</span>
                                </Link>
                                <Link
                                    to="/contacto"
                                    className={(this.state.menuPadding ? "menu-link-padding" : "menu-link") + " " + page.name}
                                    activeClassName="active"
                                    onClick={this.routeChange.bind(this)}
                                >
                                    <span>Contacto</span>
                                </Link>
                            </div>
                        </ToolbarGroup>
                        <ToolbarGroup
                            style={{
                                width: '18%'
                            }}
                        >
                            <div className="user-menu">
                                {
                                    this.state.isAuth ? (
                                        <Link
                                            to="/carrito"
                                            onChange={this.routeChange.bind(this)}
                                            className={`menu-link-basket ${page.name}`}
                                        >
                                            <div
                                                className={(this.state.menuPadding ? "basket-padding" : "basket-nopadding") + " " + page.name}
                                            >
                                                Ver Carrito ({this.state.basketCount})
                                                <ActionShoppingBasket
                                                    style={{
                                                        marginLeft: 10,
                                                        color: this.props.page.letterColor1
                                                    }}
                                                />
                                            </div>
                                        </Link>
                                    ) : (
                                        <div style={{float: 'left', display: 'none'}}>&nbsp;</div>
                                    )
                                }
                                <div className="profile-menu">
                                    {
                                        this.state.isAuth ? (
                                            <IconMenu
                                                iconButtonElement={
                                                    <IconButton
                                                        iconStyle={{
                                                            color: this.props.page.letterColor1
                                                        }}
                                                    >
                                                        <ActionAccountCircle/>
                                                    </IconButton>
                                                }
                                                style={{position:'absolute'}}
                                                anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
                                                targetOrigin={{horizontal: 'right', vertical: 'top'}}
                                                className={(this.state.menuPadding ? "icon-profile-menu-padding" : "icon-profile-menu") + " " + this.props.page.name}
                                            >
                                                <MenuItem primaryText="Ver Perfil" />
                                            </IconMenu>
                                        ) : (
                                            <div className="signup-menu">
                                                <Link
                                                    to="/registrate"
                                                    onChange={this.routeChange.bind(this)}
                                                    className={(this.state.menuPadding ? "menu-link-padding" : "menu-link") + " " + this.props.page.name}
                                                >
                                                    <span>Registrate!</span>
                                                </Link>
                                                <Link
                                                    to="/iniciar-sesion"
                                                    onChange={this.routeChange.bind(this)}
                                                    className={(this.state.menuPadding ? "menu-link-padding" : "menu-link") + " " + this.props.page.name}
                                                >
                                                    <span>Iniciar sesión!</span>
                                                </Link>
                                            </div>
                                        )
                                    }
                                </div>
                            </div>
                        </ToolbarGroup>
                    </Toolbar>
                </div>
            )
        }
    }
}

AppToolbar.PropTypes = {
    getBasket: React.PropTypes.func.isRequired,
    auth: React.PropTypes.object.isRequired,
    logout: React.PropTypes.func.isRequired
};

AppToolbar.contextTypes = {
    router: React.PropTypes.object.isRequired
};

function mapStateToProps(state){
    return {
        auth: state.auth,
        page: state.page.page
    }
}

export default connect(mapStateToProps, {logout,getBasket})(AppToolbar);
