import React from 'react'
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import ReactHtmlParser from 'react-html-parser';

import {fetchArticle} from '../../actions/articleActions';

class Article extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            article: {},
            image: '',
            content: (<div>&nbsp;</div>)
        }
    }

    componentDidMount(){
        let This = this;
        let fetchArticle = This.props.fetchArticle(This.props.params.articleId);
        console.log(fetchArticle);
        /*This.props.getArticle({ id: This.props.params.articleId }).then(result => {
            let article = result.data.article;
            let content = ReactHtmlParser(article.content);
            let media = JSON.parse(article.media);
            let count = 0;
            let image = '';
            for (let key in media) {
                if (media.hasOwnProperty(key) && count === 0) {
                    image = media[key];
                    ++count;
                }
            }
            This.setState({
                article,
                image,
                content
            });
        });*/
    }

    render(){
        return(
            <div className="article-page">
                <figure>
                    <img src={this.state.image} alt={this.state.article.title}/>
                </figure>
                <h1>{this.state.article.title}</h1>
                <div>
                    {this.state.content}
                </div>
            </div>
        )
    }
}

Article.PropTypes = {
    fetchArticle: PropTypes.func.isRequired
};

function mapStateToProps(state){
    return {
        page: state.page.page
    }
}

export default connect(mapStateToProps, {fetchArticle})(Article);