import { SET_CURRENT_PAGE } from '../actions/types';
import currentPage from '../utils/pages';

// Set the initial state for the localStorage
const initialState = {
    page: currentPage
};

// Export this function to handle the action dispatcher and update the redux state
export default (state = initialState, action = {}) => {
    switch (action.type){
        case SET_CURRENT_PAGE:
            return {
                page: action.page
            };
        default: return state;
    }
}