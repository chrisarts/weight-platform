import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import AppBar from 'material-ui/AppBar';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import {
    ContentClear,
    ContentCreate
} from 'material-ui/svg-icons';

import LoadingSection from '../../globalComponents/loading/LoadingSection';
import { getProductList, deleteProduct } from '../../actions/productActions';

import Navigation from '../Navigation';

class ProductsList extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            products: [],
            pagination: {
                pageSize: 20,
                currentPage: 1,
                page: this.props.page.name
            },
            deleteOpen: false,
            isLoading: true,
            productToDelete: -1
        };
        this.styles = {
            deleteActions: {
                float: 'right',
                buttons: {
                    marginRight: 20
                }
            }
        };
    }

    componentDidMount(){
        let This = this;
        this.props.getProductList(This.state.pagination).then(result => {
            This.setState({
                products: result.data.filteredProducts,
                isLoading: false
            });
        })
    };

    handleDeleteOpen(product){
        this.setState({
            deleteOpen: true,
            productToDelete: product
        })
    };

    deleteActionsPositive(){
        let This = this;
        This.props.deleteProduct({ id: This.state.productToDelete }).then( () => {
            This.props.getProductList(This.state.pagination).then(result => {
                This.setState({
                    deleteOpen: false,
                    products: result.data.filteredProducts
                });
            })

        });
    };

    deleteActionsNegative(){
        this.setState({
            deleteOpen: false
        });
    };

    render(){
        return(
            <div>
                <Navigation/>
                <div className="admin-page">
                    <AppBar
                        title="Panel de Administración"
                        className={`app-toolbar-${this.props.page.name}`}
                    />
                    <h1>Lista de Productos</h1>
                    <LoadingSection
                        payload={this.state.isLoading}
                        width="100%"
                        height="300px"
                    />
                    <Table>
                        <TableHeader>
                            <TableRow>
                                <TableHeaderColumn>ID</TableHeaderColumn>
                                <TableHeaderColumn>Nombre</TableHeaderColumn>
                                <TableHeaderColumn>Descripción</TableHeaderColumn>
                                <TableHeaderColumn>Estado</TableHeaderColumn>
                                <TableHeaderColumn>Editar</TableHeaderColumn>
                                <TableHeaderColumn>Eliminar</TableHeaderColumn>
                            </TableRow>
                        </TableHeader>
                        <TableBody>
                            {
                                this.state.products.map(product =>
                                    <TableRow key={product.id}>
                                        <TableRowColumn>{product.id}</TableRowColumn>
                                        <TableRowColumn>{product.name}</TableRowColumn>
                                        <TableRowColumn>{product.description.replace(/-/g, ' ')}</TableRowColumn>
                                        <TableRowColumn>{product.active ? 'Activo' : 'Inactivo'}</TableRowColumn>
                                        <TableRowColumn>
                                            <Link to={`/admin/products/${product.id}`}>
                                                <ContentCreate/>
                                            </Link>
                                        </TableRowColumn>
                                        <TableRowColumn>
                                            <Link to="/admin/products#" onClick={this.handleDeleteOpen.bind(this, product.id)}>
                                                <ContentClear/>
                                            </Link>
                                            <Dialog
                                                title="¿Estás seguro de querer eliminar éste producto?"
                                                open={this.state.deleteOpen}
                                            >
                                                <div
                                                    className="delete-actions"
                                                    style={this.styles.deleteActions}
                                                >
                                                    <FlatButton
                                                        label="No Eliminar"
                                                        onClick={this.deleteActionsNegative.bind(this)}
                                                        backgroundColor="#a4c639"
                                                        hoverColor="#8AA62F"
                                                        style={this.styles.deleteActions.buttons}
                                                    />
                                                    <FlatButton
                                                        label="Eliminar"
                                                        onClick={this.deleteActionsPositive.bind(this, product.id)}
                                                        backgroundColor="#9b2211"
                                                        hoverColor="#8b1e0f"
                                                        style={this.styles.deleteActions.buttons}
                                                    />
                                                </div>
                                            </Dialog>
                                        </TableRowColumn>
                                    </TableRow>
                                )
                            }
                        </TableBody>
                    </Table>
                </div>
            </div>
        )
    }
}

ProductsList.PropTypes = {
    getProductList: PropTypes.func.isRequired,
    deleteProduct: PropTypes.func.isRequired
};

function mapStateToProps(state){
    return {
        page: state.page.page
    }
}

export default connect(mapStateToProps, {getProductList, deleteProduct})(ProductsList);