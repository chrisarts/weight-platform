import React from 'react';
import { connect } from 'react-redux';
import AppBar from 'material-ui/AppBar';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import TinyMCE from 'react-tinymce';
import UploadPreview from 'material-ui-upload/UploadPreview';
import Toggle from 'material-ui/Toggle';


import Navigation from '../Navigation';

import { getArticle, updateArticle } from '../../actions/articleActions';

import './articles.css';

import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';

function validateInputEvent(data){
    let errors = {};

    if(Validator.isEmpty(data.title)){
        errors.title = 'Este campo es obligatorio'
    }
    if(Validator.isEmpty(data.content)){
        errors.content = 'Este campo es obligatorio'
    }

    return {
        errors,
        isValid: isEmpty(errors)
    };
}

class EditArticle extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            title: '',
            content: '',
            pictures: {},
            images: [],
            category: this.props.page.name,
            date: new Date(),
            outstanding: false,
            active: true,
            errors: {},
            isLoading: true
        }
    }

    componentDidMount(){
        let This = this;
        This.props.getArticle({id: This.props.params.identifier}).then(result => {
            console.log('Edit Article: ', result.data);
            let {
                title,
                content,
                media,
                category,
                date,
                outstanding,
                active
            } = result.data.article;
            media = JSON.parse(media);
            let count = 0;
            let images = [];
            for (let key in media) {
                if (media.hasOwnProperty(key) && count === 0) {
                    images.push(media[key]);
                    ++count;
                }
            }
            This.setState({
                title,
                content,
                media,
                category,
                date,
                outstanding,
                active,
                images,
                isLoading: false
            });
        });
    }

    isValid(){
        const { errors, isValid } = validateInputEvent(this.state);

        if(!isValid){
            this.setState({ errors: errors })
        }

        return isValid;
    }

    onChange(event){
        this.setState({ [event.target.name]: event.target.value });
    }

    handleEditorChange = (e) => {
        this.setState({
            content: e.target.getContent()
        })
    };

    onChangeMedia = (pictures) => {
        this.setState({ pictures });
        console.log(pictures);
    };

    onChangeCategory(event, index, value){
        this.setState({
            category: value
        })
    }

    onToggleActive(event, toggle){
        this.setState({
            active: toggle
        });
    }

    onSubmit(event){
        event.preventDefault();
        let This = this;
        if(This.isValid()){
            this.setState({ errors: {}, isLoading: true });
            console.log(this.state);
            this.props.updateArticle(this.state).then(
                () => {
                    This.context.router.push('/admin/home');
                },
                (err) => this.setState({ errors: err.response, isLoading: false })
            );
        }
    }

    render(){
        const { errors, isLoading } = this.state;
        let count = 0;
        return(
            <div>
                <Navigation/>
                <div className="admin-page">
                    <AppBar
                        title="Actualizar Artículo"
                        className={`app-toolbar-${this.props.page.name}`}
                    />
                    <form onSubmit={this.onSubmit.bind(this)}>
                        <TextField
                            name="title"
                            floatingLabelText="Título del Artículo *"
                            value={this.state.title}
                            errorText={errors.title}
                            onChange={this.onChange.bind(this)}
                        />
                        <TinyMCE
                            content={this.state.content}
                            config={{
                                plugins: 'link code',
                                toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
                            }}
                            onChange={this.handleEditorChange.bind(this)}
                            errorText={errors.content}
                        />
                        <figure
                            style={{
                                width: '100%'
                            }}
                        >
                            <h4>Imagenes Actuales</h4>
                            {
                                this.state.images.map(image => {
                                    ++count;
                                    return(
                                        <img
                                            key={count}
                                            src={image}
                                            alt=""
                                            style={{
                                                float: 'left',
                                                width: '30%'
                                            }}
                                        />
                                    )
                                })
                            }
                        </figure>
                        <div className="image-upload">
                            <UploadPreview
                                title="Nuevas Imágenes"
                                label="Añadir"
                                initialItems={this.state.pictures}
                                onChange={this.onChangeMedia}
                            />
                        </div>
                        <Toggle
                            label="¿Artículo activo?"
                            labelPosition="right"
                            defaultToggled={this.state.active}
                            onToggle={this.onToggleActive}
                        />
                        <RaisedButton
                            label="Actualizar Artículo"
                            type="submit"
                            disabled={isLoading}
                        />
                    </form>
                </div>
            </div>
        )
    }
}

EditArticle.PropTypes = {
    getArticle: React.PropTypes.func.isRequired,
    updateArticle: React.PropTypes.func.isRequired
};

EditArticle.contextTypes = {
    router: React.PropTypes.object.isRequired
};

function mapStateToProps(state){
    return {
        page: state.page.page
    }
}

export default connect(mapStateToProps, { getArticle, updateArticle })(EditArticle);