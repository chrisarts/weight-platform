import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import AppBar from 'material-ui/AppBar';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import TinyMCE from 'react-tinymce';
import UploadPreview from 'material-ui-upload/UploadPreview';
import Toggle from 'material-ui/Toggle';
import Navigation from '../Navigation';
import {
    ActionNoteAdd,
    AvArtTrack
} from 'material-ui/svg-icons';

//Component Actions
import {getProduct,updateProduct} from '../../actions/productActions';

import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';

function validateInputEvent(data){
    let errors = {};

    if(Validator.isEmpty(data.name)){
        errors.name = 'Este campo es obligatorio'
    }
    if(Validator.isEmpty(data.description)){
        errors.description = 'Este campo es obligatorio'
    }

    if(Validator.isEmpty(data.price)){
        errors.price = 'Este campo es obligatorio'
    }

    return {
        errors,
        isValid: isEmpty(errors)
    };
}

class EditProduct extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            id: this.props.params.identifier,
            name: '',
            description: '',
            media: {},
            images: [],
            price: 0,
            page: this.props.page.name,
            active: false,
            isLoading: false,
            errors: {}
        };
    };

    componentDidMount(){
        let This = this;
        This.props.getProduct({ id: This.props.params.identifier }).then(result => {
            let media = JSON.parse(result.data.product.media);
            let count = 0;
            let images = [];
            for (let key in media) {
                if (media.hasOwnProperty(key) && count === 0) {
                    images.push(media[key]);
                    ++count;
                }
            }
            This.setState({
                name: result.data.product.name,
                description: result.data.product.description,
                media: media,
                images,
                price: result.data.product.price,
                page: result.data.product.page,
                active: result.data.product.active
            })
        });
    }

    isValid(){
        const { errors, isValid } = validateInputEvent(this.state);

        if(!isValid){
            this.setState({ errors: errors })
        }

        return isValid;
    };

    onChange(event){
        this.setState({ [event.target.name]: event.target.value });
    };

    handleEditorChange(e){
        this.setState({
            description: e.target.getContent()
        })
    };

    onChangeMedia(pictures){
        this.setState({ media: pictures });
        console.log(pictures);
    };

    onToggleActive(event, toggle){
        console.log(toggle);
        this.setState({
            active: toggle
        });
    };

    createProduct(e){
        e.preventDefault();
        let This = this;
        console.log(This.state);
        if(This.isValid()){
            This.setState({ errors: {}, isLoading: true });
            This.props.updateProduct(This.state).then(
                () => {
                    This.setState({isLoading: false});
                    This.context.router.push('/admin/products');
                },
                (err) => {
                    console.log('Current errors: ', err.response);
                    window.scrollTo(0, 0);
                    if(err.response.data.errors){
                        This.setState({ errors: err.response.data.errors, isLoading: false })
                    }else{
                        This.setState({ errors: err.response, isLoading: false });
                    }
                }
            );
        }
    };

    render(){
        const { errors, isLoading } = this.state;
        let count = 0;
        return(
            <div className="create-product">
                <Navigation/>
                <div className="admin-page">
                    <AppBar
                        title="Actualizar Producto"
                        className={`app-toolbar-${this.props.page.name}`}
                        titleStyle={{marginLeft: 5}}
                        iconElementLeft={
                            <ActionNoteAdd
                                color="white"
                                style={{
                                    width: 50,
                                    height: 50
                                }}
                            />
                        }
                    />
                    <form onSubmit={this.createProduct.bind(this)}>
                        <AppBar
                            title="Información del Producto"
                            style={{
                                height: 35
                            }}
                            titleStyle={{
                                marginLeft: 5,
                                fontSize: 15,
                                lineHeight: '37px'
                            }}
                            iconElementLeft={
                                <AvArtTrack
                                    color="white"
                                    style={{
                                        width: 30,
                                        height: 30
                                    }}
                                />
                            }
                        />
                        <div>
                            <TextField
                                name="name"
                                floatingLabelText="Nombre del Producto *"
                                value={this.state.name}
                                errorText={errors.name}
                                onChange={this.onChange.bind(this)}
                                style={{
                                    width: '100%'
                                }}
                            />
                            <TextField
                                name="price"
                                floatingLabelText="Precio del Producto *"
                                value={this.state.price}
                                errorText={errors.price}
                                onChange={this.onChange.bind(this)}
                                style={{
                                    width: '100%'
                                }}
                            />
                        </div>
                        {this.state.description !== '' ? (
                            <TinyMCE
                                content={this.state.description}
                                config={{
                                    plugins: 'link image code',
                                    toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
                                }}
                                onChange={this.handleEditorChange.bind(this)}
                                errorText={errors.description}
                            />
                        ) : (
                            <div>&nbsp;</div>
                        )
                        }
                        <figure
                            style={{
                                width: '100%'
                            }}
                        >
                            <h4>Imagenes Actuales</h4>
                            {
                                this.state.images.map(image => {
                                    ++count;
                                    return(
                                        <img
                                            key={count}
                                            src={image}
                                            alt=""
                                            style={{
                                                float: 'left',
                                                width: '30%',
                                                border: '1px solid'
                                            }}
                                        />
                                    )
                                })
                            }
                        </figure>
                        <div
                            className="image-upload"
                        >
                            <UploadPreview
                                title="Imágenes"
                                label="Añadir"
                                initialItems={this.state.media}
                                onChange={this.onChangeMedia.bind(this)}
                            />
                        </div>
                        <Toggle
                            label="¿Producto activo?"
                            labelPosition="right"
                            defaultToggled={this.state.active}
                            onToggle={this.onToggleActive.bind(this)}
                        />
                        <RaisedButton
                            label="Actualizar Producto"
                            type="submit"
                            disabled={isLoading}
                        />
                    </form>
                </div>
            </div>
        )
    }
}

EditProduct.PropTypes = {
    getProduct: PropTypes.func.isRequired,
    updateProduct: PropTypes.func.isRequired
};

EditProduct.contextTypes = {
    router: React.PropTypes.object.isRequired
};

function mapStateToProps(state){
    return {
        page: state.page.page
    }
}

export default connect(mapStateToProps, {getProduct,updateProduct})(EditProduct);