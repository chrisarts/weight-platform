import React from 'react';
import { connect } from 'react-redux';
import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import { List, ListItem } from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import FlatButton from 'material-ui/FlatButton';
import { Link } from 'react-router';
import {
    CommunicationImportContacts,
    ActionList,
    ContentCreate
} from 'material-ui/svg-icons';

import { logout } from '../actions/authActions';

class Navigation extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            drawer: true
        }
    }

    logout(event){
        event.preventDefault();
        this.props.logout();
    }

    render(){
        const userLinks = (
            <div>
                <Link to="/#">
                    <FlatButton
                        label="Salir"
                        onClick={this.logout.bind(this)}
                    />
                </Link>
            </div>
        );
        const page = this.props.page;
        return(
            <div>
                <Drawer open={this.state.drawer}>
                    <AppBar
                        title="Menú"
                        iconElementRight={userLinks}
                        className={`app-toolbar-${page.name}`}
                    />
                    <List>
                        <Subheader>Menú de administración</Subheader>
                        <ListItem
                            primaryText="Articulos"
                            leftIcon={<CommunicationImportContacts />}
                            initiallyOpen={true}
                            primaryTogglesNestedList={true}
                            nestedItems={[
                                <ListItem
                                    key={2}
                                    primaryText="Lista"
                                    leftIcon={<ActionList />}
                                    href="/admin/home"
                                />,
                                <ListItem
                                    key={3}
                                    primaryText="Crear"
                                    leftIcon={<ContentCreate />}
                                    href="/admin/articles/create"
                                />,
                            ]}
                        />
                        <ListItem
                            primaryText="Productos"
                            leftIcon={<CommunicationImportContacts />}
                            initiallyOpen={true}
                            primaryTogglesNestedList={true}
                            nestedItems={[
                                <ListItem
                                    key={2}
                                    primaryText="Lista"
                                    leftIcon={<ActionList />}
                                    href="/admin/products"
                                />,
                                <ListItem
                                    key={3}
                                    primaryText="Crear"
                                    leftIcon={<ContentCreate />}
                                    href="/admin/products/create"
                                />,
                            ]}
                        />
                    </List>
                </Drawer>
            </div>
        )
    }
}

Navigation.PropTypes = {
    logout: React.PropTypes.func.isRequired
};

function mapStateToProps(state){
    return {
        auth: state.auth,
        page: state.page.page
    }
}

export default connect(mapStateToProps, { logout })(Navigation);