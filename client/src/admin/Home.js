import React from 'react';
import {Link} from 'react-router';
import { connect } from 'react-redux';
import AppBar from 'material-ui/AppBar';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import  dateFormat from 'dateformat';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import {
    ContentClear,
    ContentCreate
} from 'material-ui/svg-icons';

import Navigation from './Navigation';
import LoadingSection from '../globalComponents/loading/LoadingSection';
import { getArticleList, deleteArticle } from '../actions/articleActions';

class Home extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            articles: [],
            category: this.props.page.name,
            pagination: {
                pageSize: 20,
                currentPage: 1,
                page: this.props.page.name
            },
            deleteOpen: false,
            isLoading: true
        };
        this.styles = {
            deleteActions: {
                float: 'right',
                buttons: {
                    marginRight: 20
                }
            }
        };
    }

    componentDidMount(){
        let This = this;
        this.props.getArticleList(This.state.pagination).then(result => {
            This.setState({
                articles: result.data.filteredArticles,
                isLoading: false
            });
        })
    }

    handleDeleteOpen(){
        this.setState({
            deleteOpen: true
        })
    };

    deleteActionsPositive(article){
        let This = this;
        This.setState({
            isLoading: true
        });
        This.props.deleteArticle({ id: article }).then( () => {
            This.props.getArticleList(This.state.pagination).then(result => {
                This.setState({
                    deleteOpen: false,
                    articles: result.data.filteredArticles
                });
            })

        });
    };

    deleteActionsNegative(){
        this.setState({
            deleteOpen: false
        });
    };

    render(){
        return(
            <div>
                <Navigation/>
                <div className="admin-page">
                    <AppBar
                        title="Panel de Administración"
                        className={`app-toolbar-${this.props.page.name}`}
                    />
                    <h1>Lista de Artículos</h1>
                    <LoadingSection
                        payload={this.state.isLoading}
                        width="100%"
                        height="300px"
                    />
                    <Table>
                        <TableHeader>
                            <TableRow>
                                <TableHeaderColumn>ID</TableHeaderColumn>
                                <TableHeaderColumn>Titulo</TableHeaderColumn>
                                <TableHeaderColumn>Categoría</TableHeaderColumn>
                                <TableHeaderColumn>Fecha</TableHeaderColumn>
                                <TableHeaderColumn>Estado</TableHeaderColumn>
                                <TableHeaderColumn>Editar</TableHeaderColumn>
                                <TableHeaderColumn>Borrar</TableHeaderColumn>
                            </TableRow>
                        </TableHeader>
                        <TableBody>
                            {
                                this.state.articles.map(article =>
                                    <TableRow key={article.id}>
                                        <TableRowColumn>{article.id}</TableRowColumn>
                                        <TableRowColumn>{article.title}</TableRowColumn>
                                        <TableRowColumn>{article.category.replace(/-/g, ' ')}</TableRowColumn>
                                        <TableRowColumn>{dateFormat(article.created_at, "dddd, mmmm dS, yyyy")}</TableRowColumn>
                                        <TableRowColumn>{article.active ? 'Activo' : 'Inactivo'}</TableRowColumn>
                                        <TableRowColumn>
                                            <Link to={`/admin/articles/${article.id}`}>
                                                <ContentCreate/>
                                            </Link>
                                        </TableRowColumn>
                                        <TableRowColumn>
                                            <Link to="/admin/home#" onClick={this.handleDeleteOpen.bind(this)}>
                                                <ContentClear/>
                                            </Link>
                                            <Dialog
                                                title="¿Estás seguro de querer eliminar éste artículo?"
                                                open={this.state.deleteOpen}
                                            >
                                                <div
                                                    className="delete-actions"
                                                    style={this.styles.deleteActions}
                                                >
                                                    <FlatButton
                                                        label="No Eliminar"
                                                        onClick={this.deleteActionsNegative.bind(this)}
                                                        backgroundColor="#a4c639"
                                                        hoverColor="#8AA62F"
                                                        style={this.styles.deleteActions.buttons}
                                                    />
                                                    <FlatButton
                                                        label="Eliminar"
                                                        onClick={this.deleteActionsPositive.bind(this, article.id)}
                                                        backgroundColor="#9b2211"
                                                        hoverColor="#8b1e0f"
                                                        style={this.styles.deleteActions.buttons}
                                                    />
                                                </div>
                                            </Dialog>
                                        </TableRowColumn>
                                    </TableRow>
                                )
                            }
                        </TableBody>
                    </Table>
                </div>
            </div>
        )
    }
}

Home.PropTypes = {
    getArticleList: React.PropTypes.func.isRequired,
    deleteArticle: React.PropTypes.func.isRequired
};

function mapStateToProps(state){
    return {
        page: state.page.page
    }
}

export default connect(mapStateToProps, { getArticleList, deleteArticle })(Home);
