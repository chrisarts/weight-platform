import fetch from 'isomorphic-fetch';

const getArticle = (id = -1) => {
    return fetch('/api/articles/get', id)
            .then(rawResponse => rawResponse.json())
}

export default { getArticle };