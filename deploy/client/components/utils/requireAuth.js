'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

exports.default = function (ComposedComponent) {
    // This is the High Component
    var Authenticate = function (_React$Component) {
        _inherits(Authenticate, _React$Component);

        function Authenticate() {
            _classCallCheck(this, Authenticate);

            return _possibleConstructorReturn(this, (Authenticate.__proto__ || Object.getPrototypeOf(Authenticate)).apply(this, arguments));
        }

        _createClass(Authenticate, [{
            key: 'componentWillMount',


            // Check the required props before the component get mount
            value: function componentWillMount() {
                if (!this.props.isAuthenticated) {
                    this.props.addFlashMessage({
                        type: 'error',
                        text: 'Debes loguearte para acceder a esta sección'
                    });
                    this.context.router.push('/admin/login');
                }
            }
            // When the user use the logout() dispatch this update

        }, {
            key: 'componentWillUpdate',
            value: function componentWillUpdate(nextProps) {
                if (!nextProps.isAuthenticated) {
                    this.context.router.push('/');
                }
            }
        }, {
            key: 'render',
            value: function render() {
                //Here we build the child component who will return the protected view
                return _react2.default.createElement(ComposedComponent, this.props);
            }
        }]);

        return Authenticate;
    }(_react2.default.Component);

    // Add the known props that we use for the high component


    Authenticate.PropTypes = {
        isAuthenticated: _propTypes2.default.bool.isRequired,
        addFlashMessage: _propTypes2.default.func.isRequired
    };

    Authenticate.contextTypes = {
        router: _propTypes2.default.object.isRequired
    };

    function mapStateToProps(state) {
        return {
            isAuthenticated: state.auth.isAuthenticated
        };
    }

    // Connect the High component to the global app
    return (0, _reactRedux.connect)(mapStateToProps, { addFlashMessage: _flashMessages.addFlashMessage })(Authenticate);
};

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = require('react-redux');

var _flashMessages = require('../actions/flashMessages');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// This function create a HOC (High-Order Component)
// This is for view protecting across the requireAuth function in the routes