'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _App = require('./template/components/App');

var _App2 = _interopRequireDefault(_App);

var _Home = require('./template/components/Home');

var _Home2 = _interopRequireDefault(_Home);

var _LoginFront = require('./template/components/login/LoginFront');

var _LoginFront2 = _interopRequireDefault(_LoginFront);

var _SignUpPage = require('./template/components/signup/SignUpPage');

var _SignUpPage2 = _interopRequireDefault(_SignUpPage);

var _Product = require('./template/components/Product');

var _Product2 = _interopRequireDefault(_Product);

var _Products = require('./template/components/Products');

var _Products2 = _interopRequireDefault(_Products);

var _AboutFitmas = require('./template/components/aboutComponents/AboutFitmas');

var _AboutFitmas2 = _interopRequireDefault(_AboutFitmas);

var _AboutLipoblue = require('./template/components/aboutComponents/AboutLipoblue');

var _AboutLipoblue2 = _interopRequireDefault(_AboutLipoblue);

var _Blog = require('./template/components/Blog');

var _Blog2 = _interopRequireDefault(_Blog);

var _Article = require('./template/components/Article');

var _Article2 = _interopRequireDefault(_Article);

var _Contact = require('./template/components/Contact');

var _Contact2 = _interopRequireDefault(_Contact);

var _Basket = require('./template/components/basket/Basket');

var _Basket2 = _interopRequireDefault(_Basket);

var _Payment = require('./template/components/payment/Payment');

var _Payment2 = _interopRequireDefault(_Payment);

var _requireAuth = require('./utils/requireAuth');

var _requireAuth2 = _interopRequireDefault(_requireAuth);

var _Admin = require('./admin/Admin');

var _Admin2 = _interopRequireDefault(_Admin);

var _Home3 = require('./admin/Home');

var _Home4 = _interopRequireDefault(_Home3);

var _LoginPage = require('./admin/login/LoginPage');

var _LoginPage2 = _interopRequireDefault(_LoginPage);

var _CreateArticle = require('./admin/articles/CreateArticle');

var _CreateArticle2 = _interopRequireDefault(_CreateArticle);

var _EditArticle = require('./admin/articles/EditArticle');

var _EditArticle2 = _interopRequireDefault(_EditArticle);

var _CreateProduct = require('./admin/products/CreateProduct');

var _CreateProduct2 = _interopRequireDefault(_CreateProduct);

var _ProductsList = require('./admin/products/ProductsList');

var _ProductsList2 = _interopRequireDefault(_ProductsList);

var _EditProduct = require('./admin/products/EditProduct');

var _EditProduct2 = _interopRequireDefault(_EditProduct);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//Routes for this app (if any route must be protected wrap it with the function requireAuth(ReactComponent)


// Fitmas Page Components
exports.default = _react2.default.createElement(
    _reactRouter.Router,
    { history: _reactRouter.browserHistory },
    _react2.default.createElement(
        _reactRouter.Route,
        { path: '/', components: _App2.default },
        _react2.default.createElement(_reactRouter.IndexRoute, { component: _Home2.default }),
        _react2.default.createElement(_reactRouter.Route, { path: '/iniciar-sesion', component: _LoginFront2.default }),
        _react2.default.createElement(_reactRouter.Route, { path: '/registrarse', component: _SignUpPage2.default }),
        _react2.default.createElement(_reactRouter.Route, { path: '/productos', component: _Products2.default }),
        _react2.default.createElement(_reactRouter.Route, { path: '/producto/:name/:identifier', component: _Product2.default }),
        _react2.default.createElement(_reactRouter.Route, { path: '/blog', component: _Blog2.default }),
        _react2.default.createElement(_reactRouter.Route, { path: '/blog/:articleName/:articleId', component: _Article2.default }),
        _react2.default.createElement(_reactRouter.Route, { path: '/contacto', component: _Contact2.default }),
        _react2.default.createElement(_reactRouter.Route, { path: '/sobre-fitmas', component: _AboutFitmas2.default }),
        _react2.default.createElement(_reactRouter.Route, { path: '/sobre-lipoblue', component: _AboutLipoblue2.default }),
        _react2.default.createElement(_reactRouter.Route, { path: '/carrito', component: _Basket2.default }),
        _react2.default.createElement(_reactRouter.Route, { path: '/pago', component: _Payment2.default })
    ),
    _react2.default.createElement(
        _reactRouter.Route,
        { path: '/admin', component: _Admin2.default },
        _react2.default.createElement(_reactRouter.Route, { path: '/admin/login', component: _LoginPage2.default }),
        _react2.default.createElement(_reactRouter.Route, { path: '/admin/home', component: (0, _requireAuth2.default)(_Home4.default) }),
        _react2.default.createElement(_reactRouter.Route, { path: '/admin/articles/create', component: (0, _requireAuth2.default)(_CreateArticle2.default) }),
        _react2.default.createElement(_reactRouter.Route, { path: '/admin/products', component: _ProductsList2.default }),
        _react2.default.createElement(_reactRouter.Route, { path: '/admin/products/create', component: _CreateProduct2.default }),
        _react2.default.createElement(_reactRouter.Route, { path: '/admin/products/:identifier', component: _EditProduct2.default }),
        _react2.default.createElement(_reactRouter.Route, { path: '/admin/articles/:identifier', component: _EditArticle2.default })
    )
);