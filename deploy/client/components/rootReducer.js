'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _redux = require('redux');

var _flashMessages = require('./reducers/flashMessages');

var _flashMessages2 = _interopRequireDefault(_flashMessages);

var _auth = require('./reducers/auth');

var _auth2 = _interopRequireDefault(_auth);

var _page = require('./reducers/page');

var _page2 = _interopRequireDefault(_page);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//Mix the reducers for this app in the localStorage
exports.default = (0, _redux.combineReducers)({
    flashMessages: _flashMessages2.default,
    auth: _auth2.default,
    page: _page2.default
});