'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.setCurrentPage = setCurrentPage;

var _types = require('./types');

// Set the page in localStorage using his reducer
function setCurrentPage(page) {
    return {
        type: _types.SET_CURRENT_PAGE,
        page: page
    };
}