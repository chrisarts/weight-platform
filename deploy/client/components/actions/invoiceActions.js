'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getInvoice = getInvoice;
exports.createInvoice = createInvoice;
exports.updateInvoice = updateInvoice;
exports.getInvoiceList = getInvoiceList;
exports.getInvoiceListLimit = getInvoiceListLimit;
exports.deleteInvoice = deleteInvoice;

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getInvoice(article) {
    return function (dispatch) {
        return _axios2.default.post('/api/invoices/get', article);
    };
}

function createInvoice(article) {
    return function (dispatch) {
        return _axios2.default.post('/api/invoices/create', article);
    };
}

function updateInvoice(article) {
    return function (dispatch) {
        return _axios2.default.post('/api/invoices/update', article);
    };
}

function getInvoiceList(article) {
    return function (dispatch) {
        return _axios2.default.post('/api/invoices/all', article);
    };
}

function getInvoiceListLimit(limit) {
    return function (dispatch) {
        return _axios2.default.post('/api/invoices/all/limit', limit);
    };
}

function deleteInvoice(article) {
    return function (dispatch) {
        return _axios2.default.post('/api/invoices/all/limit', article);
    };
}