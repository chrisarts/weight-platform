'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var ADD_FLASH_MESSAGE = exports.ADD_FLASH_MESSAGE = 'ADD_FLASH_MESSAGE';
var SET_CURRENT_USER = exports.SET_CURRENT_USER = 'SET_CURRENT_USER';
var SET_CURRENT_PAGE = exports.SET_CURRENT_PAGE = 'SET_CURRENT_PAGE';