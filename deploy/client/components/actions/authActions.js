'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.setCurrentUser = setCurrentUser;
exports.signup = signup;
exports.logout = logout;
exports.login = login;

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

var _setAuthorizationToken = require('../utils/setAuthorizationToken');

var _setAuthorizationToken2 = _interopRequireDefault(_setAuthorizationToken);

var _types = require('./types');

var _jwtDecode = require('jwt-decode');

var _jwtDecode2 = _interopRequireDefault(_jwtDecode);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Set the user in localStorage using his reducer
function setCurrentUser(user) {
    return {
        type: _types.SET_CURRENT_USER,
        user: user
    };
}

function signup(user) {
    return function () {
        return _axios2.default.post('/api/users', user);
    };
}

// Destroy the token from localStorage and the request Headers
function logout() {
    return function (dispatch) {
        localStorage.removeItem('jwtToken');
        (0, _setAuthorizationToken2.default)(false);
        dispatch(setCurrentUser({}));
    };
}

// Create a new Token and set header with it
function login(data) {
    return function (dispatch) {
        return _axios2.default.post('/api/login', data).then(function (res) {
            var token = res.data.token;
            localStorage.setItem('jwtToken', token);
            (0, _setAuthorizationToken2.default)(token);
            dispatch(setCurrentUser((0, _jwtDecode2.default)(token)));
        });
    };
}