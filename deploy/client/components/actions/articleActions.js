'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getArticle = getArticle;
exports.createArticle = createArticle;
exports.updateArticle = updateArticle;
exports.getArticleList = getArticleList;
exports.getArticleListLimit = getArticleListLimit;
exports.deleteArticle = deleteArticle;

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getArticle(article) {
    return function (dispatch) {
        return _axios2.default.post('/api/articles/get', article);
    };
}

function createArticle(article) {
    return function (dispatch) {
        return _axios2.default.post('/api/articles/create', article);
    };
}

function updateArticle(article) {
    return function (dispatch) {
        return _axios2.default.post('/api/articles/update', article);
    };
}

function getArticleList(article) {
    return function (dispatch) {
        return _axios2.default.post('/api/articles/all', article);
    };
}

function getArticleListLimit(limit) {
    return function (dispatch) {
        return _axios2.default.post('/api/articles/all/limit', limit);
    };
}

function deleteArticle(article) {
    return function (dispatch) {
        return _axios2.default.post('/api/articles/all/limit', article);
    };
}