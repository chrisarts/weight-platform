'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.addFlashMessage = addFlashMessage;

var _types = require('./types');

// Reducer for flash messages in the app
function addFlashMessage(message) {
    return {
        type: _types.ADD_FLASH_MESSAGE,
        message: message
    };
}