'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getProductList = getProductList;
exports.getProduct = getProduct;
exports.createProduct = createProduct;
exports.updateProduct = updateProduct;
exports.deleteProduct = deleteProduct;

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getProductList(pagination) {
    return function () {
        return _axios2.default.post('/api/products/all', pagination);
    };
}

function getProduct(product) {
    return function () {
        return _axios2.default.post('/api/products/get', product);
    };
}

function createProduct(product) {
    return function () {
        return _axios2.default.post('/api/products', product);
    };
}

function updateProduct(product) {
    return function () {
        return _axios2.default.post('/api/products/update', product);
    };
}

function deleteProduct(id) {
    return function () {
        return _axios2.default.post('/api/products/delete', id);
    };
}