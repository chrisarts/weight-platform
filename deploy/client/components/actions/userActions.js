'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.addToCar = addToCar;
exports.getBasket = getBasket;
exports.findItem = findItem;
exports.findProduct = findProduct;
exports.deleteBasketProduct = deleteBasketProduct;
exports.updateBasketProduct = updateBasketProduct;

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function addToCar(params) {
    return function () {
        return _axios2.default.post('/api/basket', params);
    };
}

function getBasket(user) {
    return function () {
        return _axios2.default.get('/api/basket/user/' + user);
    };
}

function findItem(id) {
    return function () {
        return _axios2.default.get('/api/basket/' + id);
    };
}

function findProduct(id) {
    return function () {
        return _axios2.default.get('/api/basket/product/' + id);
    };
}

function deleteBasketProduct(id) {
    return function () {
        return _axios2.default.post('/api/basket/delete', id);
    };
}

function updateBasketProduct(params) {
    return function () {
        return _axios2.default.post('/api/basket/update/product', params);
    };
}