"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  "slides": [{
    "title": "",
    "text": "",
    "imageSrc": "https://dummyimage.com/1800x500/000/fff",
    "linkTo": "https://dummyimage.com/600x400/000/fff"
  }, {
    "title": "",
    "text": "",
    "imageSrc": "https://dummyimage.com/1800x500/000/fff",
    "linkTo": "https://dummyimage.com/600x400/000/fff"
  }],
  "articles": [{
    "title": "Articulo 1",
    "text": "Texto del articulo 1, Texto del articulo 1, Texto del articulo 1, Texto del articulo 1, Texto del articulo 1, Texto del articulo 1.",
    "summary": "Resumen del articulo 1.",
    "image": "https://dummyimage.com/720x400/000/fff"
  }, {
    "title": "Articulo 2",
    "text": "Texto del articulo 2, Texto del articulo 2, Texto del articulo 2, Texto del articulo 2, Texto del articulo 2, Texto del articulo 2.",
    "summary": "Resumen del articulo 2.",
    "image": "https://dummyimage.com/720x400/000/fff"
  }, {
    "title": "Articulo 3",
    "text": "Texto del articulo 2, Texto del articulo 2, Texto del articulo 2, Texto del articulo 2, Texto del articulo 2, Texto del articulo 2.",
    "summary": "Resumen del articulo 3.",
    "image": "https://dummyimage.com/720x400/000/fff"
  }, {
    "title": "Articulo 4",
    "text": "Texto del articulo 4, Texto del articulo 4, Texto del articulo 4, Texto del articulo 4, Texto del articulo 4, Texto del articulo 4.",
    "summary": "Resumen del articulo 4.",
    "image": "https://dummyimage.com/720x400/000/fff"
  }, {
    "title": "Articulo 5",
    "text": "Texto del articulo 5, Texto del articulo 5, Texto del articulo 5, Texto del articulo 5, Texto del articulo 5, Texto del articulo 5.",
    "summary": "Resumen del articulo 5.",
    "image": "https://dummyimage.com/720x400/000/fff"
  }, {
    "title": "Articulo 6",
    "text": "Texto del articulo 6, Texto del articulo 6, Texto del articulo 6, Texto del articulo 6, Texto del articulo 6, Texto del articulo 6.",
    "summary": "Resumen del articulo 6.",
    "image": "https://dummyimage.com/720x400/000/fff"
  }]
};