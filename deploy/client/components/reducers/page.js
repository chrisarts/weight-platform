'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _types = require('../actions/types');

var _pages = require('../utils/pages');

var _pages2 = _interopRequireDefault(_pages);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Set the initial state for the localStorage
var initialState = {
    page: _pages2.default
};

// Export this function to handle the action dispatcher and update the redux state

exports.default = function () {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
    var action = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    switch (action.type) {
        case _types.SET_CURRENT_PAGE:
            return {
                page: action.page
            };
        default:
            return state;
    }
};