'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _types = require('../actions/types');

var _isEmpty = require('lodash/isEmpty');

var _isEmpty2 = _interopRequireDefault(_isEmpty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Set the initial state for the localStorage
var initialState = {
    isAuthenticated: false,
    user: {}
};

// Export this function to handle the action dispatcher and update the redux state

exports.default = function () {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
    var action = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    switch (action.type) {
        case _types.SET_CURRENT_USER:
            return {
                isAuthenticated: !(0, _isEmpty2.default)(action.user),
                user: action.user
            };
        default:
            return state;
    }
};