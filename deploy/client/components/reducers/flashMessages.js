'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _types = require('../actions/types');

var _shortid = require('shortid');

var _shortid2 = _interopRequireDefault(_shortid);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

// Dispatcher for flashMessages
exports.default = function () {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    var action = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    switch (action.type) {
        case _types.ADD_FLASH_MESSAGE:
            return [].concat(_toConsumableArray(state), [{
                id: _shortid2.default.generate(),
                type: action.message.type,
                text: action.message.text
            }]);
        default:
            return state;
    }
};