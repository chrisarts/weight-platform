'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var About = function (_React$Component) {
    _inherits(About, _React$Component);

    function About() {
        _classCallCheck(this, About);

        return _possibleConstructorReturn(this, (About.__proto__ || Object.getPrototypeOf(About)).apply(this, arguments));
    }

    _createClass(About, [{
        key: 'render',
        value: function render() {
            var page = this.props.page;
            var responsive = window.innerWidth < 700;
            return _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(
                    'figure',
                    null,
                    _react2.default.createElement('img', {
                        style: {
                            width: '100%'
                        },
                        src: '/images/' + page.name + '/aboutBanner.jpg', alt: 'Sobre ' + page.trueName })
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'about-section ' + page.name },
                    _react2.default.createElement(
                        'div',
                        { className: 'image', style: responsive ? {
                                width: '90%',
                                margin: '0 auto'
                            } : {} },
                        _react2.default.createElement(
                            'figure',
                            null,
                            _react2.default.createElement('img', {
                                src: '/images/' + page.name + '/aboutInfo1.jpg', alt: page.trueName + ' Producto'
                            })
                        )
                    ),
                    _react2.default.createElement(
                        'div',
                        { className: 'text', style: responsive ? {
                                width: '95%',
                                margin: '0 auto'
                            } : {} },
                        _react2.default.createElement(
                            'h3',
                            { style: responsive ? {
                                    textAlign: 'center'
                                } : {} },
                            'TODOS QUEREMOS UN CUERPO \u201CIDEAL\u201D UNA SALUD \u201CIDEAL\u201D UNA VIDA \u201CIDEAL\u201D'
                        ),
                        _react2.default.createElement(
                            'p',
                            null,
                            'La quema de grasa es vital para tener un cuerpo con alto porcentaje m\xFAscular y un bajo nivel graso, y numerosos estudios han demostrado que los componentes de ciertos alimentos ayudan a estimular la quema de grasas en el cuerpo.'
                        ),
                        _react2.default.createElement(
                            'strong',
                            null,
                            'ESTO TERMINA EN UN CUERPO CON UNA COMPOSICI\xD3N ALTA EN GRASA Y BAJA EN M\xDASCULO, SIENDO TODO LO CONTRARIO DE LO IDEAL PARA TENER UNA VIDA SANA Y EN\xC9RGICA'
                        )
                    )
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'about-section1 ' + page.name },
                    _react2.default.createElement(
                        'div',
                        { className: 'left',
                            style: responsive ? { width: '100%' } : {}
                        },
                        _react2.default.createElement(
                            'h3',
                            null,
                            'COMPONENTES PRINCIPALES DE FIT+'
                        ),
                        _react2.default.createElement(
                            'ul',
                            null,
                            _react2.default.createElement(
                                'li',
                                null,
                                'Extracto de semilla de mango Africano'
                            ),
                            _react2.default.createElement(
                                'li',
                                null,
                                'Extracto de cascara de Naranja Agria'
                            ),
                            _react2.default.createElement(
                                'li',
                                null,
                                'Extracto de ra\xEDz de Coleus Forksholll'
                            ),
                            _react2.default.createElement(
                                'li',
                                null,
                                'Dihidrocapsiato (componente clave del pigmento rojo)'
                            )
                        )
                    ),
                    _react2.default.createElement(
                        'div',
                        { className: 'right', style: responsive ? { width: '100%' } : {} },
                        _react2.default.createElement(
                            'div',
                            null,
                            'UN PRODUCTO DISE\xD1ADO PARA AYUDAR A LOS ADULTOS DE TODAS LAS EDADES A ALCANZAR UN CUERPO DELGADO Y SALUDABLE'
                        )
                    )
                )
            );
        }
    }]);

    return About;
}(_react2.default.Component);

function mapStateToProps(state) {
    return {
        page: state.page.page
    };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, {})(About);