'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var About = function (_React$Component) {
    _inherits(About, _React$Component);

    function About() {
        _classCallCheck(this, About);

        return _possibleConstructorReturn(this, (About.__proto__ || Object.getPrototypeOf(About)).apply(this, arguments));
    }

    _createClass(About, [{
        key: 'render',
        value: function render() {
            var page = this.props.page;
            return _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(
                    'figure',
                    null,
                    _react2.default.createElement('img', {
                        style: {
                            width: '100%'
                        },
                        src: '/images/' + page.name + '/aboutBanner.jpg', alt: 'Sobre ' + page.trueName })
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'about-section' },
                    _react2.default.createElement(
                        'div',
                        { className: 'image' },
                        _react2.default.createElement(
                            'figure',
                            null,
                            _react2.default.createElement('img', { src: '/images/' + page.name + '/aboutInfo1.jpg', alt: page.trueName + ' Producto' })
                        )
                    ),
                    _react2.default.createElement(
                        'div',
                        { className: 'text' },
                        _react2.default.createElement(
                            'h3',
                            null,
                            'TODOS QUEREMOS UN CUERPO \u201CIDEAL\u201D UNA SALUD \u201CIDEAL\u201D UNA VIDA \u201CIDEAL\u201D'
                        ),
                        _react2.default.createElement(
                            'p',
                            null,
                            'La quema de grasa es vital para tener un cuerpo con alto porcentaje m\xFAscular y un bajo nivel graso, y numerosos estudios han demostrado que los componentes de ciertos alimentos ayudan a estimular la quema de grasas en el cuerpo.'
                        ),
                        _react2.default.createElement(
                            'strong',
                            null,
                            'ESTO TERMINA EN UN CUERPO CON UNA COMPOSICI\xD3N ALTA EN GRASA Y BAJA EN M\xDASCULO, SIENDO TODO LO CONTRARIO DE LO IDEAL PARA TENER UNA VIDA SANA Y EN\xC9RGICA'
                        )
                    )
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'about-section2' },
                    _react2.default.createElement(
                        'div',
                        { className: 'left' },
                        _react2.default.createElement(
                            'h3',
                            null,
                            'Con base en estas investigaciones hemos desarrollado un producto que potencializa la quema acelerada de Grasa en nuestro organismo'
                        ),
                        _react2.default.createElement(
                            'figure',
                            null,
                            _react2.default.createElement('img', { src: '/images/lipoblue/logo.png ', alt: '' })
                        )
                    )
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'about-section3' },
                    _react2.default.createElement(
                        'div',
                        { className: 'left' },
                        _react2.default.createElement(
                            'h3',
                            null,
                            'BENEFICIOS DE LIPOBLUE'
                        ),
                        _react2.default.createElement(
                            'ul',
                            null,
                            _react2.default.createElement(
                                'li',
                                null,
                                'Estimula la quema de grasas en tu cuerpo'
                            ),
                            _react2.default.createElement(
                                'li',
                                null,
                                'Reduce la ansiedad de comer'
                            ),
                            _react2.default.createElement(
                                'li',
                                null,
                                'Provee energ\xEDa para acelerar tus rutinas de ejercicio y alcanzar un rendimiento \xF3ptimo'
                            )
                        )
                    ),
                    _react2.default.createElement(
                        'div',
                        { className: 'right' },
                        _react2.default.createElement(
                            'div',
                            { className: 'quote' },
                            '\xA1Con LIPOBLUE podr\xE1s quemar de 4 a 6 kg de grasa en solo 30 d\xEDas!'
                        ),
                        _react2.default.createElement(
                            'div',
                            { className: 'big-text' },
                            'UN PRODUCTO DISE\xD1ADO PARA AYUDAR A LOS ADULTOS DE TODAS LAS EDADES A MEJORAR SU SALUD, CALIDAD DE VIDA Y AUTOESTIMA'
                        )
                    )
                )
            );
        }
    }]);

    return About;
}(_react2.default.Component);

function mapStateToProps(state) {
    return {
        page: state.page.page
    };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, {})(About);