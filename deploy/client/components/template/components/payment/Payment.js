'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = require('react-redux');

var _TextField = require('material-ui/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _FlatButton = require('material-ui/FlatButton');

var _FlatButton2 = _interopRequireDefault(_FlatButton);

var _Table = require('material-ui/Table');

var _validator = require('validator');

var _validator2 = _interopRequireDefault(_validator);

var _isEmpty = require('lodash/isEmpty');

var _isEmpty2 = _interopRequireDefault(_isEmpty);

var _jwtDecode = require('jwt-decode');

var _jwtDecode2 = _interopRequireDefault(_jwtDecode);

var _md = require('md5');

var _md2 = _interopRequireDefault(_md);

var _LoadingSection = require('../../../globalComponents/loading/LoadingSection');

var _LoadingSection2 = _interopRequireDefault(_LoadingSection);

var _ProductSubtotal = require('./ProductSubtotal');

var _ProductSubtotal2 = _interopRequireDefault(_ProductSubtotal);

var _userActions = require('../../../actions/userActions');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function validateInputEvent(data) {
    var errors = {};

    if (_validator2.default.isEmpty(data.name)) {
        errors.name = 'Este campo es obligatorio';
    }
    if (_validator2.default.isEmpty(data.address)) {
        errors.address = 'Este campo es obligatorio';
    }

    if (_validator2.default.isEmpty(data.price)) {
        errors.price = 'Este campo es obligatorio';
    }

    return {
        errors: errors,
        isValid: (0, _isEmpty2.default)(errors)
    };
}

var Payment = function (_React$Component) {
    _inherits(Payment, _React$Component);

    function Payment(props) {
        _classCallCheck(this, Payment);

        var _this = _possibleConstructorReturn(this, (Payment.__proto__ || Object.getPrototypeOf(Payment)).call(this, props));

        _this.state = {
            user: {},
            receiverName: '',
            receiverAddress: '',
            products: [],
            signature: '',
            total: '0',
            merchantId: '',
            errors: {},
            isLoading: false
        };
        return _this;
    }

    _createClass(Payment, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var This = this;
            var decodedToken = (0, _jwtDecode2.default)(localStorage.jwtToken);
            This.props.getBasket(decodedToken.id).then(function (result) {
                This.setState({
                    user: decodedToken,
                    products: result.data.basket
                });
            });
        }
    }, {
        key: 'isValid',
        value: function isValid() {
            var _validateInputEvent = validateInputEvent(this.state),
                errors = _validateInputEvent.errors,
                isValid = _validateInputEvent.isValid;

            if (!isValid) {
                this.setState({ errors: errors });
            }

            return isValid;
        }
    }, {
        key: 'onChange',
        value: function onChange(event) {
            this.setState(_defineProperty({}, event.target.name, event.target.value));
        }
    }, {
        key: 'updateTotal',
        value: function updateTotal(newTotal) {
            var signature = (0, _md2.default)('4Vj8eK4rloUd272L48hsrarnUA~508029~TestPayU~20000~COP');
            this.setState({
                total: newTotal,
                signature: signature
            });
        }
    }, {
        key: 'getCurrentTotal',
        value: function getCurrentTotal() {
            return this.state.total;
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var errors = this.state.errors;

            return _react2.default.createElement(
                'div',
                { className: 'page-payment' },
                _react2.default.createElement(
                    'h1',
                    null,
                    'Informaci\xF3n de env\xEDo'
                ),
                _react2.default.createElement(
                    'form',
                    {
                        method: 'post',
                        action: 'https://sandbox.gateway.payulatam.com/ppp-web-gateway',
                        target: '_blank'
                    },
                    _react2.default.createElement('input', { name: 'merchantId', type: 'hidden', value: '508029' }),
                    _react2.default.createElement('input', { name: 'accountId', type: 'hidden', value: '512321' }),
                    _react2.default.createElement('input', { name: 'signature', type: 'hidden', defaultValue: this.state.signature }),
                    _react2.default.createElement('input', { name: 'referenceCode', type: 'hidden', defaultValue: 'TestPayU' }),
                    _react2.default.createElement('input', { name: 'amount', type: 'hidden', value: '20000' }),
                    _react2.default.createElement('input', { name: 'buyerFullName', type: 'hidden', value: 'Chris el comprador' }),
                    _react2.default.createElement('input', { name: 'buyerEmail', type: 'hidden', value: 'test@test.com' }),
                    _react2.default.createElement('input', { name: 'description', type: 'hidden', value: 'Test PAYU' }),
                    _react2.default.createElement('input', { name: 'responseUrl', type: 'hidden', defaultValue: 'http://' + document.domain + '/response' }),
                    _react2.default.createElement('input', { name: 'confirmationUrl', type: 'hidden', defaultValue: 'http://' + document.domain + '/confirmation' }),
                    _react2.default.createElement('input', { name: 'tax', type: 'hidden', value: '3193' }),
                    _react2.default.createElement('input', { name: 'taxReturnBase', type: 'hidden', value: '16806' }),
                    _react2.default.createElement('input', { name: 'currency', type: 'hidden', value: 'COP' }),
                    _react2.default.createElement('input', { name: 'test', type: 'hidden', value: '1' }),
                    _react2.default.createElement(_TextField2.default, {
                        name: 'name',
                        floatingLabelText: 'Nombre de qui\xE9n recibe *',
                        value: this.state.name,
                        errorText: errors.name,
                        onChange: this.onChange.bind(this),
                        style: {
                            width: '100%'
                        }
                    }),
                    _react2.default.createElement(_TextField2.default, {
                        name: 'address',
                        floatingLabelText: 'Direcci\xF3n de qui\xE9n recibe *',
                        value: this.state.address,
                        errorText: errors.address,
                        onChange: this.onChange.bind(this),
                        style: {
                            width: '100%'
                        }
                    }),
                    _react2.default.createElement(
                        'section',
                        null,
                        _react2.default.createElement(
                            'h2',
                            null,
                            'Lista de Productos'
                        ),
                        _react2.default.createElement(_LoadingSection2.default, {
                            payload: this.state.isLoading,
                            width: '100%',
                            height: '400px'
                        }),
                        _react2.default.createElement(
                            _Table.Table,
                            null,
                            _react2.default.createElement(
                                _Table.TableHeader,
                                {
                                    displaySelectAll: false,
                                    adjustForCheckbox: false
                                },
                                _react2.default.createElement(
                                    _Table.TableRow,
                                    null,
                                    _react2.default.createElement(
                                        _Table.TableHeaderColumn,
                                        null,
                                        'Nombre'
                                    ),
                                    _react2.default.createElement(
                                        _Table.TableHeaderColumn,
                                        null,
                                        'Cantidad'
                                    ),
                                    _react2.default.createElement(
                                        _Table.TableHeaderColumn,
                                        null,
                                        'Precio Unitario'
                                    ),
                                    _react2.default.createElement(
                                        _Table.TableHeaderColumn,
                                        null,
                                        'Subtotal'
                                    )
                                )
                            ),
                            _react2.default.createElement(
                                _Table.TableBody,
                                {
                                    displayRowCheckbox: false
                                },
                                this.state.products.map(function (product) {
                                    return _react2.default.createElement(_ProductSubtotal2.default, {
                                        key: product.id,
                                        updateTotal: _this2.updateTotal.bind(_this2),
                                        getTotal: _this2.getCurrentTotal.bind(_this2),
                                        basketProduct: product
                                    });
                                })
                            ),
                            _react2.default.createElement(
                                _Table.TableFooter,
                                null,
                                _react2.default.createElement(
                                    _Table.TableRow,
                                    null,
                                    _react2.default.createElement(
                                        _Table.TableRowColumn,
                                        null,
                                        '\xA0'
                                    ),
                                    _react2.default.createElement(
                                        _Table.TableRowColumn,
                                        null,
                                        '\xA0'
                                    ),
                                    _react2.default.createElement(
                                        _Table.TableRowColumn,
                                        null,
                                        _react2.default.createElement(
                                            'h4',
                                            {
                                                style: {
                                                    textAlign: 'right'
                                                }
                                            },
                                            'Total:'
                                        )
                                    ),
                                    _react2.default.createElement(
                                        _Table.TableRowColumn,
                                        null,
                                        _react2.default.createElement(
                                            'h4',
                                            null,
                                            '$',
                                            this.state.total.toLocaleString(undefined, // use a string like 'en-US' to override browser locale
                                            { minimumFractionDigits: 0 })
                                        )
                                    )
                                )
                            )
                        ),
                        _react2.default.createElement(_FlatButton2.default, {
                            label: 'Proceder al pago',
                            type: 'submit',
                            backgroundColor: '#89286d',
                            className: 'payment-button',
                            style: {
                                marginTop: 20,
                                fontFamily: 'cuyabra',
                                color: 'white',
                                fontWeight: 'bold',
                                padding: '0px 25px',
                                float: 'right'
                            }
                        })
                    )
                )
            );
        }
    }]);

    return Payment;
}(_react2.default.Component);

Payment.PropTypes = {
    getBasket: _propTypes2.default.func.isRequired
};

function mapStateToProps(state) {
    return {
        page: state.page.page
    };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, { getBasket: _userActions.getBasket })(Payment);