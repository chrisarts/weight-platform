'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = require('react-redux');

var _Table = require('material-ui/Table');

var _productActions = require('../../../actions/productActions');

var _LoadingSection = require('../../../globalComponents/loading/LoadingSection');

var _LoadingSection2 = _interopRequireDefault(_LoadingSection);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ProductSubtotal = function (_React$Component) {
    _inherits(ProductSubtotal, _React$Component);

    function ProductSubtotal(props) {
        _classCallCheck(this, ProductSubtotal);

        var _this = _possibleConstructorReturn(this, (ProductSubtotal.__proto__ || Object.getPrototypeOf(ProductSubtotal)).call(this, props));

        _this.state = {
            id: 0,
            name: '',
            price: '',
            subTotal: ''
        };
        _this.styles = {
            deleteActions: {
                float: 'right',
                buttons: {
                    marginRight: 20
                }
            }
        };
        return _this;
    }

    _createClass(ProductSubtotal, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var This = this;
            console.log(this.props.basketProduct);
            This.props.getProduct({ id: This.props.basketProduct.productId }).then(function (result) {
                var price = parseInt(result.data.product.price, 0);
                var priceFormat = price.toLocaleString(undefined, // use a string like 'en-US' to override browser locale
                { minimumFractionDigits: 0 });
                var subTotal = price * parseInt(This.props.basketProduct.quantity, 0);
                var subTotalFormat = subTotal.toLocaleString(undefined, // use a string like 'en-US' to override browser locale
                { minimumFractionDigits: 0 });
                This.setState({
                    id: result.data.product.id,
                    name: result.data.product.name,
                    price: priceFormat,
                    subTotal: subTotalFormat
                });
                var currentTotal = This.props.getTotal();
                var newTotal = parseInt(currentTotal, 0) + parseInt(subTotal, 0);
                This.props.updateTotal(newTotal);
            });
        }
    }, {
        key: 'render',
        value: function render() {
            return _react2.default.createElement(
                _Table.TableRow,
                { key: this.state.id },
                _react2.default.createElement(_LoadingSection2.default, {
                    payload: this.state.isLoading,
                    width: '100%',
                    height: '100px'
                }),
                _react2.default.createElement(
                    _Table.TableRowColumn,
                    null,
                    this.state.name
                ),
                _react2.default.createElement(
                    _Table.TableRowColumn,
                    null,
                    this.props.basketProduct.quantity
                ),
                _react2.default.createElement(
                    _Table.TableRowColumn,
                    null,
                    '$ ',
                    this.state.price
                ),
                _react2.default.createElement(
                    _Table.TableRowColumn,
                    null,
                    '$ ',
                    this.state.subTotal
                )
            );
        }
    }]);

    return ProductSubtotal;
}(_react2.default.Component);

ProductSubtotal.PropTypes = {
    getProduct: _propTypes2.default.func.isRequired,
    basketProduct: _propTypes2.default.object.isRequired,
    updateTotal: _propTypes2.default.func.isRequired,
    getTotal: _propTypes2.default.func.isRequired
};

exports.default = (0, _reactRedux.connect)(null, { getProduct: _productActions.getProduct })(ProductSubtotal);