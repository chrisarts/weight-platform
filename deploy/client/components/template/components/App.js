'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _MuiThemeProvider = require('material-ui/styles/MuiThemeProvider');

var _MuiThemeProvider2 = _interopRequireDefault(_MuiThemeProvider);

var _AppToolbar = require('./commons/AppToolbar');

var _AppToolbar2 = _interopRequireDefault(_AppToolbar);

var _Footer = require('./commons/Footer');

var _Footer2 = _interopRequireDefault(_Footer);

var _SubFooter = require('./commons/SubFooter');

var _SubFooter2 = _interopRequireDefault(_SubFooter);

var _FlashMessagesList = require('../../globalComponents/flashMessages/FlashMessagesList');

var _FlashMessagesList2 = _interopRequireDefault(_FlashMessagesList);

require('../css/styles.css');

var _reactTapEventPlugin = require('react-tap-event-plugin');

var _reactTapEventPlugin2 = _interopRequireDefault(_reactTapEventPlugin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// Needed for onTouchTap


(0, _reactTapEventPlugin2.default)();

//This will be our Main Component

var App = function (_React$Component) {
    _inherits(App, _React$Component);

    function App(props) {
        _classCallCheck(this, App);

        var _this = _possibleConstructorReturn(this, (App.__proto__ || Object.getPrototypeOf(App)).call(this, props));

        _this.state = {
            value: 3
        };
        return _this;
    }

    _createClass(App, [{
        key: 'render',
        value: function render() {
            var page = this.props.page;
            return _react2.default.createElement(
                _MuiThemeProvider2.default,
                null,
                _react2.default.createElement(
                    'div',
                    null,
                    _react2.default.createElement(_AppToolbar2.default, null),
                    _react2.default.createElement(_FlashMessagesList2.default, null),
                    _react2.default.createElement(
                        'div',
                        { className: 'global-container ' + page.name },
                        this.props.children,
                        _react2.default.createElement(_Footer2.default, null),
                        _react2.default.createElement(_SubFooter2.default, { background: page.footerBackground })
                    )
                )
            );
        }
    }]);

    return App;
}(_react2.default.Component);

function mapStateToProps(state) {
    return {
        page: state.page.page
    };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, {})(App);