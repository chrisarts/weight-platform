'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = require('react-redux');

var _reactBootstrap = require('react-bootstrap');

var _Card = require('material-ui/Card');

var _FlatButton = require('material-ui/FlatButton');

var _FlatButton2 = _interopRequireDefault(_FlatButton);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _reactHelmet = require('react-helmet');

var _articleActions = require('../../actions/articleActions');

var _LoadingSection = require('../../globalComponents/loading/LoadingSection');

var _LoadingSection2 = _interopRequireDefault(_LoadingSection);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Blog = function (_React$Component) {
    _inherits(Blog, _React$Component);

    function Blog(props) {
        _classCallCheck(this, Blog);

        var _this = _possibleConstructorReturn(this, (Blog.__proto__ || Object.getPrototypeOf(Blog)).call(this, props));

        _this.state = {
            articles: [],
            page: _this.props.page.name,
            pagination: {
                pageSize: 20,
                currentPage: 1,
                page: _this.props.page.name
            },
            isLoading: true
        };
        _moment2.default.locale('es');
        return _this;
    }

    _createClass(Blog, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var This = this;
            This.props.getArticleList(This.state.pagination).then(function (result) {
                This.setState({
                    articles: result.data.filteredArticles,
                    isLoading: false
                });
            });
        }
    }, {
        key: 'goArticle',
        value: function goArticle(articleUrl) {
            this.context.router.push(articleUrl);
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            return _react2.default.createElement(
                _reactBootstrap.Grid,
                { className: 'blog-grid  ' + (this.state.isLoading ? "section-loading" : "section-loaded") },
                _react2.default.createElement(
                    _reactHelmet.Helmet,
                    null,
                    _react2.default.createElement(
                        'title',
                        null,
                        'Blog'
                    )
                ),
                _react2.default.createElement(
                    _reactBootstrap.Row,
                    null,
                    _react2.default.createElement(
                        'h1',
                        null,
                        'Articulos'
                    ),
                    _react2.default.createElement(_LoadingSection2.default, {
                        payload: this.state.isLoading,
                        width: '100%',
                        height: '300px'
                    }),
                    this.state.articles.map(function (article) {
                        var media = JSON.parse(article.media);
                        var count = 0;
                        var image = '';
                        for (var key in media) {
                            if (media.hasOwnProperty(key) && count === 0) {
                                image = media[key];
                                ++count;
                            }
                        }
                        var newDescription = article.content.replace(/<\/?[^>]+(>|$)/g, "");
                        newDescription = newDescription.substring(0, 200);
                        var newDate = (0, _moment2.default)(article.created_at).format("dddd, MMMM D YYYY, h:mm a");
                        newDate = newDate.replace('pm', "PM");
                        newDate = newDate.replace('am', "AM");
                        var urlName = article.title.replace(' ', '-');
                        var articleUrl = '/blog/' + urlName + '/' + article.id;
                        return _react2.default.createElement(
                            _reactBootstrap.Col,
                            { md: 4, key: article.id },
                            _react2.default.createElement(
                                _Card.Card,
                                null,
                                _react2.default.createElement(_Card.CardHeader, {
                                    title: article.title,
                                    subtitle: newDate,
                                    style: {
                                        textTransform: 'capitalize'
                                    }
                                }),
                                _react2.default.createElement(
                                    _Card.CardMedia,
                                    null,
                                    _react2.default.createElement('img', { src: image, alt: article.title })
                                ),
                                _react2.default.createElement(_Card.CardTitle, { title: article.title, subtitle: article.category }),
                                _react2.default.createElement(
                                    _Card.CardText,
                                    null,
                                    newDescription
                                ),
                                _react2.default.createElement(
                                    _Card.CardActions,
                                    null,
                                    _react2.default.createElement(_FlatButton2.default, {
                                        label: 'Ver Articulo',
                                        fullWidth: true,
                                        backgroundColor: _this2.props.page.buttonColor,
                                        labelStyle: {
                                            color: 'white'
                                        },
                                        onClick: _this2.goArticle.bind(_this2, articleUrl)
                                    })
                                )
                            )
                        );
                    })
                )
            );
        }
    }]);

    return Blog;
}(_react2.default.Component);

Blog.PropTypes = {
    getArticleList: _propTypes2.default.func.isRequired
};
Blog.contextTypes = {
    router: _react2.default.PropTypes.object.isRequired
};

function mapStateToProps(state) {
    return {
        page: state.page.page
    };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, { getArticleList: _articleActions.getArticleList })(Blog);