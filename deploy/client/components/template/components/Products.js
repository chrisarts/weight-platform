'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _reactBootstrap = require('react-bootstrap');

var _reactHelmet = require('react-helmet');

var _ProductsGridFull = require('./commons/ProductsGridFull');

var _ProductsGridFull2 = _interopRequireDefault(_ProductsGridFull);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Products = function (_React$Component) {
    _inherits(Products, _React$Component);

    function Products() {
        _classCallCheck(this, Products);

        return _possibleConstructorReturn(this, (Products.__proto__ || Object.getPrototypeOf(Products)).apply(this, arguments));
    }

    _createClass(Products, [{
        key: 'render',
        value: function render() {
            var page = this.props.page;
            return _react2.default.createElement(
                'div',
                { className: 'products-page' },
                _react2.default.createElement(
                    _reactBootstrap.Grid,
                    null,
                    _react2.default.createElement(
                        _reactHelmet.Helmet,
                        null,
                        _react2.default.createElement(
                            'title',
                            null,
                            'Tienda'
                        )
                    ),
                    _react2.default.createElement(
                        _reactBootstrap.Row,
                        { className: 'products-sales' },
                        _react2.default.createElement(
                            _reactBootstrap.Col,
                            { md: 6, className: 'sale' },
                            _react2.default.createElement('img', { src: '/images/' + page.name + '/productsPromo1.jpg', alt: page.name + ' promoci\xF3n' })
                        ),
                        _react2.default.createElement(
                            _reactBootstrap.Col,
                            { md: 6, className: 'sale' },
                            _react2.default.createElement('img', { src: '/images/' + page.name + '/productsPromo2.jpg', alt: page.name + ' promoci\xF3n' })
                        )
                    ),
                    _react2.default.createElement(
                        _reactBootstrap.Row,
                        { className: 'products-packs' },
                        _react2.default.createElement(
                            _reactBootstrap.Col,
                            { md: 4, className: 'pack' },
                            _react2.default.createElement('img', { src: '/images/' + page.name + '/productsMediaPromo1.jpg', alt: page.name + ' Lista de Promociones' })
                        ),
                        _react2.default.createElement(
                            _reactBootstrap.Col,
                            { md: 4, className: 'pack' },
                            _react2.default.createElement('img', { src: '/images/' + page.name + '/productsMediaPromo2.jpg', alt: page.name + ' Tabla de Precios' })
                        ),
                        _react2.default.createElement(
                            _reactBootstrap.Col,
                            { md: 4, className: 'pack' },
                            _react2.default.createElement('img', { src: '/images/' + page.name + '/productsMediaPromo3.jpg', alt: page.name + ' PDF' })
                        )
                    ),
                    _react2.default.createElement(_ProductsGridFull2.default, { pageSize: 3 })
                )
            );
        }
    }]);

    return Products;
}(_react2.default.Component);

function mapStateToProps(state) {
    return {
        page: state.page.page
    };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, {})(Products);