'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _TextField = require('material-ui/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _RaisedButton = require('material-ui/RaisedButton');

var _RaisedButton2 = _interopRequireDefault(_RaisedButton);

var _reactHelmet = require('react-helmet');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Contact = function (_React$Component) {
    _inherits(Contact, _React$Component);

    function Contact(props) {
        _classCallCheck(this, Contact);

        var _this = _possibleConstructorReturn(this, (Contact.__proto__ || Object.getPrototypeOf(Contact)).call(this, props));

        _this.state = {
            name: '',
            city: '',
            telephone: '',
            isLoading: false,
            errors: {}
        };
        return _this;
    }

    _createClass(Contact, [{
        key: 'onChange',
        value: function onChange(event) {
            this.setState(_defineProperty({}, event.target.name, event.target.value));
        }
    }, {
        key: 'render',
        value: function render() {
            var responsive = window.innerWidth <= 830;
            var _state = this.state,
                isLoading = _state.isLoading,
                errors = _state.errors;

            return _react2.default.createElement(
                'div',
                {
                    className: 'page-contact',
                    style: responsive ? {
                        paddingTop: 30
                    } : {}
                },
                _react2.default.createElement(
                    _reactHelmet.Helmet,
                    null,
                    _react2.default.createElement(
                        'title',
                        null,
                        'Contacto'
                    )
                ),
                _react2.default.createElement(
                    'div',
                    {
                        className: 'left',
                        style: responsive ? {
                            width: '100%',
                            paddingLeft: 0,
                            padding: '0 15px',
                            textAlign: 'center'
                        } : {}
                    },
                    _react2.default.createElement(
                        'h1',
                        null,
                        'CONTACTO'
                    ),
                    _react2.default.createElement(
                        'p',
                        null,
                        'Tienda Mujer Slim es perfecta para todas las mujeres interesadas en belleza, bienestar y salud, a trav\xE9s de nuestros productos buscamos afianzar la confianza en s\xED misma.'
                    ),
                    _react2.default.createElement(
                        'p',
                        null,
                        'Contra Entrega, Bogot\xE1 - Cali'
                    ),
                    _react2.default.createElement(
                        'p',
                        null,
                        'PBX (1) 381-9663 / 314-773-823'
                    )
                ),
                _react2.default.createElement(
                    'div',
                    {
                        className: 'right',
                        style: responsive ? {
                            width: '100%',
                            paddingLeft: 0,
                            padding: '0 15px',
                            textAlign: 'center'
                        } : {}
                    },
                    _react2.default.createElement(
                        'form',
                        { action: '' },
                        _react2.default.createElement(
                            'h2',
                            null,
                            '\xBFQuieres ser distribuidor en tu pa\xEDs y ciudad? Env\xEDanos tus datos'
                        ),
                        _react2.default.createElement(_TextField2.default, {
                            name: 'name',
                            floatingLabelText: 'Nombre',
                            value: this.state.name,
                            errorText: errors.name,
                            onChange: this.onChange.bind(this),
                            style: {
                                width: '100%'
                            },
                            inputStyle: {
                                backgroundColor: '#53263a'
                            },
                            floatingLabelStyle: {
                                color: 'white'
                            },
                            underlineFocusStyle: {
                                color: 'white'
                            },
                            underlineStyle: {
                                color: 'white'
                            }
                        }),
                        _react2.default.createElement(_TextField2.default, {
                            name: 'city',
                            floatingLabelText: 'Ciudad',
                            value: this.state.city,
                            errorText: errors.city,
                            onChange: this.onChange.bind(this),
                            style: {
                                width: '100%'
                            },
                            inputStyle: {
                                backgroundColor: '#53263a'
                            },
                            floatingLabelStyle: {
                                color: 'white'
                            }
                        }),
                        _react2.default.createElement(_TextField2.default, {
                            name: 'telephone',
                            floatingLabelText: 'Tel\xE9fono',
                            value: this.state.telephone,
                            errorText: errors.telephone,
                            onChange: this.onChange.bind(this),
                            style: {
                                width: '100%'
                            },
                            inputStyle: {
                                backgroundColor: '#53263a'
                            },
                            floatingLabelStyle: {
                                color: 'white'
                            }
                        }),
                        _react2.default.createElement(_RaisedButton2.default, {
                            label: 'Enviar',
                            type: 'submit',
                            disabled: isLoading
                        })
                    )
                )
            );
        }
    }]);

    return Contact;
}(_react2.default.Component);

exports.default = Contact;