'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SubFooter = function (_React$Component) {
    _inherits(SubFooter, _React$Component);

    function SubFooter(props) {
        _classCallCheck(this, SubFooter);

        var _this = _possibleConstructorReturn(this, (SubFooter.__proto__ || Object.getPrototypeOf(SubFooter)).call(this, props));

        _this.styles = {
            padding: '24px 21%',
            backgroundColor: _this.props.page.footerBackground,
            fontSize: 12
        };
        return _this;
    }

    _createClass(SubFooter, [{
        key: 'render',
        value: function render() {
            var page = this.props.page;
            return _react2.default.createElement(
                'div',
                { style: this.styles, className: 'subfooter ' + page.name },
                'Las declaraciones aqu\xED manifestadas no han sido evaluadas por el ministerio de salud u organismo de control de drogas. Con estos productos no se intenta diagnosticar, tratar, curar \xF3 prevenir ninguna enfermedad. Lea todas las etiquetas contenidas en el producto antes de usarlo. El logo de Facebook pertenece a Facebook Inc. Apple y el logo de Apple son marcas registradas de Apple Inc. registrados en U.S. y otros paises. App Store es un servicio registrado por Apple Inc., registrado en U.S. y otros paises. Google Play y su logo son marcas registradas de Google Inc. \xA9 2017.'
            );
        }
    }]);

    return SubFooter;
}(_react2.default.Component);

function mapStateToProps(state) {
    return {
        page: state.page.page
    };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, {})(SubFooter);