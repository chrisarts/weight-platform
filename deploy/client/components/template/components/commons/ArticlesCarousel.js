'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactSlick = require('react-slick');

var _reactSlick2 = _interopRequireDefault(_reactSlick);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ArticlesCarousel = function (_React$Component) {
    _inherits(ArticlesCarousel, _React$Component);

    function ArticlesCarousel(props) {
        _classCallCheck(this, ArticlesCarousel);

        var _this = _possibleConstructorReturn(this, (ArticlesCarousel.__proto__ || Object.getPrototypeOf(ArticlesCarousel)).call(this, props));

        _this.state = {
            settings: {
                dots: true,
                infinite: true,
                speed: 500,
                slidesToShow: 3,
                slidesToScroll: 3
            },
            slicks: [],
            slickKey: 0
        };
        return _this;
    }

    _createClass(ArticlesCarousel, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            if (this.props.articles.length > 0) {
                this.slickMaker();
            }
        }
    }, {
        key: 'slickMaker',
        value: function slickMaker() {
            var This = this;
            This.props.articles.map(function (article) {
                var newSlick = This.state.slicks;
                newSlick.push(_react2.default.createElement(
                    'div',
                    { key: This.state.slickKey, className: 'article-carousel-item' },
                    _react2.default.createElement(
                        'figure',
                        null,
                        _react2.default.createElement('img', { src: article.image, alt: article.title })
                    ),
                    _react2.default.createElement(
                        'h5',
                        null,
                        article.title
                    ),
                    _react2.default.createElement(
                        'p',
                        null,
                        article.text
                    )
                ));
                This.setState({
                    slicks: newSlick,
                    slickKey: This.state.slickKey++
                });
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var slicks = [];
            if (this.state.slicks.length > 0) {
                slicks = this.state.slicks;
            } else {
                slicks.push(_react2.default.createElement(
                    'div',
                    { key: 999999 },
                    '\xA0'
                ));
            }
            if (this.props.articles.length > 0) {
                return _react2.default.createElement(
                    _reactSlick2.default,
                    _extends({}, this.state.settings, { className: 'articles-carousel' }),
                    slicks
                );
            } else {
                return _react2.default.createElement(
                    'div',
                    null,
                    _react2.default.createElement(
                        'h2',
                        {
                            style: {
                                textAlign: 'center',
                                padding: 50
                            }
                        },
                        'No existen articulos a\xFAn.'
                    )
                );
            }
        }
    }]);

    return ArticlesCarousel;
}(_react2.default.Component);

ArticlesCarousel.PropTypes = {
    articles: _propTypes2.default.array.isRequired
};

exports.default = ArticlesCarousel;