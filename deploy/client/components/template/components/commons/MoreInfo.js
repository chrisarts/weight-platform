"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MoreInfo = function (_React$Component) {
    _inherits(MoreInfo, _React$Component);

    function MoreInfo(props) {
        _classCallCheck(this, MoreInfo);

        return _possibleConstructorReturn(this, (MoreInfo.__proto__ || Object.getPrototypeOf(MoreInfo)).call(this, props));
    }

    _createClass(MoreInfo, [{
        key: "render",
        value: function render() {
            return _react2.default.createElement(
                "div",
                { className: "home-app" },
                _react2.default.createElement(
                    "div",
                    { className: "text" },
                    _react2.default.createElement(
                        "h3",
                        null,
                        "Descarga nuestra aplicaci\xF3n"
                    ),
                    _react2.default.createElement(
                        "p",
                        null,
                        "Nuestra aplicaci\xF3n contiene herramientras extra que te ayudar\xE1n a cumplir tus metas de reducci\xF3n de peso. Este anuncio puede contener informaci\xF3n sobre ventas \xF3 redes sociales."
                    ),
                    _react2.default.createElement(
                        "div",
                        { className: "app-stores" },
                        _react2.default.createElement("img", { src: "https://www.hydroxycut.com/wp-content/uploads/download-apple-btn.png", alt: "" }),
                        _react2.default.createElement("img", { src: "https://www.hydroxycut.com/wp-content/uploads/download-google-btn.png", alt: "" })
                    )
                ),
                _react2.default.createElement(
                    "div",
                    { className: "image" },
                    _react2.default.createElement(
                        "figure",
                        null,
                        _react2.default.createElement("img", { src: require('../images/health-app.jpg'), alt: "Health App" })
                    )
                )
            );
        }
    }]);

    return MoreInfo;
}(_react2.default.Component);

exports.default = MoreInfo;