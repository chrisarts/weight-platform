'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _Carousel = require('react-bootstrap/lib/Carousel');

var _Carousel2 = _interopRequireDefault(_Carousel);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Slider = function (_React$Component) {
    _inherits(Slider, _React$Component);

    function Slider(props) {
        _classCallCheck(this, Slider);

        var _this = _possibleConstructorReturn(this, (Slider.__proto__ || Object.getPrototypeOf(Slider)).call(this, props));

        var This = _this;
        This.settings = {};
        if (This.props.settings) {
            This.settings = This.props.settings;
        } else {
            This.settings = {
                dots: true,
                infinite: true,
                speed: 500,
                slidesToShow: 1,
                slidesToScroll: 1
            };
        }
        This.state = {
            slicks: [],
            slickKey: 0
        };
        This.styles = {
            carousel: {
                overflow: 'hidden'
            },
            image: {
                width: '100%'
            }
        };
        return _this;
    }

    _createClass(Slider, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var This = this;
            This.props.slicks.map(function (slick) {
                return This.slickMaker(slick);
            });
        }
    }, {
        key: 'slickMaker',
        value: function slickMaker(slick) {
            var This = this;
            var newSlicks = This.state.slicks;
            var page = This.props.page;
            newSlicks.push(_react2.default.createElement(
                _Carousel2.default.Item,
                {
                    animateIn: true,
                    animateOut: true,
                    key: This.state.slickKey
                },
                _react2.default.createElement('img', { src: slick.imageSrc, alt: page.name + ' Banner' })
            ));
            This.setState({
                slickKey: This.state.slickKey++,
                slicks: newSlicks
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var page = this.props.page;
            return _react2.default.createElement(
                _Carousel2.default,
                {
                    style: this.styles.carousel,
                    controls: false,
                    indicators: false
                },
                _react2.default.createElement(
                    _Carousel2.default.Item,
                    {
                        animateIn: true,
                        animateOut: true
                    },
                    _react2.default.createElement('img', {
                        style: this.styles.image,
                        src: '/images/' + page.name + '/homeBanner.jpg', alt: page.name + ' Banner' })
                )
            );
        }
    }]);

    return Slider;
}(_react2.default.Component);

Slider.PropTypes = {
    settings: _propTypes2.default.object,
    slicks: _propTypes2.default.array.isRequired
};

function mapStateToProps(state) {
    return {
        page: state.page.page
    };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, {})(Slider);