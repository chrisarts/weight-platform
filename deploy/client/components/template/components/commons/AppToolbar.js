'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _reactRouter = require('react-router');

var _MenuItem = require('material-ui/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _IconMenu = require('material-ui/IconMenu');

var _IconMenu2 = _interopRequireDefault(_IconMenu);

var _IconButton = require('material-ui/IconButton');

var _IconButton2 = _interopRequireDefault(_IconButton);

var _AppBar = require('material-ui/AppBar');

var _AppBar2 = _interopRequireDefault(_AppBar);

var _Drawer = require('material-ui/Drawer');

var _Drawer2 = _interopRequireDefault(_Drawer);

var _Toolbar = require('material-ui/Toolbar');

var _svgIcons = require('material-ui/svg-icons');

var _jwtDecode = require('jwt-decode');

var _jwtDecode2 = _interopRequireDefault(_jwtDecode);

var _authActions = require('../../../actions/authActions');

var _userActions = require('../../../actions/userActions');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AppToolbar = function (_React$Component) {
    _inherits(AppToolbar, _React$Component);

    function AppToolbar(props) {
        _classCallCheck(this, AppToolbar);

        var _this = _possibleConstructorReturn(this, (AppToolbar.__proto__ || Object.getPrototypeOf(AppToolbar)).call(this, props));

        _this.drawerToggle = function () {
            _this.setState({
                drawerOpen: !_this.state.drawerOpen
            });
            console.log('click!');
        };

        _this.state = {
            toolbarStyle: {
                backgroundColor: 'rgba(0,0,0,0.5)',
                position: 'fixed',
                top: 0,
                width: '100%',
                height: 56,
                zIndex: 10
            },
            currentRoute: '/',
            isAuth: false,
            basketCount: 0,
            user: {},
            menuPadding: false,
            drawerOpen: false
        };
        _this.styles = {
            menuItem: {
                display: 'block',
                width: '100%',
                padding: '10px 0',
                color: '#666666',
                textDecoration: 'none',
                paddingLeft: '10%'
            },
            responsiveMenuItem: {
                position: 'relative',
                top: 6,
                marginRight: 10
            }
        };
        return _this;
    }

    _createClass(AppToolbar, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var This = this;
            window.addEventListener('scroll', this.handleScroll.bind(this));
            if (This.props.auth.isAuthenticated) {
                var decodedToken = (0, _jwtDecode2.default)(localStorage.jwtToken);
                This.setState({
                    isAuth: true,
                    user: decodedToken
                });
                This.props.getBasket(decodedToken.id).then(function (result) {
                    This.setState({
                        basketCount: result.data.basket.length,
                        currentRoute: This.context.router.location.pathname
                    });
                });
            }
            _reactRouter.browserHistory.listen(function (location) {
                document.body.scrollTop = 0;
                window.pageYOffset = 0;
                if (This.props.auth.isAuthenticated) {
                    var _decodedToken = (0, _jwtDecode2.default)(localStorage.jwtToken);
                    This.setState({
                        isAuth: true,
                        user: _decodedToken
                    });
                }
            });
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            window.removeEventListener('scroll', this.handleScroll.bind(this));
        }
    }, {
        key: 'handleScroll',
        value: function handleScroll(event) {
            var newToolbarStyle = this.state.toolbarStyle;
            if (event.target.body.scrollTop > 150) {
                newToolbarStyle.backgroundColor = '#1a1a1a';
                newToolbarStyle.height = 70;
                this.setState({
                    toolbarStyle: newToolbarStyle,
                    menuPadding: true
                });
            } else {
                newToolbarStyle.backgroundColor = 'rgba(0,0,0,0.5)';
                newToolbarStyle.height = 56;
                this.setState({
                    toolbarStyle: newToolbarStyle,
                    menuPadding: false
                });
            }
        }
    }, {
        key: 'routeChange',
        value: function routeChange(e) {
            e.preventDefault();
            var linkTo = e.target.parentNode.getAttribute('href');
            this.setState({
                currentRoute: linkTo
            });
            this.context.router.push(linkTo);
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var page = this.props.page;
            var windowWith = window.innerWidth;
            if (windowWith <= 1224) {
                return _react2.default.createElement(
                    'div',
                    null,
                    _react2.default.createElement(
                        _Drawer2.default,
                        {
                            open: this.state.drawerOpen
                        },
                        _react2.default.createElement(_AppBar2.default, {
                            className: 'app-toolbar-' + page.name,
                            style: this.state.toolbarStyle,
                            title: _react2.default.createElement('img', {
                                style: { height: '81%', padding: '13px 0px' },
                                src: '/images/' + page.name + '/logo.png', alt: page.trueName }),
                            iconElementLeft: _react2.default.createElement(
                                _IconButton2.default,
                                {
                                    tooltip: 'Abrir men\xFA',
                                    onClick: function onClick() {
                                        return _this2.drawerToggle();
                                    }
                                },
                                _react2.default.createElement(_svgIcons.NavigationMenu, null)
                            )
                        }),
                        _react2.default.createElement(
                            'div',
                            {
                                style: {
                                    marginTop: 70
                                }
                            },
                            _react2.default.createElement(
                                _reactRouter.Link,
                                {
                                    to: '/',
                                    className: 'menu-item-animated-background',
                                    style: this.styles.menuItem
                                },
                                _react2.default.createElement(_svgIcons.ActionHome, {
                                    style: this.styles.responsiveMenuItem,
                                    color: '#666666'
                                }),
                                ' ',
                                _react2.default.createElement(
                                    'span',
                                    null,
                                    'Home'
                                )
                            ),
                            _react2.default.createElement(
                                _reactRouter.Link,
                                {
                                    to: '/sobre-' + page.name,
                                    className: 'menu-item-animated-background',
                                    style: this.styles.menuItem
                                },
                                _react2.default.createElement(_svgIcons.ActionInfo, {
                                    style: this.styles.responsiveMenuItem,
                                    color: '#666666'
                                }),
                                ' ',
                                _react2.default.createElement(
                                    'span',
                                    null,
                                    'Sobre ',
                                    page.trueName
                                )
                            ),
                            _react2.default.createElement(
                                _reactRouter.Link,
                                {
                                    to: '/productos',
                                    className: 'menu-item-animated-background',
                                    style: this.styles.menuItem
                                },
                                _react2.default.createElement(_svgIcons.MapsStoreMallDirectory, {
                                    style: this.styles.responsiveMenuItem,
                                    color: '#666666'
                                }),
                                ' ',
                                _react2.default.createElement(
                                    'span',
                                    null,
                                    'Tienda'
                                )
                            ),
                            _react2.default.createElement(
                                _reactRouter.Link,
                                {
                                    to: '/blog',
                                    className: 'menu-item-animated-background',
                                    style: this.styles.menuItem
                                },
                                _react2.default.createElement(_svgIcons.CommunicationRssFeed, {
                                    style: this.styles.responsiveMenuItem,
                                    color: '#666666'
                                }),
                                ' ',
                                _react2.default.createElement(
                                    'span',
                                    null,
                                    'Blog'
                                )
                            ),
                            _react2.default.createElement(
                                _reactRouter.Link,
                                {
                                    to: '/contacto',
                                    className: 'menu-item-animated-background',
                                    style: this.styles.menuItem
                                },
                                _react2.default.createElement(_svgIcons.ActionPermPhoneMsg, {
                                    style: this.styles.responsiveMenuItem,
                                    color: '#666666'
                                }),
                                ' ',
                                _react2.default.createElement(
                                    'span',
                                    null,
                                    'Cont\xE1ctanos'
                                )
                            ),
                            this.state.isAuth ? _react2.default.createElement(
                                'div',
                                null,
                                _react2.default.createElement(
                                    _reactRouter.Link,
                                    {
                                        to: '/carrito',
                                        className: 'menu-item-animated-background',
                                        style: this.styles.menuItem
                                    },
                                    _react2.default.createElement(_svgIcons.ActionShoppingCart, {
                                        style: this.styles.responsiveMenuItem,
                                        color: '#666666'
                                    }),
                                    ' ',
                                    _react2.default.createElement(
                                        'span',
                                        null,
                                        'Carrito (0)'
                                    )
                                ),
                                _react2.default.createElement(
                                    _reactRouter.Link,
                                    {
                                        to: '/pedidos',
                                        className: 'menu-item-animated-background',
                                        style: this.styles.menuItem
                                    },
                                    _react2.default.createElement(_svgIcons.PlacesAirportShuttle, {
                                        style: this.styles.responsiveMenuItem,
                                        color: '#666666'
                                    }),
                                    ' ',
                                    _react2.default.createElement(
                                        'span',
                                        null,
                                        'Mis Pedidos'
                                    )
                                )
                            ) : _react2.default.createElement(
                                'div',
                                null,
                                _react2.default.createElement(
                                    _MenuItem2.default,
                                    null,
                                    'Iniciar Sesi\xF3n'
                                ),
                                _react2.default.createElement(
                                    _MenuItem2.default,
                                    null,
                                    'Registrarme!'
                                )
                            )
                        )
                    ),
                    _react2.default.createElement(_AppBar2.default, {
                        className: 'app-toolbar-' + page.name,
                        style: this.state.toolbarStyle,
                        title: _react2.default.createElement('img', {
                            style: { height: '81%', padding: '13px 0px' },
                            src: '/images/' + page.name + '/logo.png', alt: page.trueName }),
                        iconElementLeft: _react2.default.createElement(
                            _IconButton2.default,
                            {
                                tooltip: 'Abrir men\xFA',
                                onClick: function onClick() {
                                    return _this2.drawerToggle();
                                }
                            },
                            _react2.default.createElement(_svgIcons.NavigationMenu, null)
                        )
                    })
                );
            } else {
                return _react2.default.createElement(
                    'div',
                    null,
                    _react2.default.createElement(
                        _Toolbar.Toolbar,
                        {
                            className: 'app-toolbar-' + page.name,
                            style: this.state.toolbarStyle
                        },
                        _react2.default.createElement(
                            _Toolbar.ToolbarGroup,
                            { firstChild: true },
                            _react2.default.createElement(
                                'figure',
                                { className: 'page-logo' },
                                _react2.default.createElement('img', {
                                    style: { height: '100%' },
                                    src: '/images/' + page.name + '/logo.png', alt: page.trueName })
                            )
                        ),
                        _react2.default.createElement(
                            _Toolbar.ToolbarGroup,
                            null,
                            _react2.default.createElement(
                                'div',
                                {
                                    className: 'main-menu ' + page.name
                                },
                                _react2.default.createElement(
                                    _reactRouter.Link,
                                    {
                                        to: '/',
                                        className: (this.state.menuPadding ? "menu-link-padding" : "menu-link") + " " + page.name,
                                        activeClassName: 'active',
                                        activeStyle: this.state.currentRoute === '/' ? { color: "white" } : { color: page.letterColor1 },
                                        onClick: this.routeChange.bind(this)
                                    },
                                    _react2.default.createElement(
                                        'span',
                                        null,
                                        'Home'
                                    )
                                ),
                                _react2.default.createElement(
                                    _reactRouter.Link,
                                    {
                                        to: '/sobre-' + page.name,
                                        className: (this.state.menuPadding ? "menu-link-padding" : "menu-link") + " " + page.name,
                                        activeClassName: 'active',
                                        onClick: this.routeChange.bind(this)
                                    },
                                    _react2.default.createElement(
                                        'span',
                                        null,
                                        'Sobre ',
                                        page.trueName
                                    )
                                ),
                                _react2.default.createElement(
                                    _reactRouter.Link,
                                    {
                                        to: '/productos',
                                        className: (this.state.menuPadding ? "menu-link-padding" : "menu-link") + " " + page.name,
                                        activeClassName: 'active',
                                        onClick: this.routeChange.bind(this)
                                    },
                                    _react2.default.createElement(
                                        'span',
                                        null,
                                        'Tienda'
                                    )
                                ),
                                _react2.default.createElement(
                                    _reactRouter.Link,
                                    {
                                        to: '/blog',
                                        className: (this.state.menuPadding ? "menu-link-padding" : "menu-link") + " " + page.name,
                                        activeClassName: 'active',
                                        onClick: this.routeChange.bind(this)
                                    },
                                    _react2.default.createElement(
                                        'span',
                                        null,
                                        'Blog'
                                    )
                                ),
                                _react2.default.createElement(
                                    _reactRouter.Link,
                                    {
                                        to: '/contacto',
                                        className: (this.state.menuPadding ? "menu-link-padding" : "menu-link") + " " + page.name,
                                        activeClassName: 'active',
                                        onClick: this.routeChange.bind(this)
                                    },
                                    _react2.default.createElement(
                                        'span',
                                        null,
                                        'Contacto'
                                    )
                                )
                            )
                        ),
                        _react2.default.createElement(
                            _Toolbar.ToolbarGroup,
                            {
                                style: {
                                    width: '18%'
                                }
                            },
                            _react2.default.createElement(
                                'div',
                                { className: 'user-menu' },
                                this.state.isAuth ? _react2.default.createElement(
                                    _reactRouter.Link,
                                    {
                                        to: '/carrito',
                                        onChange: this.routeChange.bind(this),
                                        className: 'menu-link-basket ' + page.name
                                    },
                                    _react2.default.createElement(
                                        'div',
                                        {
                                            className: (this.state.menuPadding ? "basket-padding" : "basket-nopadding") + " " + page.name
                                        },
                                        'Ver Carrito (',
                                        this.state.basketCount,
                                        ')',
                                        _react2.default.createElement(_svgIcons.ActionShoppingBasket, {
                                            style: {
                                                marginLeft: 10,
                                                color: this.props.page.letterColor1
                                            }
                                        })
                                    )
                                ) : _react2.default.createElement(
                                    'div',
                                    { style: { float: 'left', display: 'none' } },
                                    '\xA0'
                                ),
                                _react2.default.createElement(
                                    'div',
                                    { className: 'profile-menu' },
                                    this.state.isAuth ? _react2.default.createElement(
                                        _IconMenu2.default,
                                        {
                                            iconButtonElement: _react2.default.createElement(
                                                _IconButton2.default,
                                                {
                                                    iconStyle: {
                                                        color: this.props.page.letterColor1
                                                    }
                                                },
                                                _react2.default.createElement(_svgIcons.ActionAccountCircle, null)
                                            ),
                                            style: { position: 'absolute' },
                                            anchorOrigin: { horizontal: 'right', vertical: 'bottom' },
                                            targetOrigin: { horizontal: 'right', vertical: 'top' },
                                            className: (this.state.menuPadding ? "icon-profile-menu-padding" : "icon-profile-menu") + " " + this.props.page.name
                                        },
                                        _react2.default.createElement(_MenuItem2.default, { primaryText: 'Ver Perfil' })
                                    ) : _react2.default.createElement(
                                        'div',
                                        { className: 'signup-menu' },
                                        _react2.default.createElement(
                                            _reactRouter.Link,
                                            {
                                                to: '/registrate',
                                                onChange: this.routeChange.bind(this),
                                                className: (this.state.menuPadding ? "menu-link-padding" : "menu-link") + " " + this.props.page.name
                                            },
                                            _react2.default.createElement(
                                                'span',
                                                null,
                                                'Registrate!'
                                            )
                                        ),
                                        _react2.default.createElement(
                                            _reactRouter.Link,
                                            {
                                                to: '/iniciar-sesion',
                                                onChange: this.routeChange.bind(this),
                                                className: (this.state.menuPadding ? "menu-link-padding" : "menu-link") + " " + this.props.page.name
                                            },
                                            _react2.default.createElement(
                                                'span',
                                                null,
                                                'Iniciar sesi\xF3n!'
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                );
            }
        }
    }]);

    return AppToolbar;
}(_react2.default.Component);

AppToolbar.PropTypes = {
    getBasket: _react2.default.PropTypes.func.isRequired,
    auth: _react2.default.PropTypes.object.isRequired,
    logout: _react2.default.PropTypes.func.isRequired
};

AppToolbar.contextTypes = {
    router: _react2.default.PropTypes.object.isRequired
};

function mapStateToProps(state) {
    return {
        auth: state.auth,
        page: state.page.page
    };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, { logout: _authActions.logout, getBasket: _userActions.getBasket })(AppToolbar);