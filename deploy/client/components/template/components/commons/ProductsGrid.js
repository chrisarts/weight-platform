'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = require('react-redux');

var _reactRouter = require('react-router');

var _reactBootstrap = require('react-bootstrap');

var _reactSlick = require('react-slick');

var _reactSlick2 = _interopRequireDefault(_reactSlick);

var _LoadingSection = require('../../../globalComponents/loading/LoadingSection');

var _LoadingSection2 = _interopRequireDefault(_LoadingSection);

var _productActions = require('../../../actions/productActions');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ProductsGrid = function (_React$Component) {
    _inherits(ProductsGrid, _React$Component);

    function ProductsGrid(props) {
        _classCallCheck(this, ProductsGrid);

        var _this = _possibleConstructorReturn(this, (ProductsGrid.__proto__ || Object.getPrototypeOf(ProductsGrid)).call(this, props));

        var slidesToRender = 3;
        if (window.innerWidth <= 1024 && window.innerWidth > 720) {
            slidesToRender = 2;
        } else if (window.innerWidth <= 720) {
            slidesToRender = 1;
        }
        _this.state = {
            products: [],
            pagination: {
                pageSize: _this.props.pageSize,
                page: _this.props.page.name
            },
            settings: {
                dots: true,
                infinite: true,
                speed: 500,
                slidesToShow: slidesToRender,
                slidesToScroll: slidesToRender
            },
            slicks: [],
            slickKey: 0,
            isLoading: true
        };
        return _this;
    }

    _createClass(ProductsGrid, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var This = this;
            This.props.getProductList(This.state.pagination).then(function (result) {
                This.setState({
                    products: result.data.filteredProducts,
                    isLoading: false
                });
                if (result.data.filteredProducts.length > 0) {
                    This.slickMaker();
                }
            });
        }
    }, {
        key: 'slickMaker',
        value: function slickMaker() {
            var _this2 = this;

            var This = this;
            var responsive = window.innerWidth < 1024;
            This.state.products.map(function (product) {
                var newSlick = This.state.slicks;
                console.log('asd');
                var newDescription = product.description.replace(/<\/?[^>]+(>|$)/g, "");
                newDescription = newDescription.substring(0, 200);
                var urlName = product.name.replace(' ', '-');
                var media = JSON.parse(product.media);
                var image = '';
                var count = 0;
                for (var key in media) {
                    if (media.hasOwnProperty(key) && count === 0) {
                        image = media[key];
                        ++count;
                    }
                }

                newSlick.push(_react2.default.createElement(
                    _reactBootstrap.Col,
                    { md: 4, key: product.id },
                    _react2.default.createElement(
                        'figure',
                        { style: responsive ? {
                                width: '90%'
                            } : {
                                position: 'relative'
                            } },
                        _react2.default.createElement('img', {
                            src: image,
                            alt: '',
                            style: responsive ? {
                                height: 250,
                                width: 'auto',
                                maxWidth: window.innerWidth - 15,
                                margin: '0 auto'
                            } : {
                                height: 350,
                                width: 'auto',
                                margin: '0 auto'
                            }
                        })
                    ),
                    _react2.default.createElement(
                        'h4',
                        { style: { textAlign: 'center', fontWeight: 'bold' } },
                        product.name
                    ),
                    _react2.default.createElement(
                        'p',
                        null,
                        newDescription + '...'
                    ),
                    _react2.default.createElement(
                        'div',
                        {
                            className: 'product-item-buttons'
                        },
                        _react2.default.createElement(
                            _reactRouter.Link,
                            {
                                to: '/producto/' + urlName + '/' + product.id,
                                className: 'product-link ' + _this2.props.page.name
                            },
                            'APRENDE M\xC1S'
                        )
                    )
                ));
                This.setState({
                    slicks: newSlick,
                    slickKey: This.state.slickKey++
                });
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var page = this.props.page;
            var slicks = [];
            if (this.state.products.length > 0) {
                slicks = this.state.slicks;
            } else {
                slicks.push(_react2.default.createElement(
                    'div',
                    { key: 999999 },
                    '\xA0'
                ));
            }
            if (this.state.isLoading) {
                return _react2.default.createElement(_LoadingSection2.default, {
                    payload: this.state.isLoading,
                    height: '576px',
                    width: '100%',
                    text: 'Cargando Productos...'
                });
            }
            if (this.state.slicks.length > 0) {
                return _react2.default.createElement(
                    _reactSlick2.default,
                    _extends({}, this.state.settings, { className: 'products-carousel' }),
                    slicks
                );
            } else {
                return _react2.default.createElement(
                    'div',
                    null,
                    _react2.default.createElement(
                        'h2',
                        {
                            style: {
                                textAlign: 'center',
                                padding: 50
                            }
                        },
                        'No existen articulos a\xFAn.'
                    )
                );
            }
        }
    }]);

    return ProductsGrid;
}(_react2.default.Component);

ProductsGrid.PropTypes = {
    getProductList: _propTypes2.default.func.isRequired,
    pageSize: _propTypes2.default.number.isRequired
};

function mapStateToProps(state) {
    return {
        auth: state.auth,
        page: state.page.page
    };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, { getProductList: _productActions.getProductList })(ProductsGrid);