'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _reactRouter = require('react-router');

var _reactBootstrap = require('react-bootstrap');

var _TextField = require('material-ui/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _IconButton = require('material-ui/IconButton');

var _IconButton2 = _interopRequireDefault(_IconButton);

var _FontIcon = require('material-ui/FontIcon');

var _FontIcon2 = _interopRequireDefault(_FontIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Footer = function (_React$Component) {
    _inherits(Footer, _React$Component);

    function Footer(props) {
        _classCallCheck(this, Footer);

        var _this = _possibleConstructorReturn(this, (Footer.__proto__ || Object.getPrototypeOf(Footer)).call(this, props));

        _this.styles = {
            global: {
                background: _this.props.page.footerBackground,
                fontSize: 14,
                paddingTop: 25
            },
            grid: {
                margin: '0 auto',
                width: '90%'
            },
            socialIcons: {
                marginLeft: 8
            }
        };
        return _this;
    }

    _createClass(Footer, [{
        key: 'render',
        value: function render() {
            var page = this.props.page;
            return _react2.default.createElement(
                'div',
                { className: 'footer', style: this.styles.global },
                _react2.default.createElement(
                    _reactBootstrap.Grid,
                    { style: this.styles.grid },
                    _react2.default.createElement(
                        _reactBootstrap.Col,
                        { md: 3, className: 'footer-block' },
                        _react2.default.createElement(
                            'h4',
                            null,
                            'Mapa del Sitio'
                        ),
                        _react2.default.createElement(
                            'ul',
                            { className: 'footer-list ' + page.name },
                            _react2.default.createElement(
                                'li',
                                null,
                                _react2.default.createElement(
                                    _reactRouter.Link,
                                    {
                                        to: '/'
                                    },
                                    'Home'
                                )
                            ),
                            _react2.default.createElement(
                                'li',
                                null,
                                _react2.default.createElement(
                                    _reactRouter.Link,
                                    {
                                        to: '/sobre-' + page.name
                                    },
                                    'Sobre Fit+'
                                )
                            ),
                            _react2.default.createElement(
                                'li',
                                null,
                                _react2.default.createElement(
                                    _reactRouter.Link,
                                    {
                                        to: '/productos'
                                    },
                                    'Tienda'
                                )
                            ),
                            _react2.default.createElement(
                                'li',
                                null,
                                _react2.default.createElement(
                                    _reactRouter.Link,
                                    {
                                        to: '/blog'
                                    },
                                    'Blog'
                                )
                            ),
                            _react2.default.createElement(
                                'li',
                                null,
                                _react2.default.createElement(
                                    _reactRouter.Link,
                                    {
                                        to: '/contacto'
                                    },
                                    'Contacto'
                                )
                            )
                        )
                    ),
                    _react2.default.createElement(
                        _reactBootstrap.Col,
                        { md: 3, className: 'footer-block' },
                        _react2.default.createElement(
                            'h4',
                            null,
                            'Informaci\xF3n vital'
                        ),
                        _react2.default.createElement(
                            'ul',
                            { className: 'footer-list ' + page.name },
                            _react2.default.createElement(
                                'li',
                                null,
                                _react2.default.createElement(
                                    'a',
                                    { href: '/devoluciones' },
                                    'Politicas de devoluci\xF3n'
                                )
                            )
                        )
                    ),
                    _react2.default.createElement(
                        _reactBootstrap.Col,
                        { md: 3, className: 'footer-block' },
                        _react2.default.createElement(
                            'h4',
                            null,
                            'Ubicaci\xF3n'
                        ),
                        _react2.default.createElement(
                            'ul',
                            { className: 'footer-list ' + page.name },
                            _react2.default.createElement(
                                'li',
                                null,
                                _react2.default.createElement(
                                    'a',
                                    { href: '#' },
                                    'Cali, Colombia'
                                )
                            ),
                            _react2.default.createElement(
                                'li',
                                null,
                                _react2.default.createElement(
                                    'a',
                                    { href: '#' },
                                    'Bogot\xE1, Colombia'
                                )
                            )
                        ),
                        _react2.default.createElement(
                            'div',
                            null,
                            _react2.default.createElement(_TextField2.default, {
                                hintText: 'Buscar en este sitio',
                                floatingLabelText: '\xBFBuscas algo?'
                            }),
                            _react2.default.createElement(_IconButton2.default, { iconClassName: 'muidocs-icon-custom-github' })
                        )
                    ),
                    _react2.default.createElement(
                        _reactBootstrap.Col,
                        { md: 3, className: 'footer-block' },
                        _react2.default.createElement(
                            'h4',
                            null,
                            'Siguenos'
                        ),
                        _react2.default.createElement(
                            'div',
                            { className: 'social-icons' },
                            _react2.default.createElement(
                                'a',
                                {
                                    href: '#',
                                    style: this.styles.socialIcons
                                },
                                _react2.default.createElement(_FontIcon2.default, {
                                    className: 'fa fa-facebook',
                                    color: '#777777'
                                })
                            ),
                            _react2.default.createElement(
                                'a',
                                {
                                    href: '#',
                                    style: this.styles.socialIcons
                                },
                                _react2.default.createElement(_FontIcon2.default, {
                                    className: 'fa fa-google-plus',
                                    color: '#777777'
                                })
                            ),
                            _react2.default.createElement(
                                'a',
                                {
                                    href: '#',
                                    style: this.styles.socialIcons
                                },
                                _react2.default.createElement(_FontIcon2.default, {
                                    className: 'fa fa-twitter',
                                    color: '#777777'
                                })
                            ),
                            _react2.default.createElement(
                                'a',
                                {
                                    href: '#',
                                    style: this.styles.socialIcons
                                },
                                _react2.default.createElement(_FontIcon2.default, {
                                    className: 'fa fa-instagram',
                                    color: '#777777'
                                })
                            ),
                            _react2.default.createElement(
                                'a',
                                {
                                    href: '#',
                                    style: this.styles.socialIcons
                                },
                                _react2.default.createElement(_FontIcon2.default, {
                                    className: 'fa fa-pinterest-p',
                                    color: '#777777'
                                })
                            )
                        )
                    )
                )
            );
        }
    }]);

    return Footer;
}(_react2.default.Component);

function mapStateToProps(state) {
    return {
        auth: state.auth,
        page: state.page.page
    };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, {})(Footer);