'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = require('react-redux');

var _FlatButton = require('material-ui/FlatButton');

var _FlatButton2 = _interopRequireDefault(_FlatButton);

var _Table = require('material-ui/Table');

var _jwtDecode = require('jwt-decode');

var _jwtDecode2 = _interopRequireDefault(_jwtDecode);

var _LoadingSection = require('../../../globalComponents/loading/LoadingSection');

var _LoadingSection2 = _interopRequireDefault(_LoadingSection);

var _ProductItem = require('./ProductItem');

var _ProductItem2 = _interopRequireDefault(_ProductItem);

var _userActions = require('../../../actions/userActions');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Basket = function (_React$Component) {
    _inherits(Basket, _React$Component);

    function Basket(props) {
        _classCallCheck(this, Basket);

        var _this = _possibleConstructorReturn(this, (Basket.__proto__ || Object.getPrototypeOf(Basket)).call(this, props));

        _this.state = {
            products: [],
            isAuth: false,
            user: {},
            deleteOpen: false,
            isLoading: true
        };
        return _this;
    }

    _createClass(Basket, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var This = this;
            if (This.props.auth.isAuthenticated) {
                var decodedToken = (0, _jwtDecode2.default)(localStorage.jwtToken);
                This.setState({
                    isAuth: true,
                    user: decodedToken
                });
                This.props.getBasket(decodedToken.id).then(function (result) {
                    console.log('Basket: ', result);
                    This.setState({
                        products: result.data.basket,
                        isLoading: false
                    });
                });
            }
        }
    }, {
        key: 'render',
        value: function render() {
            return _react2.default.createElement(
                'div',
                { className: 'page-basket' },
                _react2.default.createElement(
                    'h1',
                    null,
                    'Productos en Carrito'
                ),
                _react2.default.createElement(_LoadingSection2.default, {
                    payload: this.state.isLoading,
                    width: '100%',
                    height: '300px'
                }),
                _react2.default.createElement(
                    'div',
                    {
                        style: {
                            width: '100%',
                            textAlign: 'center'
                        }
                    },
                    _react2.default.createElement(_FlatButton2.default, {
                        label: 'Proceder al pago',
                        backgroundColor: '#89286d',
                        style: {
                            fontFamily: 'cuyabra',
                            color: 'white',
                            fontWeight: 'bold',
                            padding: '0px 25px'
                        },
                        href: '/pago'
                    })
                ),
                _react2.default.createElement(
                    _Table.Table,
                    null,
                    _react2.default.createElement(
                        _Table.TableHeader,
                        {
                            displaySelectAll: false,
                            adjustForCheckbox: false
                        },
                        _react2.default.createElement(
                            _Table.TableRow,
                            null,
                            _react2.default.createElement(
                                _Table.TableHeaderColumn,
                                null,
                                'Im\xE1gen'
                            ),
                            _react2.default.createElement(
                                _Table.TableHeaderColumn,
                                null,
                                'Nombre'
                            ),
                            _react2.default.createElement(
                                _Table.TableHeaderColumn,
                                null,
                                'Cantidad'
                            ),
                            _react2.default.createElement(
                                _Table.TableHeaderColumn,
                                null,
                                'Precio'
                            ),
                            _react2.default.createElement(
                                _Table.TableHeaderColumn,
                                null,
                                'Editar'
                            ),
                            _react2.default.createElement(
                                _Table.TableHeaderColumn,
                                null,
                                'Eliminar'
                            )
                        )
                    ),
                    _react2.default.createElement(
                        _Table.TableBody,
                        {
                            displayRowCheckbox: false
                        },
                        this.state.products.map(function (product) {
                            return _react2.default.createElement(_ProductItem2.default, {
                                key: product.id,
                                basketProduct: product
                            });
                        })
                    )
                )
            );
        }
    }]);

    return Basket;
}(_react2.default.Component);

Basket.PropTypes = {
    getBasket: _propTypes2.default.func.isRequired
};

function mapStateToProps(state) {
    return {
        auth: state.auth
    };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, { getBasket: _userActions.getBasket })(Basket);