'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = require('react-redux');

var _reactRouter = require('react-router');

var _Dialog = require('material-ui/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _FlatButton = require('material-ui/FlatButton');

var _FlatButton2 = _interopRequireDefault(_FlatButton);

var _TextField = require('material-ui/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _Table = require('material-ui/Table');

var _svgIcons = require('material-ui/svg-icons');

var _productActions = require('../../../actions/productActions');

var _userActions = require('../../../actions/userActions');

var _flashMessages = require('../../../actions/flashMessages');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ProductItem = function (_React$Component) {
    _inherits(ProductItem, _React$Component);

    function ProductItem(props) {
        _classCallCheck(this, ProductItem);

        var _this = _possibleConstructorReturn(this, (ProductItem.__proto__ || Object.getPrototypeOf(ProductItem)).call(this, props));

        _this.state = {
            id: 0,
            name: '',
            price: '',
            media: '{}',
            quantity: _this.props.basketProduct.quantity,
            deleteOpen: false,
            productToDelete: 0,
            editOpen: false,
            productToEdit: 0
        };
        _this.styles = {
            deleteActions: {
                float: 'right',
                buttons: {
                    marginRight: 20
                }
            }
        };
        return _this;
    }

    _createClass(ProductItem, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var _this2 = this;

            var This = this;
            This.props.getProduct({ id: This.props.basketProduct.productId }).then(function (result) {
                var price = parseInt(result.data.product.price, 0);
                var subTotal = price * parseInt(_this2.props.basketProduct.quantity, 0);
                var priceFormat = subTotal.toLocaleString(undefined, // use a string like 'en-US' to override browser locale
                { minimumFractionDigits: 0 });
                This.setState({
                    id: result.data.product.id,
                    name: result.data.product.name,
                    price: priceFormat,
                    media: result.data.product.media
                });
            });
        }
    }, {
        key: 'onChange',
        value: function onChange(event) {
            this.setState(_defineProperty({}, event.target.name, event.target.value));
        }
    }, {
        key: 'deleteActionsNegative',
        value: function deleteActionsNegative() {
            this.setState({
                deleteOpen: false
            });
        }
    }, {
        key: 'deleteActionsPositive',
        value: function deleteActionsPositive() {
            var This = this;
            This.props.deleteBasketProduct({ id: This.props.basketProduct.id }).then(function () {
                This.props.addFlashMessage({
                    type: 'success',
                    text: 'Producto Eliminado'
                });
                This.setState({
                    deleteOpen: false
                });
                window.location.reload();
            });
        }
    }, {
        key: 'handleDeleteOpen',
        value: function handleDeleteOpen() {
            this.setState({
                deleteOpen: true
            });
        }
    }, {
        key: 'handleEditOpen',
        value: function handleEditOpen() {
            this.setState({
                editOpen: true
            });
        }
    }, {
        key: 'editActionsNegative',
        value: function editActionsNegative() {
            this.setState({
                editOpen: false
            });
        }
    }, {
        key: 'editActionsPositive',
        value: function editActionsPositive() {
            var This = this;
            This.props.updateBasketProduct({
                id: This.props.basketProduct.id,
                quantity: This.state.quantity
            }).then(function () {
                window.location.reload();
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var media = JSON.parse(this.state.media);
            var count = 0;
            var image = '';
            for (var key in media) {
                if (media.hasOwnProperty(key) && count === 0) {
                    image = media[key];
                    ++count;
                }
            }
            return _react2.default.createElement(
                _Table.TableRow,
                { key: this.state.id },
                _react2.default.createElement(
                    _Table.TableRowColumn,
                    null,
                    _react2.default.createElement(
                        'figure',
                        null,
                        _react2.default.createElement('img', {
                            height: 100,
                            src: image, alt: '' })
                    )
                ),
                _react2.default.createElement(
                    _Table.TableRowColumn,
                    null,
                    this.state.name
                ),
                _react2.default.createElement(
                    _Table.TableRowColumn,
                    null,
                    this.props.basketProduct.quantity
                ),
                _react2.default.createElement(
                    _Table.TableRowColumn,
                    null,
                    '$ ',
                    this.state.price
                ),
                _react2.default.createElement(
                    _Table.TableRowColumn,
                    null,
                    _react2.default.createElement(
                        _reactRouter.Link,
                        { onClick: this.handleEditOpen.bind(this) },
                        _react2.default.createElement(_svgIcons.ContentCreate, null)
                    ),
                    _react2.default.createElement(
                        _Dialog2.default,
                        {
                            title: 'Editar cantidad de productos en el pedido',
                            open: this.state.editOpen
                        },
                        _react2.default.createElement(
                            'div',
                            null,
                            _react2.default.createElement(_TextField2.default, {
                                name: 'quantity',
                                floatingLabelText: 'Cantidad de Productos *',
                                value: this.state.quantity,
                                type: 'number',
                                onChange: this.onChange.bind(this),
                                style: {
                                    width: '100%'
                                }
                            })
                        ),
                        _react2.default.createElement(
                            'div',
                            {
                                className: 'delete-actions',
                                style: this.styles.deleteActions
                            },
                            _react2.default.createElement(_FlatButton2.default, {
                                label: 'No Actualizar',
                                onClick: this.editActionsNegative.bind(this),
                                backgroundColor: '#a4c639',
                                hoverColor: '#8AA62F',
                                style: this.styles.deleteActions.buttons
                            }),
                            _react2.default.createElement(_FlatButton2.default, {
                                label: 'Actualizar',
                                onClick: this.editActionsPositive.bind(this),
                                backgroundColor: '#9b2211',
                                hoverColor: '#8b1e0f',
                                style: this.styles.deleteActions.buttons
                            })
                        )
                    )
                ),
                _react2.default.createElement(
                    _Table.TableRowColumn,
                    null,
                    _react2.default.createElement(
                        _reactRouter.Link,
                        { onClick: this.handleDeleteOpen.bind(this) },
                        _react2.default.createElement(_svgIcons.ContentClear, null)
                    ),
                    _react2.default.createElement(
                        _Dialog2.default,
                        {
                            title: '\xBFEst\xE1s seguro de querer eliminar \xE9ste Producto?',
                            open: this.state.deleteOpen
                        },
                        _react2.default.createElement(
                            'div',
                            {
                                className: 'delete-actions',
                                style: this.styles.deleteActions
                            },
                            _react2.default.createElement(_FlatButton2.default, {
                                label: 'No Eliminar',
                                onClick: this.deleteActionsNegative.bind(this),
                                backgroundColor: '#a4c639',
                                hoverColor: '#8AA62F',
                                style: this.styles.deleteActions.buttons
                            }),
                            _react2.default.createElement(_FlatButton2.default, {
                                label: 'Eliminar',
                                onClick: this.deleteActionsPositive.bind(this, this.state.id),
                                backgroundColor: '#9b2211',
                                hoverColor: '#8b1e0f',
                                style: this.styles.deleteActions.buttons
                            })
                        )
                    )
                )
            );
        }
    }]);

    return ProductItem;
}(_react2.default.Component);

ProductItem.PropTypes = {
    getProduct: _propTypes2.default.object.isRequired,
    basketProduct: _propTypes2.default.object.isRequired,
    deleteBasketProduct: _propTypes2.default.func.isRequired,
    addFlashMessage: _propTypes2.default.func.isRequired,
    updateBasketProduct: _propTypes2.default.func.isRequired
};

exports.default = (0, _reactRedux.connect)(null, {
    getProduct: _productActions.getProduct,
    deleteBasketProduct: _userActions.deleteBasketProduct,
    addFlashMessage: _flashMessages.addFlashMessage,
    updateBasketProduct: _userActions.updateBasketProduct
})(ProductItem);