'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = require('react-redux');

var _reactHtmlParser = require('react-html-parser');

var _reactHtmlParser2 = _interopRequireDefault(_reactHtmlParser);

var _articleActions = require('../../actions/articleActions');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Article = function (_React$Component) {
    _inherits(Article, _React$Component);

    function Article(props) {
        _classCallCheck(this, Article);

        var _this = _possibleConstructorReturn(this, (Article.__proto__ || Object.getPrototypeOf(Article)).call(this, props));

        _this.state = {
            article: {},
            image: '',
            content: _react2.default.createElement(
                'div',
                null,
                '\xA0'
            )
        };
        return _this;
    }

    _createClass(Article, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var This = this;
            This.props.getArticle({ id: This.props.params.articleId }).then(function (result) {
                var article = result.data.article;
                var content = (0, _reactHtmlParser2.default)(article.content);
                var media = JSON.parse(article.media);
                var count = 0;
                var image = '';
                for (var key in media) {
                    if (media.hasOwnProperty(key) && count === 0) {
                        image = media[key];
                        ++count;
                    }
                }
                This.setState({
                    article: article,
                    image: image,
                    content: content
                });
            });
        }
    }, {
        key: 'render',
        value: function render() {
            return _react2.default.createElement(
                'div',
                { className: 'article-page' },
                _react2.default.createElement(
                    'figure',
                    null,
                    _react2.default.createElement('img', { src: this.state.image, alt: this.state.article.title })
                ),
                _react2.default.createElement(
                    'h1',
                    null,
                    this.state.article.title
                ),
                _react2.default.createElement(
                    'div',
                    null,
                    this.state.content
                )
            );
        }
    }]);

    return Article;
}(_react2.default.Component);

Article.PropTypes = {
    getArticle: _propTypes2.default.func.isRequired
};

function mapStateToProps(state) {
    return {
        page: state.page.page
    };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, { getArticle: _articleActions.getArticle })(Article);