'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = require('react-redux');

var _reactSlick = require('react-slick');

var _reactSlick2 = _interopRequireDefault(_reactSlick);

var _reactBootstrap = require('react-bootstrap');

var _svgIcons = require('material-ui/svg-icons');

var _FlatButton = require('material-ui/FlatButton');

var _FlatButton2 = _interopRequireDefault(_FlatButton);

var _TextField = require('material-ui/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _reactHtmlParser = require('react-html-parser');

var _reactHtmlParser2 = _interopRequireDefault(_reactHtmlParser);

var _jwtDecode = require('jwt-decode');

var _jwtDecode2 = _interopRequireDefault(_jwtDecode);

var _reactHelmet = require('react-helmet');

var _ProductsGridFull = require('./commons/ProductsGridFull');

var _ProductsGridFull2 = _interopRequireDefault(_ProductsGridFull);

var _LoadingSection = require('../../globalComponents/loading/LoadingSection');

var _LoadingSection2 = _interopRequireDefault(_LoadingSection);

var _productActions = require('../../actions/productActions');

var _userActions = require('../../actions/userActions');

var _flashMessages = require('../../actions/flashMessages');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Product = function (_React$Component) {
    _inherits(Product, _React$Component);

    function Product(props) {
        _classCallCheck(this, Product);

        var _this = _possibleConstructorReturn(this, (Product.__proto__ || Object.getPrototypeOf(Product)).call(this, props));

        _this.state = {
            name: '',
            description: '',
            media: {},
            page: '',
            price: '',
            quantity: 1,
            isLoading: true
        };
        _this.productSlider = {
            dots: true,
            fade: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            style: {
                width: 'auto',
                height: '100%',
                maxHeight: '500px',
                margin: '0 auto'
            }
        };
        return _this;
    }

    _createClass(Product, [{
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProps) {
            this.setState({
                isLoading: true
            });
            this.chargeProduct(nextProps.params.identifier);
        }
    }, {
        key: 'onChangeQuantity',
        value: function onChangeQuantity(event) {
            var This = this;
            if (event.target.value > 0) {
                This.setState({
                    quantity: event.target.value
                });
            } else {
                window.alert('Valor inválido.');
            }
        }
    }, {
        key: 'componentDidMount',
        value: function componentDidMount() {
            this.chargeProduct(this.props.params.identifier);
        }
    }, {
        key: 'chargeProduct',
        value: function chargeProduct(id) {
            var This = this;
            This.props.getProduct({ id: id }).then(function (result) {
                var newDescription = (0, _reactHtmlParser2.default)(result.data.product.description);
                var media = JSON.parse(result.data.product.media);
                var count = 0;
                var image = '';
                for (var key in media) {
                    if (media.hasOwnProperty(key) && count === 0) {
                        image = media[key];
                        ++count;
                    }
                }
                var price = parseInt(result.data.product.price, 0);
                var priceFormat = price.toLocaleString(undefined, // use a string like 'en-US' to override browser locale
                { minimumFractionDigits: 0 });
                This.setState({
                    name: result.data.product.name,
                    price: priceFormat,
                    page: result.data.product.page,
                    description: newDescription,
                    media: image,
                    isLoading: false
                });
            });
        }
    }, {
        key: 'addToCar',
        value: function addToCar() {
            var This = this;
            if (This.props.auth.isAuthenticated) {
                This.props.findProduct(This.props.params.identifier).then(function (result) {
                    if (result.data.basket.length === 0) {
                        var decodedToken = (0, _jwtDecode2.default)(localStorage.jwtToken);
                        var params = {
                            productId: This.props.params.identifier,
                            userId: decodedToken.id,
                            quantity: This.state.quantity,
                            page: 'fitmas'
                        };
                        This.props.addToCar(params).then(function () {
                            This.props.addFlashMessage({
                                type: 'success',
                                text: 'Producto añadido al carrito'
                            });
                        });
                    } else {
                        window.alert('El producto ya está en el carrito');
                    }
                });
            } else {
                window.alert('Debes loguearte para agregar productos al carrito');
            }
        }
    }, {
        key: 'render',
        value: function render() {

            return _react2.default.createElement(
                'div',
                { style: {
                        paddingTop: 60,
                        width: '100%',
                        margin: '0 auto',
                        marginBottom: 70
                    } },
                _react2.default.createElement(
                    _reactHelmet.Helmet,
                    null,
                    _react2.default.createElement(
                        'title',
                        null,
                        this.state.name
                    )
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'product-preview' },
                    _react2.default.createElement(_LoadingSection2.default, {
                        payload: this.state.isLoading,
                        height: '576px',
                        width: '100%'
                    }),
                    _react2.default.createElement(
                        _reactBootstrap.Grid,
                        null,
                        _react2.default.createElement(
                            _reactBootstrap.Row,
                            { className: 'product-image-features' },
                            _react2.default.createElement(
                                _reactBootstrap.Col,
                                { md: 4, className: 'product-media' },
                                _react2.default.createElement(
                                    _reactSlick2.default,
                                    this.productSlider,
                                    _react2.default.createElement(
                                        'div',
                                        null,
                                        _react2.default.createElement('img', {
                                            src: this.state.media, alt: '',
                                            style: this.productSlider.style
                                        })
                                    )
                                )
                            ),
                            _react2.default.createElement(
                                _reactBootstrap.Col,
                                { md: 8, className: 'product-features' },
                                _react2.default.createElement(
                                    'div',
                                    { className: 'product-head' },
                                    _react2.default.createElement(
                                        'h1',
                                        null,
                                        this.state.name
                                    ),
                                    _react2.default.createElement(
                                        'div',
                                        { className: 'basket' },
                                        _react2.default.createElement(
                                            'div',
                                            { className: 'product-quantity' },
                                            _react2.default.createElement(_TextField2.default, {
                                                name: 'quantity',
                                                value: this.state.quantity,
                                                type: 'number',
                                                style: {
                                                    width: 60,
                                                    background: 'rgba(255,255,255,0.2)'
                                                },
                                                inputStyle: {
                                                    textAlign: 'center'
                                                },
                                                underlineFocusStyle: {
                                                    borderColor: '#fb2900'
                                                },
                                                onChange: this.onChangeQuantity.bind(this)
                                            })
                                        ),
                                        _react2.default.createElement(
                                            'strong',
                                            null,
                                            '$ ',
                                            this.state.price
                                        ),
                                        _react2.default.createElement(_FlatButton2.default, {
                                            label: 'A\xF1adir al Carrito',
                                            backgroundColor: '#ff2a00',
                                            icon: _react2.default.createElement(_svgIcons.ActionShoppingBasket, null),
                                            style: {
                                                color: 'white'
                                            },
                                            onClick: this.addToCar.bind(this)
                                        })
                                    )
                                ),
                                _react2.default.createElement(
                                    'div',
                                    { className: 'product-description' },
                                    this.state.description
                                )
                            )
                        )
                    ),
                    _react2.default.createElement('hr', { style: { width: '100%' } }),
                    _react2.default.createElement(_ProductsGridFull2.default, { pageSize: 3 })
                )
            );
        }
    }]);

    return Product;
}(_react2.default.Component);

Product.PropTypes = {
    getProduct: _propTypes2.default.func.isRequired,
    addToCar: _propTypes2.default.func.isRequired,
    auth: _react2.default.PropTypes.object.isRequired,
    findProduct: _react2.default.PropTypes.func.isRequired,
    addFlashMessage: _react2.default.PropTypes.func.isRequired
};

function mapStateToProps(state) {
    return {
        auth: state.auth
    };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, { getProduct: _productActions.getProduct, addToCar: _userActions.addToCar, findProduct: _userActions.findProduct, addFlashMessage: _flashMessages.addFlashMessage })(Product);