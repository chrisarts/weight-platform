'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _TextField = require('material-ui/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _RaisedButton = require('material-ui/RaisedButton');

var _RaisedButton2 = _interopRequireDefault(_RaisedButton);

var _reactRedux = require('react-redux');

var _authActions = require('../../../actions/authActions');

var _validator = require('validator');

var _validator2 = _interopRequireDefault(_validator);

var _isEmpty = require('lodash/isEmpty');

var _isEmpty2 = _interopRequireDefault(_isEmpty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function validateInput(data) {
    var errors = {};

    if (_validator2.default.isEmpty(data.name)) {
        errors.name = 'Este campo es obligatorio';
    }

    if (_validator2.default.isEmpty(data.email)) {
        errors.email = 'Este campo es obligatorio';
    }

    if (_validator2.default.isEmpty(data.password)) {
        errors.password = 'Este campo es obligatorio';
    }
    if (_validator2.default.isEmpty(data.confirmPassword)) {
        errors.confirmPassword = 'Este campo es obligatorio';
    }

    if (data.password !== data.confirmPassword) {
        errors.password = 'Las contraseñas no coinciden';
        errors.confirmPassword = 'Las contraseñas no coinciden';
    }

    return {
        errors: errors,
        isValid: (0, _isEmpty2.default)(errors)
    };
}

var SignUpForm = function (_React$Component) {
    _inherits(SignUpForm, _React$Component);

    function SignUpForm(props) {
        _classCallCheck(this, SignUpForm);

        var _this = _possibleConstructorReturn(this, (SignUpForm.__proto__ || Object.getPrototypeOf(SignUpForm)).call(this, props));

        _this.state = {
            name: '',
            email: '',
            password: '',
            gender: '',
            birthDate: '2000-01-01',
            telephone: '',
            city: '',
            isAdmin: false,
            isSuperAdmin: false,
            confirmPassword: '',
            errors: {},
            isLoading: false
        };
        _this.onChange = _this.onChange.bind(_this);
        _this.onSubmit = _this.onSubmit.bind(_this);
        return _this;
    }

    _createClass(SignUpForm, [{
        key: 'onChange',
        value: function onChange(event) {
            this.setState(_defineProperty({}, event.target.name, event.target.value));
        }
    }, {
        key: 'isValid',
        value: function isValid() {
            var _validateInput = validateInput(this.state),
                errors = _validateInput.errors,
                isValid = _validateInput.isValid;

            if (!isValid) {
                this.setState({ errors: errors });
            }

            return isValid;
        }
    }, {
        key: 'onSubmit',
        value: function onSubmit(event) {
            var _this2 = this;

            event.preventDefault();
            if (this.isValid()) {
                this.setState({
                    errors: {},
                    isLoading: true
                });
                this.props.signup(this.state).then(function (res) {
                    return window.location.href = '/';
                }, function (err) {
                    return _this2.setState({ errors: err.response.data.errors, isLoading: false });
                });
            }
        }
    }, {
        key: 'render',
        value: function render() {
            var _state = this.state,
                errors = _state.errors,
                identifier = _state.identifier;

            return _react2.default.createElement(
                'form',
                { onSubmit: this.onSubmit },
                _react2.default.createElement(
                    'hgroup',
                    null,
                    _react2.default.createElement(
                        'h1',
                        null,
                        'Registrate'
                    ),
                    errors.form && _react2.default.createElement(
                        'h4',
                        null,
                        errors.form
                    )
                ),
                _react2.default.createElement(_TextField2.default, {
                    name: 'name',
                    floatingLabelText: 'Nombre del usuario',
                    value: identifier,
                    errorText: errors.name,
                    onChange: this.onChange
                }),
                _react2.default.createElement(_TextField2.default, {
                    name: 'email',
                    floatingLabelText: 'Correo electr\xF3nico',
                    value: identifier,
                    errorText: errors.email,
                    onChange: this.onChange
                }),
                _react2.default.createElement(_TextField2.default, {
                    name: 'password',
                    floatingLabelText: 'Contrase\xF1a',
                    value: this.state.password,
                    errorText: errors.password,
                    onChange: this.onChange,
                    type: 'password'
                }),
                _react2.default.createElement(_TextField2.default, {
                    name: 'confirmPassword',
                    floatingLabelText: 'Confirma Contrase\xF1a',
                    value: this.state.confirmPassword,
                    errorText: errors.password,
                    onChange: this.onChange,
                    type: 'password'
                }),
                _react2.default.createElement(_RaisedButton2.default, {
                    label: 'Registrarme',
                    type: 'submit'
                })
            );
        }
    }]);

    return SignUpForm;
}(_react2.default.Component);

SignUpForm.PropTypes = {
    signup: _react2.default.PropTypes.func.isRequired
};

SignUpForm.contextTypes = {
    router: _react2.default.PropTypes.object.isRequired
};

exports.default = (0, _reactRedux.connect)(null, { signup: _authActions.signup })(SignUpForm);