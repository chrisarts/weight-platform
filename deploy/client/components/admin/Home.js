'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _reactRedux = require('react-redux');

var _AppBar = require('material-ui/AppBar');

var _AppBar2 = _interopRequireDefault(_AppBar);

var _Table = require('material-ui/Table');

var _dateformat = require('dateformat');

var _dateformat2 = _interopRequireDefault(_dateformat);

var _FlatButton = require('material-ui/FlatButton');

var _FlatButton2 = _interopRequireDefault(_FlatButton);

var _Dialog = require('material-ui/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _svgIcons = require('material-ui/svg-icons');

var _Navigation = require('./Navigation');

var _Navigation2 = _interopRequireDefault(_Navigation);

var _LoadingSection = require('../globalComponents/loading/LoadingSection');

var _LoadingSection2 = _interopRequireDefault(_LoadingSection);

var _articleActions = require('../actions/articleActions');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Home = function (_React$Component) {
    _inherits(Home, _React$Component);

    function Home(props) {
        _classCallCheck(this, Home);

        var _this = _possibleConstructorReturn(this, (Home.__proto__ || Object.getPrototypeOf(Home)).call(this, props));

        _this.state = {
            articles: [],
            category: _this.props.page.name,
            pagination: {
                pageSize: 20,
                currentPage: 1,
                page: _this.props.page.name
            },
            deleteOpen: false,
            isLoading: true
        };
        _this.styles = {
            deleteActions: {
                float: 'right',
                buttons: {
                    marginRight: 20
                }
            }
        };
        return _this;
    }

    _createClass(Home, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var This = this;
            this.props.getArticleList(This.state.pagination).then(function (result) {
                This.setState({
                    articles: result.data.filteredArticles,
                    isLoading: false
                });
            });
        }
    }, {
        key: 'handleDeleteOpen',
        value: function handleDeleteOpen() {
            this.setState({
                deleteOpen: true
            });
        }
    }, {
        key: 'deleteActionsPositive',
        value: function deleteActionsPositive(article) {
            var This = this;
            This.setState({
                isLoading: true
            });
            This.props.deleteArticle({ id: article }).then(function () {
                This.props.getArticleList(This.state.pagination).then(function (result) {
                    This.setState({
                        deleteOpen: false,
                        articles: result.data.filteredArticles
                    });
                });
            });
        }
    }, {
        key: 'deleteActionsNegative',
        value: function deleteActionsNegative() {
            this.setState({
                deleteOpen: false
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            return _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(_Navigation2.default, null),
                _react2.default.createElement(
                    'div',
                    { className: 'admin-page' },
                    _react2.default.createElement(_AppBar2.default, {
                        title: 'Panel de Administraci\xF3n',
                        className: 'app-toolbar-' + this.props.page.name
                    }),
                    _react2.default.createElement(
                        'h1',
                        null,
                        'Lista de Art\xEDculos'
                    ),
                    _react2.default.createElement(_LoadingSection2.default, {
                        payload: this.state.isLoading,
                        width: '100%',
                        height: '300px'
                    }),
                    _react2.default.createElement(
                        _Table.Table,
                        null,
                        _react2.default.createElement(
                            _Table.TableHeader,
                            null,
                            _react2.default.createElement(
                                _Table.TableRow,
                                null,
                                _react2.default.createElement(
                                    _Table.TableHeaderColumn,
                                    null,
                                    'ID'
                                ),
                                _react2.default.createElement(
                                    _Table.TableHeaderColumn,
                                    null,
                                    'Titulo'
                                ),
                                _react2.default.createElement(
                                    _Table.TableHeaderColumn,
                                    null,
                                    'Categor\xEDa'
                                ),
                                _react2.default.createElement(
                                    _Table.TableHeaderColumn,
                                    null,
                                    'Fecha'
                                ),
                                _react2.default.createElement(
                                    _Table.TableHeaderColumn,
                                    null,
                                    'Estado'
                                ),
                                _react2.default.createElement(
                                    _Table.TableHeaderColumn,
                                    null,
                                    'Editar'
                                ),
                                _react2.default.createElement(
                                    _Table.TableHeaderColumn,
                                    null,
                                    'Borrar'
                                )
                            )
                        ),
                        _react2.default.createElement(
                            _Table.TableBody,
                            null,
                            this.state.articles.map(function (article) {
                                return _react2.default.createElement(
                                    _Table.TableRow,
                                    { key: article.id },
                                    _react2.default.createElement(
                                        _Table.TableRowColumn,
                                        null,
                                        article.id
                                    ),
                                    _react2.default.createElement(
                                        _Table.TableRowColumn,
                                        null,
                                        article.title
                                    ),
                                    _react2.default.createElement(
                                        _Table.TableRowColumn,
                                        null,
                                        article.category.replace(/-/g, ' ')
                                    ),
                                    _react2.default.createElement(
                                        _Table.TableRowColumn,
                                        null,
                                        (0, _dateformat2.default)(article.created_at, "dddd, mmmm dS, yyyy")
                                    ),
                                    _react2.default.createElement(
                                        _Table.TableRowColumn,
                                        null,
                                        article.active ? 'Activo' : 'Inactivo'
                                    ),
                                    _react2.default.createElement(
                                        _Table.TableRowColumn,
                                        null,
                                        _react2.default.createElement(
                                            _reactRouter.Link,
                                            { to: '/admin/articles/' + article.id },
                                            _react2.default.createElement(_svgIcons.ContentCreate, null)
                                        )
                                    ),
                                    _react2.default.createElement(
                                        _Table.TableRowColumn,
                                        null,
                                        _react2.default.createElement(
                                            _reactRouter.Link,
                                            { to: '/admin/home#', onClick: _this2.handleDeleteOpen.bind(_this2) },
                                            _react2.default.createElement(_svgIcons.ContentClear, null)
                                        ),
                                        _react2.default.createElement(
                                            _Dialog2.default,
                                            {
                                                title: '\xBFEst\xE1s seguro de querer eliminar \xE9ste art\xEDculo?',
                                                open: _this2.state.deleteOpen
                                            },
                                            _react2.default.createElement(
                                                'div',
                                                {
                                                    className: 'delete-actions',
                                                    style: _this2.styles.deleteActions
                                                },
                                                _react2.default.createElement(_FlatButton2.default, {
                                                    label: 'No Eliminar',
                                                    onClick: _this2.deleteActionsNegative.bind(_this2),
                                                    backgroundColor: '#a4c639',
                                                    hoverColor: '#8AA62F',
                                                    style: _this2.styles.deleteActions.buttons
                                                }),
                                                _react2.default.createElement(_FlatButton2.default, {
                                                    label: 'Eliminar',
                                                    onClick: _this2.deleteActionsPositive.bind(_this2, article.id),
                                                    backgroundColor: '#9b2211',
                                                    hoverColor: '#8b1e0f',
                                                    style: _this2.styles.deleteActions.buttons
                                                })
                                            )
                                        )
                                    )
                                );
                            })
                        )
                    )
                )
            );
        }
    }]);

    return Home;
}(_react2.default.Component);

Home.PropTypes = {
    getArticleList: _react2.default.PropTypes.func.isRequired,
    deleteArticle: _react2.default.PropTypes.func.isRequired
};

function mapStateToProps(state) {
    return {
        page: state.page.page
    };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, { getArticleList: _articleActions.getArticleList, deleteArticle: _articleActions.deleteArticle })(Home);