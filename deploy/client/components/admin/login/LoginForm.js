'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _TextField = require('material-ui/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _RaisedButton = require('material-ui/RaisedButton');

var _RaisedButton2 = _interopRequireDefault(_RaisedButton);

var _reactRedux = require('react-redux');

var _authActions = require('../../actions/authActions');

var _validator = require('validator');

var _validator2 = _interopRequireDefault(_validator);

var _isEmpty = require('lodash/isEmpty');

var _isEmpty2 = _interopRequireDefault(_isEmpty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function validateInput(data) {
    var errors = {};

    if (_validator2.default.isEmpty(data.identifier)) {
        errors.identifier = 'Este campo es obligatorio';
    }

    if (_validator2.default.isEmpty(data.password)) {
        errors.password = 'Este campo es obligatorio';
    }

    return {
        errors: errors,
        isValid: (0, _isEmpty2.default)(errors)
    };
}

var LoginForm = function (_React$Component) {
    _inherits(LoginForm, _React$Component);

    function LoginForm(props) {
        _classCallCheck(this, LoginForm);

        var _this = _possibleConstructorReturn(this, (LoginForm.__proto__ || Object.getPrototypeOf(LoginForm)).call(this, props));

        _this.state = {
            identifier: '',
            password: '',
            errors: {},
            isLoading: false
        };
        _this.onChange = _this.onChange.bind(_this);
        _this.onSubmit = _this.onSubmit.bind(_this);
        return _this;
    }

    _createClass(LoginForm, [{
        key: 'onChange',
        value: function onChange(event) {
            this.setState(_defineProperty({}, event.target.name, event.target.value));
        }
    }, {
        key: 'isValid',
        value: function isValid() {
            var _validateInput = validateInput(this.state),
                errors = _validateInput.errors,
                isValid = _validateInput.isValid;

            if (!isValid) {
                this.setState({ errors: errors });
            }

            return isValid;
        }
    }, {
        key: 'onSubmit',
        value: function onSubmit(event) {
            var _this2 = this;

            event.preventDefault();
            if (this.isValid()) {
                this.setState({
                    errors: {},
                    isLoading: true
                });
                this.props.login(this.state).then(function (res) {
                    return _this2.context.router.push('/admin/home');
                }, function (err) {
                    return _this2.setState({ errors: err.response.data.errors, isLoading: false });
                });
            }
        }
    }, {
        key: 'render',
        value: function render() {
            var _state = this.state,
                errors = _state.errors,
                identifier = _state.identifier,
                password = _state.password;

            return _react2.default.createElement(
                'form',
                { onSubmit: this.onSubmit },
                _react2.default.createElement(
                    'hgroup',
                    null,
                    errors.form && _react2.default.createElement(
                        'h4',
                        null,
                        errors.form
                    )
                ),
                _react2.default.createElement(_TextField2.default, {
                    name: 'identifier',
                    floatingLabelText: 'Nombre de Usuario',
                    value: identifier,
                    errorText: errors.identifier,
                    onChange: this.onChange
                }),
                _react2.default.createElement(_TextField2.default, {
                    name: 'password',
                    floatingLabelText: 'Contrase\xF1a',
                    value: password,
                    errorText: errors.password,
                    onChange: this.onChange,
                    type: 'password'
                }),
                _react2.default.createElement(_RaisedButton2.default, {
                    label: 'Ingresar',
                    type: 'submit'
                })
            );
        }
    }]);

    return LoginForm;
}(_react2.default.Component);

LoginForm.PropTypes = {
    login: _react2.default.PropTypes.func.isRequired
};

LoginForm.contextTypes = {
    router: _react2.default.PropTypes.object.isRequired
};

exports.default = (0, _reactRedux.connect)(null, { login: _authActions.login })(LoginForm);