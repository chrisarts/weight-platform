'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _AppBar = require('material-ui/AppBar');

var _AppBar2 = _interopRequireDefault(_AppBar);

var _TextField = require('material-ui/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _RaisedButton = require('material-ui/RaisedButton');

var _RaisedButton2 = _interopRequireDefault(_RaisedButton);

var _reactTinymce = require('react-tinymce');

var _reactTinymce2 = _interopRequireDefault(_reactTinymce);

var _UploadPreview = require('material-ui-upload/UploadPreview');

var _UploadPreview2 = _interopRequireDefault(_UploadPreview);

var _Toggle = require('material-ui/Toggle');

var _Toggle2 = _interopRequireDefault(_Toggle);

var _Navigation = require('../Navigation');

var _Navigation2 = _interopRequireDefault(_Navigation);

var _articleActions = require('../../actions/articleActions');

require('./articles.css');

var _validator = require('validator');

var _validator2 = _interopRequireDefault(_validator);

var _isEmpty = require('lodash/isEmpty');

var _isEmpty2 = _interopRequireDefault(_isEmpty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function validateInputEvent(data) {
    var errors = {};

    if (_validator2.default.isEmpty(data.title)) {
        errors.title = 'Este campo es obligatorio';
    }
    if (_validator2.default.isEmpty(data.content)) {
        errors.content = 'Este campo es obligatorio';
    }

    return {
        errors: errors,
        isValid: (0, _isEmpty2.default)(errors)
    };
}

var CreateArticle = function (_React$Component) {
    _inherits(CreateArticle, _React$Component);

    function CreateArticle(props) {
        _classCallCheck(this, CreateArticle);

        var _this = _possibleConstructorReturn(this, (CreateArticle.__proto__ || Object.getPrototypeOf(CreateArticle)).call(this, props));

        _this.handleEditorChange = function (e) {
            _this.setState({
                content: e.target.getContent()
            });
        };

        _this.onChangeMedia = function (pictures) {
            _this.setState({ pictures: pictures });
            console.log(pictures);
        };

        _this.state = {
            title: '',
            content: '',
            pictures: {},
            category: _this.props.page.name,
            date: new Date(),
            outstanding: false,
            active: true,
            errors: {},
            isLoading: false
        };
        return _this;
    }

    _createClass(CreateArticle, [{
        key: 'isValid',
        value: function isValid() {
            var _validateInputEvent = validateInputEvent(this.state),
                errors = _validateInputEvent.errors,
                isValid = _validateInputEvent.isValid;

            if (!isValid) {
                this.setState({ errors: errors });
            }

            return isValid;
        }
    }, {
        key: 'onChange',
        value: function onChange(event) {
            this.setState(_defineProperty({}, event.target.name, event.target.value));
        }
    }, {
        key: 'onChangeCategory',
        value: function onChangeCategory(event, index, value) {
            this.setState({
                category: value
            });
        }
    }, {
        key: 'onToggleActive',
        value: function onToggleActive(event, toggle) {
            this.setState({
                active: toggle
            });
        }
    }, {
        key: 'onSubmit',
        value: function onSubmit(event) {
            var _this2 = this;

            event.preventDefault();
            var This = this;
            if (This.isValid()) {
                this.setState({ errors: {}, isLoading: true });
                console.log(this.state);
                this.props.createArticle(this.state).then(function () {
                    This.context.router.push('/admin/home');
                }, function (err) {
                    return _this2.setState({ errors: err.response, isLoading: false });
                });
            }
        }
    }, {
        key: 'render',
        value: function render() {
            var _state = this.state,
                errors = _state.errors,
                isLoading = _state.isLoading;

            return _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(_Navigation2.default, null),
                _react2.default.createElement(
                    'div',
                    { className: 'admin-page' },
                    _react2.default.createElement(_AppBar2.default, {
                        title: 'Crear Nuevo Art\xEDculo',
                        className: 'app-toolbar-' + this.props.page.name
                    }),
                    _react2.default.createElement(
                        'form',
                        { onSubmit: this.onSubmit.bind(this) },
                        _react2.default.createElement(_TextField2.default, {
                            name: 'title',
                            floatingLabelText: 'T\xEDtulo del Art\xEDculo *',
                            value: this.state.title,
                            errorText: errors.title,
                            onChange: this.onChange.bind(this)
                        }),
                        _react2.default.createElement(_reactTinymce2.default, {
                            content: '<p>C\xF3ntenido del art\xEDculo...</p>',
                            config: {
                                plugins: 'link code',
                                toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
                            },
                            onChange: this.handleEditorChange.bind(this),
                            errorText: errors.content
                        }),
                        _react2.default.createElement(
                            'div',
                            { className: 'image-upload' },
                            _react2.default.createElement(_UploadPreview2.default, {
                                title: 'Im\xE1genes',
                                label: 'A\xF1adir',
                                initialItems: this.state.pictures,
                                onChange: this.onChangeMedia
                            })
                        ),
                        _react2.default.createElement(_Toggle2.default, {
                            label: '\xBFArt\xEDculo activo?',
                            labelPosition: 'right',
                            defaultToggled: this.state.active,
                            onToggle: this.onToggleActive
                        }),
                        _react2.default.createElement(_RaisedButton2.default, {
                            label: 'Ingresar Art\xEDculo',
                            type: 'submit',
                            disabled: isLoading
                        })
                    )
                )
            );
        }
    }]);

    return CreateArticle;
}(_react2.default.Component);

CreateArticle.PropTypes = {
    createArticle: _react2.default.PropTypes.func.isRequired
};

CreateArticle.contextTypes = {
    router: _react2.default.PropTypes.object.isRequired
};

function mapStateToProps(state) {
    return {
        page: state.page.page
    };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, { createArticle: _articleActions.createArticle })(CreateArticle);