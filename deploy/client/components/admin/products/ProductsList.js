'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = require('react-redux');

var _reactRouter = require('react-router');

var _AppBar = require('material-ui/AppBar');

var _AppBar2 = _interopRequireDefault(_AppBar);

var _Dialog = require('material-ui/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _FlatButton = require('material-ui/FlatButton');

var _FlatButton2 = _interopRequireDefault(_FlatButton);

var _Table = require('material-ui/Table');

var _svgIcons = require('material-ui/svg-icons');

var _LoadingSection = require('../../globalComponents/loading/LoadingSection');

var _LoadingSection2 = _interopRequireDefault(_LoadingSection);

var _productActions = require('../../actions/productActions');

var _Navigation = require('../Navigation');

var _Navigation2 = _interopRequireDefault(_Navigation);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ProductsList = function (_React$Component) {
    _inherits(ProductsList, _React$Component);

    function ProductsList(props) {
        _classCallCheck(this, ProductsList);

        var _this = _possibleConstructorReturn(this, (ProductsList.__proto__ || Object.getPrototypeOf(ProductsList)).call(this, props));

        _this.state = {
            products: [],
            pagination: {
                pageSize: 20,
                currentPage: 1,
                page: _this.props.page.name
            },
            deleteOpen: false,
            isLoading: true,
            productToDelete: -1
        };
        _this.styles = {
            deleteActions: {
                float: 'right',
                buttons: {
                    marginRight: 20
                }
            }
        };
        return _this;
    }

    _createClass(ProductsList, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var This = this;
            this.props.getProductList(This.state.pagination).then(function (result) {
                This.setState({
                    products: result.data.filteredProducts,
                    isLoading: false
                });
            });
        }
    }, {
        key: 'handleDeleteOpen',
        value: function handleDeleteOpen(product) {
            this.setState({
                deleteOpen: true,
                productToDelete: product
            });
        }
    }, {
        key: 'deleteActionsPositive',
        value: function deleteActionsPositive() {
            var This = this;
            This.props.deleteProduct({ id: This.state.productToDelete }).then(function () {
                This.props.getProductList(This.state.pagination).then(function (result) {
                    This.setState({
                        deleteOpen: false,
                        products: result.data.filteredProducts
                    });
                });
            });
        }
    }, {
        key: 'deleteActionsNegative',
        value: function deleteActionsNegative() {
            this.setState({
                deleteOpen: false
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            return _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(_Navigation2.default, null),
                _react2.default.createElement(
                    'div',
                    { className: 'admin-page' },
                    _react2.default.createElement(_AppBar2.default, {
                        title: 'Panel de Administraci\xF3n',
                        className: 'app-toolbar-' + this.props.page.name
                    }),
                    _react2.default.createElement(
                        'h1',
                        null,
                        'Lista de Productos'
                    ),
                    _react2.default.createElement(_LoadingSection2.default, {
                        payload: this.state.isLoading,
                        width: '100%',
                        height: '300px'
                    }),
                    _react2.default.createElement(
                        _Table.Table,
                        null,
                        _react2.default.createElement(
                            _Table.TableHeader,
                            null,
                            _react2.default.createElement(
                                _Table.TableRow,
                                null,
                                _react2.default.createElement(
                                    _Table.TableHeaderColumn,
                                    null,
                                    'ID'
                                ),
                                _react2.default.createElement(
                                    _Table.TableHeaderColumn,
                                    null,
                                    'Nombre'
                                ),
                                _react2.default.createElement(
                                    _Table.TableHeaderColumn,
                                    null,
                                    'Descripci\xF3n'
                                ),
                                _react2.default.createElement(
                                    _Table.TableHeaderColumn,
                                    null,
                                    'Estado'
                                ),
                                _react2.default.createElement(
                                    _Table.TableHeaderColumn,
                                    null,
                                    'Editar'
                                ),
                                _react2.default.createElement(
                                    _Table.TableHeaderColumn,
                                    null,
                                    'Eliminar'
                                )
                            )
                        ),
                        _react2.default.createElement(
                            _Table.TableBody,
                            null,
                            this.state.products.map(function (product) {
                                return _react2.default.createElement(
                                    _Table.TableRow,
                                    { key: product.id },
                                    _react2.default.createElement(
                                        _Table.TableRowColumn,
                                        null,
                                        product.id
                                    ),
                                    _react2.default.createElement(
                                        _Table.TableRowColumn,
                                        null,
                                        product.name
                                    ),
                                    _react2.default.createElement(
                                        _Table.TableRowColumn,
                                        null,
                                        product.description.replace(/-/g, ' ')
                                    ),
                                    _react2.default.createElement(
                                        _Table.TableRowColumn,
                                        null,
                                        product.active ? 'Activo' : 'Inactivo'
                                    ),
                                    _react2.default.createElement(
                                        _Table.TableRowColumn,
                                        null,
                                        _react2.default.createElement(
                                            _reactRouter.Link,
                                            { to: '/admin/products/' + product.id },
                                            _react2.default.createElement(_svgIcons.ContentCreate, null)
                                        )
                                    ),
                                    _react2.default.createElement(
                                        _Table.TableRowColumn,
                                        null,
                                        _react2.default.createElement(
                                            _reactRouter.Link,
                                            { to: '/admin/products#', onClick: _this2.handleDeleteOpen.bind(_this2, product.id) },
                                            _react2.default.createElement(_svgIcons.ContentClear, null)
                                        ),
                                        _react2.default.createElement(
                                            _Dialog2.default,
                                            {
                                                title: '\xBFEst\xE1s seguro de querer eliminar \xE9ste producto?',
                                                open: _this2.state.deleteOpen
                                            },
                                            _react2.default.createElement(
                                                'div',
                                                {
                                                    className: 'delete-actions',
                                                    style: _this2.styles.deleteActions
                                                },
                                                _react2.default.createElement(_FlatButton2.default, {
                                                    label: 'No Eliminar',
                                                    onClick: _this2.deleteActionsNegative.bind(_this2),
                                                    backgroundColor: '#a4c639',
                                                    hoverColor: '#8AA62F',
                                                    style: _this2.styles.deleteActions.buttons
                                                }),
                                                _react2.default.createElement(_FlatButton2.default, {
                                                    label: 'Eliminar',
                                                    onClick: _this2.deleteActionsPositive.bind(_this2, product.id),
                                                    backgroundColor: '#9b2211',
                                                    hoverColor: '#8b1e0f',
                                                    style: _this2.styles.deleteActions.buttons
                                                })
                                            )
                                        )
                                    )
                                );
                            })
                        )
                    )
                )
            );
        }
    }]);

    return ProductsList;
}(_react2.default.Component);

ProductsList.PropTypes = {
    getProductList: _propTypes2.default.func.isRequired,
    deleteProduct: _propTypes2.default.func.isRequired
};

function mapStateToProps(state) {
    return {
        page: state.page.page
    };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, { getProductList: _productActions.getProductList, deleteProduct: _productActions.deleteProduct })(ProductsList);