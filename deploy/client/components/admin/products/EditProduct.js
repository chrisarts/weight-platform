'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _AppBar = require('material-ui/AppBar');

var _AppBar2 = _interopRequireDefault(_AppBar);

var _TextField = require('material-ui/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _RaisedButton = require('material-ui/RaisedButton');

var _RaisedButton2 = _interopRequireDefault(_RaisedButton);

var _reactTinymce = require('react-tinymce');

var _reactTinymce2 = _interopRequireDefault(_reactTinymce);

var _UploadPreview = require('material-ui-upload/UploadPreview');

var _UploadPreview2 = _interopRequireDefault(_UploadPreview);

var _Toggle = require('material-ui/Toggle');

var _Toggle2 = _interopRequireDefault(_Toggle);

var _Navigation = require('../Navigation');

var _Navigation2 = _interopRequireDefault(_Navigation);

var _svgIcons = require('material-ui/svg-icons');

var _productActions = require('../../actions/productActions');

var _validator = require('validator');

var _validator2 = _interopRequireDefault(_validator);

var _isEmpty = require('lodash/isEmpty');

var _isEmpty2 = _interopRequireDefault(_isEmpty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

//Component Actions


function validateInputEvent(data) {
    var errors = {};

    if (_validator2.default.isEmpty(data.name)) {
        errors.name = 'Este campo es obligatorio';
    }
    if (_validator2.default.isEmpty(data.description)) {
        errors.description = 'Este campo es obligatorio';
    }

    if (_validator2.default.isEmpty(data.price)) {
        errors.price = 'Este campo es obligatorio';
    }

    return {
        errors: errors,
        isValid: (0, _isEmpty2.default)(errors)
    };
}

var EditProduct = function (_React$Component) {
    _inherits(EditProduct, _React$Component);

    function EditProduct(props) {
        _classCallCheck(this, EditProduct);

        var _this = _possibleConstructorReturn(this, (EditProduct.__proto__ || Object.getPrototypeOf(EditProduct)).call(this, props));

        _this.state = {
            id: _this.props.params.identifier,
            name: '',
            description: '',
            media: {},
            images: [],
            price: 0,
            page: _this.props.page.name,
            active: false,
            isLoading: false,
            errors: {}
        };
        return _this;
    }

    _createClass(EditProduct, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var This = this;
            This.props.getProduct({ id: This.props.params.identifier }).then(function (result) {
                var media = JSON.parse(result.data.product.media);
                var count = 0;
                var images = [];
                for (var key in media) {
                    if (media.hasOwnProperty(key) && count === 0) {
                        images.push(media[key]);
                        ++count;
                    }
                }
                This.setState({
                    name: result.data.product.name,
                    description: result.data.product.description,
                    media: media,
                    images: images,
                    price: result.data.product.price,
                    page: result.data.product.page,
                    active: result.data.product.active
                });
            });
        }
    }, {
        key: 'isValid',
        value: function isValid() {
            var _validateInputEvent = validateInputEvent(this.state),
                errors = _validateInputEvent.errors,
                isValid = _validateInputEvent.isValid;

            if (!isValid) {
                this.setState({ errors: errors });
            }

            return isValid;
        }
    }, {
        key: 'onChange',
        value: function onChange(event) {
            this.setState(_defineProperty({}, event.target.name, event.target.value));
        }
    }, {
        key: 'handleEditorChange',
        value: function handleEditorChange(e) {
            this.setState({
                description: e.target.getContent()
            });
        }
    }, {
        key: 'onChangeMedia',
        value: function onChangeMedia(pictures) {
            this.setState({ media: pictures });
            console.log(pictures);
        }
    }, {
        key: 'onToggleActive',
        value: function onToggleActive(event, toggle) {
            console.log(toggle);
            this.setState({
                active: toggle
            });
        }
    }, {
        key: 'createProduct',
        value: function createProduct(e) {
            e.preventDefault();
            var This = this;
            console.log(This.state);
            if (This.isValid()) {
                This.setState({ errors: {}, isLoading: true });
                This.props.updateProduct(This.state).then(function () {
                    This.setState({ isLoading: false });
                    This.context.router.push('/admin/products');
                }, function (err) {
                    console.log('Current errors: ', err.response);
                    window.scrollTo(0, 0);
                    if (err.response.data.errors) {
                        This.setState({ errors: err.response.data.errors, isLoading: false });
                    } else {
                        This.setState({ errors: err.response, isLoading: false });
                    }
                });
            }
        }
    }, {
        key: 'render',
        value: function render() {
            var _state = this.state,
                errors = _state.errors,
                isLoading = _state.isLoading;

            var count = 0;
            return _react2.default.createElement(
                'div',
                { className: 'create-product' },
                _react2.default.createElement(_Navigation2.default, null),
                _react2.default.createElement(
                    'div',
                    { className: 'admin-page' },
                    _react2.default.createElement(_AppBar2.default, {
                        title: 'Actualizar Producto',
                        className: 'app-toolbar-' + this.props.page.name,
                        titleStyle: { marginLeft: 5 },
                        iconElementLeft: _react2.default.createElement(_svgIcons.ActionNoteAdd, {
                            color: 'white',
                            style: {
                                width: 50,
                                height: 50
                            }
                        })
                    }),
                    _react2.default.createElement(
                        'form',
                        { onSubmit: this.createProduct.bind(this) },
                        _react2.default.createElement(_AppBar2.default, {
                            title: 'Informaci\xF3n del Producto',
                            style: {
                                height: 35
                            },
                            titleStyle: {
                                marginLeft: 5,
                                fontSize: 15,
                                lineHeight: '37px'
                            },
                            iconElementLeft: _react2.default.createElement(_svgIcons.AvArtTrack, {
                                color: 'white',
                                style: {
                                    width: 30,
                                    height: 30
                                }
                            })
                        }),
                        _react2.default.createElement(
                            'div',
                            null,
                            _react2.default.createElement(_TextField2.default, {
                                name: 'name',
                                floatingLabelText: 'Nombre del Producto *',
                                value: this.state.name,
                                errorText: errors.name,
                                onChange: this.onChange.bind(this),
                                style: {
                                    width: '100%'
                                }
                            }),
                            _react2.default.createElement(_TextField2.default, {
                                name: 'price',
                                floatingLabelText: 'Precio del Producto *',
                                value: this.state.price,
                                errorText: errors.price,
                                onChange: this.onChange.bind(this),
                                style: {
                                    width: '100%'
                                }
                            })
                        ),
                        this.state.description !== '' ? _react2.default.createElement(_reactTinymce2.default, {
                            content: this.state.description,
                            config: {
                                plugins: 'link image code',
                                toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
                            },
                            onChange: this.handleEditorChange.bind(this),
                            errorText: errors.description
                        }) : _react2.default.createElement(
                            'div',
                            null,
                            '\xA0'
                        ),
                        _react2.default.createElement(
                            'figure',
                            {
                                style: {
                                    width: '100%'
                                }
                            },
                            _react2.default.createElement(
                                'h4',
                                null,
                                'Imagenes Actuales'
                            ),
                            this.state.images.map(function (image) {
                                ++count;
                                return _react2.default.createElement('img', {
                                    key: count,
                                    src: image,
                                    alt: '',
                                    style: {
                                        float: 'left',
                                        width: '30%',
                                        border: '1px solid'
                                    }
                                });
                            })
                        ),
                        _react2.default.createElement(
                            'div',
                            {
                                className: 'image-upload'
                            },
                            _react2.default.createElement(_UploadPreview2.default, {
                                title: 'Im\xE1genes',
                                label: 'A\xF1adir',
                                initialItems: this.state.media,
                                onChange: this.onChangeMedia.bind(this)
                            })
                        ),
                        _react2.default.createElement(_Toggle2.default, {
                            label: '\xBFProducto activo?',
                            labelPosition: 'right',
                            defaultToggled: this.state.active,
                            onToggle: this.onToggleActive.bind(this)
                        }),
                        _react2.default.createElement(_RaisedButton2.default, {
                            label: 'Actualizar Producto',
                            type: 'submit',
                            disabled: isLoading
                        })
                    )
                )
            );
        }
    }]);

    return EditProduct;
}(_react2.default.Component);

EditProduct.PropTypes = {
    getProduct: _propTypes2.default.func.isRequired,
    updateProduct: _propTypes2.default.func.isRequired
};

EditProduct.contextTypes = {
    router: _react2.default.PropTypes.object.isRequired
};

function mapStateToProps(state) {
    return {
        page: state.page.page
    };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, { getProduct: _productActions.getProduct, updateProduct: _productActions.updateProduct })(EditProduct);