'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _AppBar = require('material-ui/AppBar');

var _AppBar2 = _interopRequireDefault(_AppBar);

var _Drawer = require('material-ui/Drawer');

var _Drawer2 = _interopRequireDefault(_Drawer);

var _List = require('material-ui/List');

var _Subheader = require('material-ui/Subheader');

var _Subheader2 = _interopRequireDefault(_Subheader);

var _FlatButton = require('material-ui/FlatButton');

var _FlatButton2 = _interopRequireDefault(_FlatButton);

var _reactRouter = require('react-router');

var _svgIcons = require('material-ui/svg-icons');

var _authActions = require('../actions/authActions');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Navigation = function (_React$Component) {
    _inherits(Navigation, _React$Component);

    function Navigation(props) {
        _classCallCheck(this, Navigation);

        var _this = _possibleConstructorReturn(this, (Navigation.__proto__ || Object.getPrototypeOf(Navigation)).call(this, props));

        _this.state = {
            drawer: true
        };
        return _this;
    }

    _createClass(Navigation, [{
        key: 'logout',
        value: function logout(event) {
            event.preventDefault();
            this.props.logout();
        }
    }, {
        key: 'render',
        value: function render() {
            var userLinks = _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(
                    _reactRouter.Link,
                    { to: '/#' },
                    _react2.default.createElement(_FlatButton2.default, {
                        label: 'Salir',
                        onClick: this.logout.bind(this)
                    })
                )
            );
            var page = this.props.page;
            return _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(
                    _Drawer2.default,
                    { open: this.state.drawer },
                    _react2.default.createElement(_AppBar2.default, {
                        title: 'Men\xFA',
                        iconElementRight: userLinks,
                        className: 'app-toolbar-' + page.name
                    }),
                    _react2.default.createElement(
                        _List.List,
                        null,
                        _react2.default.createElement(
                            _Subheader2.default,
                            null,
                            'Men\xFA de administraci\xF3n'
                        ),
                        _react2.default.createElement(_List.ListItem, {
                            primaryText: 'Articulos',
                            leftIcon: _react2.default.createElement(_svgIcons.CommunicationImportContacts, null),
                            initiallyOpen: true,
                            primaryTogglesNestedList: true,
                            nestedItems: [_react2.default.createElement(_List.ListItem, {
                                key: 2,
                                primaryText: 'Lista',
                                leftIcon: _react2.default.createElement(_svgIcons.ActionList, null),
                                href: '/admin/home'
                            }), _react2.default.createElement(_List.ListItem, {
                                key: 3,
                                primaryText: 'Crear',
                                leftIcon: _react2.default.createElement(_svgIcons.ContentCreate, null),
                                href: '/admin/articles/create'
                            })]
                        }),
                        _react2.default.createElement(_List.ListItem, {
                            primaryText: 'Productos',
                            leftIcon: _react2.default.createElement(_svgIcons.CommunicationImportContacts, null),
                            initiallyOpen: true,
                            primaryTogglesNestedList: true,
                            nestedItems: [_react2.default.createElement(_List.ListItem, {
                                key: 2,
                                primaryText: 'Lista',
                                leftIcon: _react2.default.createElement(_svgIcons.ActionList, null),
                                href: '/admin/products'
                            }), _react2.default.createElement(_List.ListItem, {
                                key: 3,
                                primaryText: 'Crear',
                                leftIcon: _react2.default.createElement(_svgIcons.ContentCreate, null),
                                href: '/admin/products/create'
                            })]
                        })
                    )
                )
            );
        }
    }]);

    return Navigation;
}(_react2.default.Component);

Navigation.PropTypes = {
    logout: _react2.default.PropTypes.func.isRequired
};

function mapStateToProps(state) {
    return {
        auth: state.auth,
        page: state.page.page
    };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, { logout: _authActions.logout })(Navigation);